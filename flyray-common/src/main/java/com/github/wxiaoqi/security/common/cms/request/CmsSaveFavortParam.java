package com.github.wxiaoqi.security.common.cms.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("小程序点赞参数")
public class CmsSaveFavortParam {
	@NotNull(message="用户信息编号不能为空")
	@ApiModelProperty("用户信息编号")
	private String customerId;
	@ApiModelProperty("观点编号")
	private String pointId;
	@ApiModelProperty("1点赞2取消赞")
	private Integer favortStatus;
	@NotNull(message="商户号不能为空")
	@ApiModelProperty("商户号")
	private String merchantId;
	@NotNull(message="平台编号不能为空")
	@ApiModelProperty("平台编号")
	private String platformId;
	@ApiModelProperty("昵称")
	private String nickName;
	@ApiModelProperty("头像")
	private String avatarUrl;
}
