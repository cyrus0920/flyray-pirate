package com.github.wxiaoqi.security.common.cms.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("小程序社群参数")
public class CmsSocietyRequestParam {
	@NotNull(message="商户号不能为空")
	@ApiModelProperty("商户号")
	private String merchantId;
	@NotNull(message="平台编号不能为空")
	@ApiModelProperty("平台编号")
	private String platformId;
	@ApiModelProperty("编号")
	private String id;
	@ApiModelProperty("名称")
	private String societyName;
	@ApiModelProperty("二维码")
	private String societyQr;
	@ApiModelProperty("介绍内容")
	private String societyContent;
}
