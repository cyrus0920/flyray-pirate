package com.github.wxiaoqi.security.common.vo;

/**
 * 猎手最新获赏记录
 * 
 * @author centerroot
 * @time 创建时间:2018年6月6日下午1:43:33
 * @description
 */
public class RewardRecord {

	/**
	 * 手机号
	 */
	private String mobile;
	/**
	 * 交易类型
	 */
	private String type;
	/**
	 * 交易金额
	 */
	private String amount;

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

}
