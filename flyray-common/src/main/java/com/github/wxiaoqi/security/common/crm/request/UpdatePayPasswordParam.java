package com.github.wxiaoqi.security.common.crm.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("修改用户支付密码")
public class UpdatePayPasswordParam {
	@NotNull(message="用户信息编号不能为空")
	@ApiModelProperty("用户信息编号")
	private String customerId;
	@NotNull(message="手机号不能为空")
	@ApiModelProperty("用户手机号")
	private String mobile;
	@NotNull(message="手机验证码不能为空")
	@ApiModelProperty("用户手机验证码")
	private String smsCode;
	@NotNull(message="支付密码为空")
	@ApiModelProperty("支付密码")
	private String paymentPassword;
	@NotNull(message="原支付密码不能为空")
	@ApiModelProperty("原支付密码")
	private String oldPaymentPassword;
	
}
