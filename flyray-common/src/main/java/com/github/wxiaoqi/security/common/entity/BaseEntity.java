package com.github.wxiaoqi.security.common.entity;

import java.io.Serializable;

import javax.persistence.Transient;


/**
 * 基础实体类
 * @author centerroot
 * @time 创建时间:2018年7月20日上午10:47:48
 * @description
 */
public class BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	/**
	 * 用户权限1，系统管理员2，平台管理员3，商户管理员4、平台操作员
	 */
	@Transient
    public int userType;

	public int getUserType() {
		return userType;
	}

	public void setUserType(int userType) {
		this.userType = userType;
	}
	

}
