package com.github.wxiaoqi.security.common.admin.pay.response;

import java.io.Serializable;

/**
 * 代付申请响应
 * @author Administrator
 *
 */
public class PayForAnotherApplyResponse extends BaseResponse implements Serializable{

	private static final long serialVersionUID = 1L;

	//代付订单号
	private String orderId;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

}
