package com.github.wxiaoqi.security.common.crm.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("退款时解冻资金接口参数")
public class RefundUnFreezeRequest {
	@NotNull(message = "平台编号不能为空")
	@ApiModelProperty("平台编号")
	private String platformId;
	
	@NotNull(message = "个人客户编号不能为空")
	@ApiModelProperty("个人客户编号")
	private String personalId;
	
	@NotNull(message = "退款订单号不能为空")
	@ApiModelProperty("退款订单号")
	private String refundOrderNo;
	
	@NotNull(message = "解冻金额不能为空")
	@ApiModelProperty("解冻金额")
	private String refoundAmt;

	@NotNull(message = "资金来源编号不能为空")
	@ApiModelProperty("资金来源编号")
	private String sellerId;
	
	@NotNull(message = "商品说明不能为空")
	@ApiModelProperty("商品说明")
	private String produceInfo;
	
	@NotNull(message = "付款方式不能为空")
	@ApiModelProperty("付款方式")
	private String payWay;
	
	@NotNull(message = "商户编号不能为空")
	@ApiModelProperty("商户编号")
	private String merchantId;
	
	@NotNull(message = "客户类型不能为空")
	@ApiModelProperty("客户类型")
	private String customerType;
	
	@NotNull(message = "账户类型不能为空")
	@ApiModelProperty("账户类型")
	private String accountType;
	
	@NotNull(message = "支付方式不能为空")
	@ApiModelProperty("支付方式")
	private String payType;
	
	@NotNull(message = "冻结流水号不能为空")
	@ApiModelProperty("冻结流水号")
	private String freezeId;
	
	
}
