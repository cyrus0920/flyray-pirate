package com.github.wxiaoqi.security.common.crm.request;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * 个人客户基础信息请求实体
 * @author centerroot
 * @time 创建时间:2018年7月16日下午4:24:07
 * @description
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "个人客户基础信息请求参数")
public class PersonalBaseRequest {
	
	@ApiModelProperty(value = "序号")
    private Integer id;
	
	@ApiModelProperty(value = "个人客户编号")
    private String personalId;
	
    @NotNull(message="平台编号不能为空")
	@ApiModelProperty(value = "平台编号")
    private String platformId;
	
	@ApiModelProperty(value = "用户编号")
    private String customerId;
	
	@ApiModelProperty(value = "第三方会员编号")
    private String thirdNo;
	
	@ApiModelProperty(value = "用户名称")
    private String realName;
	
	@ApiModelProperty(value = "身份证号")
    private String idNumber;
	
	@ApiModelProperty(value = "用户昵称")
    private String nickName;
	
    @ApiModelProperty(value = "性别")
    private String sex;
	
	@ApiModelProperty(value = "生日")
    private String birthday;
	
	@ApiModelProperty(value = "居住地")
    private String address;
	
	@ApiModelProperty(value = "家乡")
    private String hometown;
	
	@ApiModelProperty(value = "身份证正面")
    private String idPositive;
	
	@ApiModelProperty(value = "身份证反面")
    private String idNegative;
	
	@ApiModelProperty(value = "认证状态")
    private String authenticationStatus;
	
	@ApiModelProperty(value = "账户状态")
    private String status;
	
	@ApiModelProperty(value = "创建时间")
    private Date createTime;
	
	@ApiModelProperty(value = "更新时间")
    private Date updateTime;
	
	// 归属人（存后台管理系统登录人员id）指谁发展的客户
	@ApiModelProperty(value = "归属人")
    private long owner;

    // 用户头像
	@ApiModelProperty(value = "用户头像")
    private String avatar;
}
