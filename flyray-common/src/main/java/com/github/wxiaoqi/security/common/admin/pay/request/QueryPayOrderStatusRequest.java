package com.github.wxiaoqi.security.common.admin.pay.request;

import java.io.Serializable;

/**
 * 支付订单查询请求
 * @author hexufeng
 *
 */
public class QueryPayOrderStatusRequest extends BaseRequest implements Serializable{

	private static final long serialVersionUID = 1L;
	
	/**
	 * 平台编号
	 */
	private String platformId;
	
	/**
	 * 支付订单号
	 */
	private String payOrderNo;

	public String getPlatformId() {
		return platformId;
	}

	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}

	public String getPayOrderNo() {
		return payOrderNo;
	}

	public void setPayOrderNo(String payOrderNo) {
		this.payOrderNo = payOrderNo;
	}

}
