package com.github.wxiaoqi.security.common.msg;

/**
 * 短信通知类型
 */
public enum SmsType {

	register("01","注册"),
    changeMobile("02","更换手机号"),
    updatePaymentPassword("03","修改支付密码 "),
    logIn("04","短信登录 "),
	resetPassword("05","重置登录密码 ");

    private String code;
    private String desc;

    private SmsType(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public static SmsType getSmsType(String code) {
        for (SmsType o : SmsType.values()) {
            if (o.getCode().equals(code)) {
                return o;
            }
        }
        return null;
    }
}
