package com.github.wxiaoqi.security.common.admin.pay.request;

/** 
 * @author: bolei
 * @date：2017年2月23日 下午12:45:19 
 * @description：类说明 
 */

public class QueryRefundStatusRequest {

	/**
	 * 商户客户号
	 */
	private String merId;
	/**
	 * 平台编号
	 */
	private String platformId;

	/**
	 * 支付公司编号
	 */
	private String payCompanyNo;

	/**
	 * 支付通道编号
	 */
	private String payChannelNo;
	
	private String outTradeNo; // 商户订单号

	private String mchtRefundOrderNo;//商户退款订单号

	public String getMerId() {
		return merId;
	}

	public void setMerId(String merId) {
		this.merId = merId;
	}

	public String getPlatformId() {
		return platformId;
	}

	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}

	public String getPayCompanyNo() {
		return payCompanyNo;
	}

	public void setPayCompanyNo(String payCompanyNo) {
		this.payCompanyNo = payCompanyNo;
	}

	public String getPayChannelNo() {
		return payChannelNo;
	}

	public void setPayChannelNo(String payChannelNo) {
		this.payChannelNo = payChannelNo;
	}

	public String getOutTradeNo() {
		return outTradeNo;
	}

	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo = outTradeNo;
	}

	public String getMchtRefundOrderNo() {
		return mchtRefundOrderNo;
	}

	public void setMchtRefundOrderNo(String mchtRefundOrderNo) {
		this.mchtRefundOrderNo = mchtRefundOrderNo;
	}

}
