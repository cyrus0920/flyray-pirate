package com.github.wxiaoqi.security.common.cms.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("评价信息参数")
public class RestaurantAppraisalDetailParam extends BaseParam{
	
	@NotNull(message="餐品编号不能为空")
	@ApiModelProperty("餐品编号")
	private String dishesId;

}
