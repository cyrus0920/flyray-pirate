package com.github.wxiaoqi.security.common.cms.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("商品明细查询")
public class FightGroupDetailParam extends BaseParam{
	
	@NotNull(message="用户账号不能为空")
	@ApiModelProperty("用户账号")
	private String perId;
	
	@NotNull(message="商品编号不能为空")
	@ApiModelProperty("商品编号")
	private String goodsId;
	
	@NotNull(message="团id不能为空")
	@ApiModelProperty("团id")
	private String groupId;

}
