package com.github.wxiaoqi.security.common.msg;

public enum ResponseCode {

    OK("0000", "请求成功"),
    INVALID_FIELDS("400001", "请求参数非法"),
    ABNORMAL_FIELDS("400002", "异常"),
    CUST_NOTEXIST("400003", "用户不存在"),
    MER_NOTEXIST("400004", "商户不存在"),
    PER_NOTEXIST("400005", "会员不存在"),
    PER_ACC_NOTEXIST("400006", "会员账户不存在"),
    MER_ACC_NOTEXIST("400007", "商户账户不存在"),
	UOLOAD_FILE_FAIL("400008","文件上传失败"),
    USER_FREEZE("400009", "用户被冻结"),
    PER_FREEZE("400010", "会员被冻结"),
    MER_FREEZE("400011", "商户被冻结"),
    PER_ACC_FREEZE("400012", "会员账户被冻结"),
    MER_ACC_FREEZE("400013", "商户账户被冻结"),
	FREEZE_ID_NOTEXIST("400014","冻结流水不存在"),
	FREEZE_UNFREEZE_AMT_INCONSISTEND("400015","解冻金额大于冻结金额"),
    VERIFY_ACC_AMT("400016", "账户余额校验错误"),
    ACC_BALANCE_INSUFFICIENT("400017", "账户余额不足"),
    BIND_CARD_NOTEXIST("400018", "银行卡不存在"),
    PLAT_ACC_TYPE_NOT_EXIST("400019", "平台账户类型不存在"),
    LOCK_USER_PWD("400020", "密码锁定"),
    MOBILE_REPEAT("400021", "手机号已注册"),
    SMS_SEND_FAIL("400022", "短信发送失败"),
    MOBILE_MATCH_FAIL("400023", "手机号错误"),
    SMS_CODE_MATCH_FAIL("400024", "短信验证码错误"),
    OVER_LIMIT("400025","超出限制条数"),
    PWD_ERROR("400026", "密码错误"),
    PAY_PWD_ERROR("400027", "支付密码错误"),
    FEE_VERRIF_ERROR("400028", "手续费校验错误"),
    FEE_LOW_ERROR("400029", "悬赏金额小于平台最低金额"),
    PWD_RESET_ERROR("400030", "密码重置失败"),
    MER_ACC_OPENFAIL("400031", "商户账户开户失败"),
    MER_ACC_INTOACCOUNTFAIL("400032", "商户账户入账失败"),
    PER_ACC_OPENFAIL("400033", "个人账户开户失败"),
    PER_PAYBILL_NOT_EXIST("400034", "支付账单不存在"),
    UNFREEZE_IS_EXIST("400035", "冻结资金已解冻"),
    PER_REFONDBILL_NOT_EXIST("400036", "退款账单不存在"),
	/*****************************用户登录错误码***************************/
	USERINFO_NOTEXIST("400050","登录用户不存在"),
	USERNAME_NOTNULL("400051","登录用户不能为空"),
	PASSWORD_NOTNULL("400052","登录密码不能为空"),
	USERINFO_PWD_ERROR("400053","登录密码错误"),
	LOGIN_ERROR("400054","登录失败"),
	TOKEN_ISINVALID("400055","token失效"),
	PARAM_NOTNULL("400056","请求参数不能为空"),
	SIGN_NOTNULL("400057","签名不能为空"),
	SIGN_MATCH_FAIL("400058","签名验证失败"),
	SMS_CODE_ISNULL("400059", "短信验证码不能为空"),
	
	/*****************************用户注册错误码***************************/
	USERINFO_ISEXIST("400060","用户已注册"),
	REGISTER_ERROR("400061","注册失败"),
	/*****************************商户认证错误码****************************/
	MERCHANT_ISEXIST("400062","商户已经认证"),
	MERCHANT_FAIL("400063","商户认证失败"),
	APPLYROLE_FAIL("400064","申请失败"),
	CHANGEROLE_FAIL("400065","申请失败"),
	USERLOGINOUT_FAIL("400066","申请失败"),
	
	
	
	
	
    //电话相同简历，被多次投递
    RESUM_REPEAT_SEND_USER("400100", "用户简历重复投递"),
    FUNCTION_NOTEXIST("400101","职能不存在"),
    INDUSTRY_NOTEXIST("400102","行业不存在"),
    ORDER_NOTEXIST("400103", "订单不存在"),
    PAY_ORDER_NOTEXIST("400104","支付订单不存在"),
    FREEZE_ORDER_NOTEXIST("400105", "冻结订单不存在"),
    RESUM_NOTEXIST("400106", "简历不存在"),
    TASK_NOTEXIST("400107", "任务不存在"),
    RESUM_REPEAT_SEND("400108", "简历重复投递"),
    REPEAT_RECEIVER("400109", "任务重复领取"),
	PAY_CREATE_ORDER_FAIL("400110","创建支付订单失败"),
	PAY_INITIATE_PAYMENT_FAIL("400111","发起支付调用失败"),
    RECEIVE_OWN_ERROR("400112", "用户不能领取自己发布的任务"),
    TASK_RECEIVE_NOT("400113", "非运行中的任务不允许领取"),
    SUBFUNCTION_EXIST("400114", "有子职能存在"),
    SUBINDUSTRY_EXIST("400115", "有行业能存在"),
	
	
	
	
	/*****************************支付模块****************************/
	ORDER_EXIST("400200","订单已存在"),
	NO_AVAILABLE_PAYCHANNEL("400201","无可用支付通道"),
	ORDER_NO_EXIST("400202","订单不存在"),
	ORDER_PAID("400203","订单已支付"),
	INTERFACE_NOTEXIST("400204","接口配置不存在"),
	PAYCHANNEL_INTERFACE_NOTEXIST("400205","支付通道接口不存在"),
	PAYCHANNEL_CONFIGURATION_NOTEXIST("400206","支付通道配置不存在"),
	WECHAT_RESOLVE_FAIL("400207","微信解析失败"),
	SEND_DATA_FAIL("400208","发送数据失败"),
	WECHAT_REFUND_FAIL("400209","微信退款失败"),
	WECHAT_REFUND_TIME_OUT("400210","微信退款超时"),
	PAY_FAIL("400211","支付失败"),
	PAY_NOT_SUCCESS_CAN_NOT_REFUND("400212","订单支付未成功，不能发起退款"),
	ORDER_REFUND("400213","订单已退款"),
	PAY_SERIAL_NOTEXIST("400214","支付流水不存在"),
	PAY_FOR_ANOTHER_FAIL("400215","代付失败"),
	REFUND_AMT_OVER_LIMIT("400216","退款金额大于可退款金额"),
	PAY_CHANNEL_CONFIG_ISEXIST("400217","平台/商户支付通道已存在"),
	REFUND_FAIL("400218","退款失败"),
	/***********************************token相关错误码*****************************************/
	TOKEN_RESOLVEFAIL_CODE("400900","解析Token失败"),
	TOKEN_ISNULL_CODE("400901","获取Token失败"),
	TOKEN_CHECKFAIL_CODE("400902","验证Token失败"),
	TOKEN_ISNOTVALID_CODE("400903","Token无效"),
	TOKEN_SIGNISNULL_CODE("400904","网关传输参数签名为空"),	
	TOKEN_CHECKSIGNFAIL_CODE("400905","网关校验传输参数签名失败");
    private String code;
    private String message;

	private ResponseCode(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


}
