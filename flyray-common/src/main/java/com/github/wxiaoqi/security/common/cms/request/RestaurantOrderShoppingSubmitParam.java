package com.github.wxiaoqi.security.common.cms.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("点餐购物车信息参数")
public class RestaurantOrderShoppingSubmitParam extends BaseParam{

	@NotNull(message="订单信息不能为空")
	@ApiModelProperty("订单信息")
	private String orderInfo;

}
