package com.github.wxiaoqi.security.common.util;


import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ValidateSign {
	private final static Logger logger = LoggerFactory.getLogger(ValidateSign.class);
    /**
     * @author myq
     * @param sign 请求中的签名，key 请求中生产签名用的盐值， parammap 生产签名要用的参数串
     * @return
     */
    public static boolean checkSign(String sign,String key,Map<String, Object> parammap) {
      /*  String str = FilterUtil.createLinkString(parammap);
        MD5Salt md5Salt = new MD5Salt(key, "MD5");
        String newsign = md5Salt.encode(str);
        if(!newsign.equals(sign)){
        	return false;
        }else{
        	return true;
        }*/
    	String str = FilterUtil.createLinkString(parammap);
    	logger.info("参与签名字符串：--"+str);
    	return MD5.verify(str, sign, key, "UTF-8");
    }
    
}
