package com.github.wxiaoqi.security.common.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class SequenceUtil {
    
    /**
     * 生成27位序列号
     * @return
     */
    public static String Create27DigitalOrAlphabet() {
        String SeqNo = null;
        
        Random ne=new Random();//实例化一个random的对象ne
        int fiveNo1 = ne.nextInt(99999-10000+1)+10000;//为变量赋随机值10000-99999
        int fiveNo2 = ne.nextInt(99999-10000+1)+10000;//为变量赋随机值10000-99999
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        SeqNo = (sdf.format(new Date()) + fiveNo1) + fiveNo2;
        
        return SeqNo;
    }
	/**
	 * 中投宝易互通-生成32位序列号
	 * @param paramStr 商户号
	 * @return
	 */
	public static String CreateUmbpaySequence(String paramStr) {
		String SeqNo = null;
		
		Random ne=new Random();//实例化一个random的对象ne
        int sixNo = ne.nextInt(999999-100000+1)+100000;//为变量赋随机值100000-999999
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		SeqNo = (paramStr + sdf.format(new Date())) + sixNo;
		
		return SeqNo;
	}

}
