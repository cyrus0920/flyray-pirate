package com.github.wxiaoqi.security.common.cms.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("购物车操作参数")
public class RestaurantShoppingOperatingParam extends BaseParam{
	
	@NotNull(message="用户账号不能为空")
	@ApiModelProperty("用户账号")
	private String perId;
	
	@NotNull(message="业务类型不能为空")
	@ApiModelProperty("业务类型")
	private String type;
	
	@NotNull(message="餐品编号不能为空")
	@ApiModelProperty("餐品编号")
	private String dishesId;

}
