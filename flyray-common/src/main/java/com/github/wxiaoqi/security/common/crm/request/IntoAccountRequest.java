package com.github.wxiaoqi.security.common.crm.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("第三方支付成功后调用的入账接口参数")
public class IntoAccountRequest {
	@NotNull(message = "平台编号不能为空")
	@ApiModelProperty("平台编号")
	private String platformId;
	
	@NotNull(message = "个人客户编号不能为空")
	@ApiModelProperty("个人客户编号")
	private String personalId;
	
	@NotNull(message = "订单号不能为空")
	@ApiModelProperty("订单号")
	private String orderNo;
	
	@NotNull(message = "交易金额不能为空")
	@ApiModelProperty("交易金额")
	private String tradeAmt;

	@NotNull(message = "资金来源编号不能为空")
	@ApiModelProperty("资金来源编号")
	private String sellerId;
	
	@NotNull(message = "商品说明不能为空")
	@ApiModelProperty("商品说明")
	private String produceInfo;
	
	@NotNull(message = "付款方式不能为空")
	@ApiModelProperty("付款方式")
	private String payWay;
	
	@NotNull(message = "商户编号不能为空")
	@ApiModelProperty("商户编号")
	private String merchantId;
	
	@NotNull(message = "客户类型不能为空")
	@ApiModelProperty("客户类型")
	private String customerType;
	
	@NotNull(message = "账户类型不能为空")
	@ApiModelProperty("账户类型")
	private String accountType;
	
	@NotNull(message = "支付方式不能为空")
	@ApiModelProperty("支付方式")
	private String payType;
	
}
