package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

import com.github.wxiaoqi.security.common.entity.BaseEntity;


/**
 * 个人分销关系
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@Table(name = "personal_distribution_relation")
public class PersonalDistributionRelation extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //序号
    @Id
    private Integer id;
	
	    //平台编号
    @Column(name = "platform_id")
    private String platformId;
	
	    //用户编号
    @Column(name = "personal_id")
    private String personalId;
	
	    //父级编号
    @Column(name = "parent_per_id")
    private String parentPerId;
	
	    //父级级别
    @Column(name = "parent_level")
    private Integer parentLevel;
	
	    //创建时间
    @Column(name = "create_time")
    private Date createTime;
	

	/**
	 * 设置：序号
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：序号
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：用户编号
	 */
	public void setPersonalId(String personalId) {
		this.personalId = personalId;
	}
	/**
	 * 获取：用户编号
	 */
	public String getPersonalId() {
		return personalId;
	}
	/**
	 * 设置：父级编号
	 */
	public void setParentPerId(String parentPerId) {
		this.parentPerId = parentPerId;
	}
	/**
	 * 获取：父级编号
	 */
	public String getParentPerId() {
		return parentPerId;
	}
	/**
	 * 设置：父级级别
	 */
	public void setParentLevel(Integer parentLevel) {
		this.parentLevel = parentLevel;
	}
	/**
	 * 获取：父级级别
	 */
	public Integer getParentLevel() {
		return parentLevel;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
}
