package com.github.wxiaoqi.security.crm.core.biz;

import java.math.BigDecimal;
import java.text.Bidi;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.wxiaoqi.security.auth.common.util.SnowFlake;
import com.github.wxiaoqi.security.common.crm.request.RefundFreezeRequest;
import com.github.wxiaoqi.security.common.crm.request.RefundUnFreezeRequest;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.util.EntityUtils;
import com.github.wxiaoqi.security.crm.core.entity.FreezeJournal;
import com.github.wxiaoqi.security.crm.core.entity.MerchantAccount;
import com.github.wxiaoqi.security.crm.core.entity.MerchantAccountJournal;
import com.github.wxiaoqi.security.crm.core.entity.PersonalBilling;
import com.github.wxiaoqi.security.crm.core.entity.UnfreezeJournal;
import com.github.wxiaoqi.security.crm.core.mapper.FreezeJournalMapper;
import com.github.wxiaoqi.security.crm.core.mapper.MerchantAccountJournalMapper;
import com.github.wxiaoqi.security.crm.core.mapper.MerchantAccountMapper;
import com.github.wxiaoqi.security.crm.core.mapper.PersonalBillingMapper;
import com.github.wxiaoqi.security.crm.core.mapper.UnfreezeJournalMapper;

import lombok.extern.slf4j.Slf4j;

/**
 * 退款相关的操作接口
 */
@Slf4j
@Transactional(rollbackFor = Exception.class)
@Service
public class RefundFreezeUnFreezeBiz {
	@Autowired
	private PersonalBillingMapper personalBillingMapper;
	@Autowired
	private MerchantAccountJournalMapper merchantAccountJournalMapper;
	@Autowired
	private MerchantAccountMapper merchantAccountMapper;
	@Autowired
	private FreezeJournalMapper freezeJournalMapper;
	@Autowired
	private UnfreezeJournalMapper unfreezeJournalMapper;
	/**
	 * 发起退款时 冻结资金的接口 1.商户账户余额扣除，冻结金额增加 2.新增冻结流水记录、个人账单记录
	 */
	public Map<String, Object> freeze(RefundFreezeRequest refundFreezeRequest) {
		log.info("发起退款时 冻结资金 -请求参数：{}" + EntityUtils.beanToMap(refundFreezeRequest));
		Map<String, Object> response = new HashMap<String, Object>();
		/**
		 * 根据订单号获取个人账单中获取支付金额A
		 * 根据订单号从商户账户流水表中获取商户退款出账合计金额B 
		 * 本次请求冻结金额C 若(B+C)>A
		 * 则不能再次冻结本次订单
		 */
		PersonalBilling perBill = new PersonalBilling();
		perBill.setPlatformId(refundFreezeRequest.getPlatformId());
		perBill.setPersonalId(refundFreezeRequest.getPersonalId());
		perBill.setOrderNo(refundFreezeRequest.getPayOrderNo());
		perBill.setInOutFlag("2");//出账
		perBill.setStatus("00");// 支付成功
		PersonalBilling perPayBill=personalBillingMapper.selectOne(perBill);
		if(null!=perPayBill){
		/***
		 * 获取实际出账金额合计
		 * */
		try {
			MerchantAccountJournal merAccountJournal=new MerchantAccountJournal();
			merAccountJournal.setPlatformId(refundFreezeRequest.getPlatformId());
			merAccountJournal.setMerchantId(refundFreezeRequest.getMerchantId());
			merAccountJournal.setOrderNo(refundFreezeRequest.getRefundOrderNo());
			BigDecimal refoundCount=merchantAccountJournalMapper.merAccountRefoundCount(merAccountJournal);
			log.info("发起退款时 冻结资金--订单支付金额"+perPayBill.getTradeAmt()+"--="+"商户账户流水表中实际出账金额合计金额"+refoundCount+"+本次退款需冻结金额"+refundFreezeRequest.getRefoundAmt());
			if((new BigDecimal(refundFreezeRequest.getRefoundAmt()).add(refoundCount)).compareTo(perPayBill.getTradeAmt())>0){
				log.info("发起退款时 冻结资金--支付 订单不存在");
				response.put("code", ResponseCode.FREEZE_ID_NOTEXIST.getCode());
				response.put("msg", ResponseCode.FREEZE_ID_NOTEXIST.getMessage());
				return response;
			}
			/**
			 * 修改商户账号余额和冻结余额
			 * */
			MerchantAccount queryMerchantAccount=new MerchantAccount();
			queryMerchantAccount.setPlatformId(refundFreezeRequest.getPlatformId());
			queryMerchantAccount.setMerchantId(refundFreezeRequest.getMerchantId());
			queryMerchantAccount.setCustomerType(refundFreezeRequest.getCustomerType());
			queryMerchantAccount.setAccountType(refundFreezeRequest.getAccountType());
			MerchantAccount merchantAccount= merchantAccountMapper.selectOne(queryMerchantAccount);
			if(null!=merchantAccount){
				merchantAccount.setAccountBalance(queryMerchantAccount.getAccountBalance().subtract(new BigDecimal(refundFreezeRequest.getRefoundAmt())));
				merchantAccount.setFreezeBalance(queryMerchantAccount.getFreezeBalance().add(new BigDecimal(refundFreezeRequest.getRefoundAmt())));
				merchantAccountMapper.updateByPrimaryKeySelective(merchantAccount);
			}else{
				log.info("发起退款时 冻结资金--商户账户不存在");
				response.put("code", ResponseCode.MER_ACC_NOTEXIST.getCode());
				response.put("msg", ResponseCode.MER_ACC_NOTEXIST.getMessage());
				return response;
			}
			/***
			 * 新增冻结记录 和个人账单增加退款申请记录
			 * */
			FreezeJournal freezeJournal=new FreezeJournal();
			String freezeId =String.valueOf(SnowFlake.getId());
			freezeJournal.setJournalId(freezeId);
			freezeJournal.setPlatformId(refundFreezeRequest.getPlatformId());
			freezeJournal.setCustomerType(refundFreezeRequest.getCustomerType());
			freezeJournal.setCustomerNo(refundFreezeRequest.getMerchantId());
			freezeJournal.setTradeType("02");//交易类型（支付:01，退款:02，提现:03，充值:04）
			freezeJournal.setOrderNo(refundFreezeRequest.getRefundOrderNo());
			freezeJournal.setFreezeBalance(new BigDecimal(refundFreezeRequest.getRefoundAmt()));
			freezeJournal.setStatus("1");//冻结状态 1：已冻结  2：部分解冻  3：已解冻
			freezeJournal.setCreateTime(new Date());
			freezeJournalMapper.insertSelective(freezeJournal);
			PersonalBilling billing = new PersonalBilling();
			billing.setBillId(String.valueOf(SnowFlake.getId()));
			billing.setPlatformId(refundFreezeRequest.getPlatformId());
			billing.setOrderNo(refundFreezeRequest.getRefundOrderNo());
			//billing.setBillType("");
			billing.setInOutFlag("1");
			billing.setTradeAmt(new BigDecimal(refundFreezeRequest.getRefoundAmt()));
			billing.setSellerType(refundFreezeRequest.getCustomerType());
			billing.setProduceInfo(refundFreezeRequest.getProduceInfo());
			billing.setPayWay(refundFreezeRequest.getPayType());
			billing.setCreateTime(new Date());
			billing.setTradeType("02");//交易类型（支付:01，退款:02，提现:03，充值:04）
			billing.setStatus("02");//账单状态（付款成功 00 ，充值成功 01 ，退款申请成功02，退款成功03， 提现申请成功04 ，提现处理成功 05 ）
			personalBillingMapper.insertSelective(billing);
			response.put("freezeId", freezeId);
			response.put("code", ResponseCode.OK.getCode());
			response.put("msg", ResponseCode.OK.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			log.info("发起退款时 冻结资金报错--"+e.getMessage());
			response.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			response.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			return response;
		}
		}else{
			log.info("发起退款时 冻结资金--支付订单不存在");
			response.put("code", ResponseCode.PER_PAYBILL_NOT_EXIST.getCode());
			response.put("msg", ResponseCode.PER_PAYBILL_NOT_EXIST.getMessage());
			return response;
		}
		return response;
	}
	/**
	 * 退款失败时 解冻资金的接口 
	 * 1.商户账户冻结金额扣除，商户账户余额增加。 
	 * 2.新增解冻流水记录、个人账单原退款申请改成状态改成失败
	 */
	public Map<String, Object> refundUnFreeze(RefundUnFreezeRequest refundUnFreezeRequest) {
		log.info("退款失败时 解冻资金的接口 -请求参数：{}" + EntityUtils.beanToMap(refundUnFreezeRequest));
		Map<String, Object> response = new HashMap<String, Object>();
		/**
		 * 根据冻结流水号查询是否被解冻
		 * */
		UnfreezeJournal queryUnfreezeJournal = new UnfreezeJournal();
		queryUnfreezeJournal.setFreezeId(refundUnFreezeRequest.getFreezeId());
		queryUnfreezeJournal.setOrderNo(refundUnFreezeRequest.getRefundOrderNo());
		UnfreezeJournal unfreezeJournal=unfreezeJournalMapper.selectOne(queryUnfreezeJournal);
		if(null!=unfreezeJournal){
			log.info("退款失败时 解冻资金的接口--冻结资金已解冻");
			response.put("code", ResponseCode.UNFREEZE_IS_EXIST.getCode());
			response.put("msg", ResponseCode.UNFREEZE_IS_EXIST.getMessage());
			return response;
		}
		try {
			MerchantAccount queryMerchantAccount=new MerchantAccount();
			queryMerchantAccount.setPlatformId(refundUnFreezeRequest.getPlatformId());
			queryMerchantAccount.setMerchantId(refundUnFreezeRequest.getMerchantId());
			queryMerchantAccount.setCustomerType(refundUnFreezeRequest.getCustomerType());
			queryMerchantAccount.setAccountType(refundUnFreezeRequest.getAccountType());
			MerchantAccount merchantAccount= merchantAccountMapper.selectOne(queryMerchantAccount);
			if(null!=merchantAccount){
				merchantAccount.setAccountBalance(queryMerchantAccount.getAccountBalance().add(new BigDecimal(refundUnFreezeRequest.getRefoundAmt())));
				merchantAccount.setFreezeBalance(queryMerchantAccount.getFreezeBalance().subtract(new BigDecimal(refundUnFreezeRequest.getRefoundAmt())));
				merchantAccountMapper.updateByPrimaryKeySelective(merchantAccount);
			}else{
				log.info("退款失败时 解冻资金的接口--商户账户不存在");
				response.put("code", ResponseCode.MER_ACC_NOTEXIST.getCode());
				response.put("msg", ResponseCode.MER_ACC_NOTEXIST.getMessage());
				return response;
			}
			/***
			 * 新增解冻流水记录和个人账单增加退款申请改成失败
			 * */
			queryUnfreezeJournal.setJournalId(String.valueOf(SnowFlake.getId()));
			queryUnfreezeJournal.setTradeType("02");
			queryUnfreezeJournal.setUnfreezeBalance(new BigDecimal(refundUnFreezeRequest.getRefoundAmt()));
			queryUnfreezeJournal.setCreateTime(new Date());
			unfreezeJournalMapper.insertSelective(queryUnfreezeJournal);
			PersonalBilling billing = new PersonalBilling();
			billing.setPlatformId(refundUnFreezeRequest.getPlatformId());
			billing.setOrderNo(refundUnFreezeRequest.getRefundOrderNo());
			billing.setPersonalId(refundUnFreezeRequest.getMerchantId());
			billing.setTradeType("02");
			PersonalBilling perBill=personalBillingMapper.selectOne(billing);
			if(null!=perBill){
				perBill.setStatus("03");
				personalBillingMapper.updateByPrimaryKeySelective(perBill);
				response.put("code", ResponseCode.OK.getCode());
				response.put("msg", ResponseCode.OK.getMessage());
			}else{
				log.info("退款失败时 解冻资金的接口--商户账户不存在");
				response.put("code", ResponseCode.PER_REFONDBILL_NOT_EXIST.getCode());
				response.put("msg", ResponseCode.PER_REFONDBILL_NOT_EXIST.getMessage());
				return response;
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.info("发退款失败时 解冻资金的接口报错--"+e.getMessage());
			response.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			response.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			return response;
		}
		return response;
		
	}
}
