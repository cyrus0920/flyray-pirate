package com.github.wxiaoqi.security.crm.core.rest;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.github.wxiaoqi.security.common.crm.request.CustomerBaseRequest;
import com.github.wxiaoqi.security.common.crm.request.QueryCustomerBaseListRequest;
import com.github.wxiaoqi.security.common.crm.request.ResetPasswordRequest;
import com.github.wxiaoqi.security.common.crm.request.UpdateCustomerStatusRequest;
import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.crm.core.biz.CustomerBaseBiz;
import com.github.wxiaoqi.security.crm.core.entity.CustomerBase;

import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

/**
 * 客户信息管理
 * @author centerroot
 * @time 创建时间:2018年7月17日下午1:50:10
 * @description
 */
@Slf4j
@RestController
@RequestMapping("customerBase")
public class CustomerBaseController extends BaseController<CustomerBaseBiz,CustomerBase> {

	@Autowired
	private CustomerBaseBiz customerBaseBiz;
	
	/**
	 * 查询个人基础信息列表
	 * @author centerroot
	 * @time 创建时间:2018年7月16日下午6:02:04
	 * @param queryPersonalBaseListRequest
	 * @return
	 */
	@RequestMapping(value = "/queryList", method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> queryList(@RequestBody @Valid QueryCustomerBaseListRequest queryCustomerBaseListRequest){
		return customerBaseBiz.queryList(queryCustomerBaseListRequest);
	}
	
	/**
	 * 重置密码
	 * @author centerroot
	 * @time 创建时间:2018年7月18日上午11:35:00
	 * @param resetPasswordRequest
	 * @return
	 */
	@RequestMapping(value = "/resetPassword", method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> resetPassword(@RequestBody @Valid ResetPasswordRequest resetPasswordRequest){
		return customerBaseBiz.resetPassword(resetPasswordRequest);
	}
	
	/**
	 * 修改客户状态
	 * @author centerroot
	 * @time 创建时间:2018年7月18日上午11:35:12
	 * @param updateCustomerStatusRequest
	 * @return
	 */
	@RequestMapping(value = "/updateStatus", method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> updateStatus(@RequestBody @Valid UpdateCustomerStatusRequest updateCustomerStatusRequest){
		return customerBaseBiz.updateStatus(updateCustomerStatusRequest);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * 查询单个客户基础信息详情
	 * @author centerroot
	 * @time 创建时间:2018年7月17日上午11:41:36
	 * @param merchantId
	 * @return
	 */
	@RequestMapping(value = "/queryOneByKeys", method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> queryOneByKeys(@ApiParam("客户编号") @RequestParam(value = "customerId", required = true) String customerId){
		return customerBaseBiz.queryOneByKeys(customerId);
	}
	
	/**
	 * 添加客户基础信息
	 * @author centerroot
	 * @time 创建时间:2018年7月16日下午6:02:15
	 * @param CustomerBaseRequest
	 * @return
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> add(@RequestBody @Valid CustomerBaseRequest customerBaseRequest){
		return customerBaseBiz.add(customerBaseRequest);
	}
	
	/**
	 * 修改客户基础信息
	 * @author centerroot
	 * @time 创建时间:2018年7月16日下午6:02:22
	 * @param CustomerBaseRequest
	 * @return
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> update(@RequestBody @Valid CustomerBaseRequest customerBaseRequest){
		return customerBaseBiz.update(customerBaseRequest);
	}
	
	/**
	 * 删除客户基础信息
	 * @author centerroot
	 * @time 创建时间:2018年7月16日下午6:02:30
	 * @param personalId
	 * @return
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> delete(@ApiParam("客户编号") @RequestParam(value = "customerId", required = true) String customerId){
		return customerBaseBiz.delete(customerId);
	}

}