package com.github.wxiaoqi.security.crm.core.biz;

import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.crm.core.entity.PersonalAccount;
import com.github.wxiaoqi.security.crm.core.mapper.PersonalAccountMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

/**
 * 个人账户信息
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@Service
public class PersonalAccountBiz extends BaseBiz<PersonalAccountMapper,PersonalAccount> {
}