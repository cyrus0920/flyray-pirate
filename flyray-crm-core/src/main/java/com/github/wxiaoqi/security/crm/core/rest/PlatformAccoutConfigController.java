package com.github.wxiaoqi.security.crm.core.rest;

import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.crm.core.biz.PlatformAccoutConfigBiz;
import com.github.wxiaoqi.security.crm.core.entity.PlatformAccoutConfig;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("platformAccoutConfig")
public class PlatformAccoutConfigController extends BaseController<PlatformAccoutConfigBiz,PlatformAccoutConfig> {

}