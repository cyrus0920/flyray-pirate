package com.github.wxiaoqi.security.crm.core.biz;

import java.math.BigDecimal;
import java.text.Bidi;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.wxiaoqi.security.auth.common.util.SnowFlake;
import com.github.wxiaoqi.security.common.crm.request.RefundFreezeRequest;
import com.github.wxiaoqi.security.common.crm.request.RefundUnFreezeRequest;
import com.github.wxiaoqi.security.common.crm.request.UnFreAndOutAccountRequest;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.util.EntityUtils;
import com.github.wxiaoqi.security.crm.core.entity.FreezeJournal;
import com.github.wxiaoqi.security.crm.core.entity.MerchantAccount;
import com.github.wxiaoqi.security.crm.core.entity.MerchantAccountJournal;
import com.github.wxiaoqi.security.crm.core.entity.PersonalBilling;
import com.github.wxiaoqi.security.crm.core.entity.UnfreezeJournal;
import com.github.wxiaoqi.security.crm.core.mapper.FreezeJournalMapper;
import com.github.wxiaoqi.security.crm.core.mapper.MerchantAccountJournalMapper;
import com.github.wxiaoqi.security.crm.core.mapper.MerchantAccountMapper;
import com.github.wxiaoqi.security.crm.core.mapper.PersonalBillingMapper;
import com.github.wxiaoqi.security.crm.core.mapper.UnfreezeJournalMapper;

import lombok.extern.slf4j.Slf4j;

/**
 * 退款相关的操作接口
 */
@Slf4j
@Transactional(rollbackFor = Exception.class)
@Service
public class RefundUnFreAndOutAccountBiz {
	@Autowired
	private PersonalBillingMapper personalBillingMapper;
	@Autowired
	private MerchantAccountJournalMapper merchantAccountJournalMapper;
	@Autowired
	private MerchantAccountMapper merchantAccountMapper;
	@Autowired
	private FreezeJournalMapper freezeJournalMapper;
	@Autowired
	private UnfreezeJournalMapper unfreezeJournalMapper;
	/**
	 * 第三方支付-退款成功则需要解冻资金并出账接口
	 * 1.商户账户冻结金额扣除
	 * 2.新增商户流水记录、解冻流水记录
	 * 3.个人账单原退款申请记录置为成功
	 */
	public Map<String, Object> unFreAndOutAccount(UnFreAndOutAccountRequest unFreAndOutAccountRequest) {
		log.info("第三方支付退款成功则需要解冻资金并出账接口-请求参数：{}" + EntityUtils.beanToMap(unFreAndOutAccountRequest));
		Map<String, Object> response = new HashMap<String, Object>();
		/**
		 * 根据冻结流水号查询是否被解冻
		 * */
		UnfreezeJournal queryUnfreezeJournal = new UnfreezeJournal();
		queryUnfreezeJournal.setFreezeId(unFreAndOutAccountRequest.getFreezeId());
		queryUnfreezeJournal.setOrderNo(unFreAndOutAccountRequest.getRefundOrderNo());
		UnfreezeJournal unfreezeJournal=unfreezeJournalMapper.selectOne(queryUnfreezeJournal);
		if(null!=unfreezeJournal){
			log.info("第三方支付-退款成功则需要解冻资金并出账接口--冻结资金已解冻");
			response.put("code", ResponseCode.UNFREEZE_IS_EXIST.getCode());
			response.put("msg", ResponseCode.UNFREEZE_IS_EXIST.getMessage());
			return response;
		}
		MerchantAccount queryMerchantAccount=new MerchantAccount();
		queryMerchantAccount.setPlatformId(unFreAndOutAccountRequest.getPlatformId());
		queryMerchantAccount.setMerchantId(unFreAndOutAccountRequest.getMerchantId());
		queryMerchantAccount.setCustomerType(unFreAndOutAccountRequest.getCustomerType());
		queryMerchantAccount.setAccountType(unFreAndOutAccountRequest.getAccountType());
		MerchantAccount merchantAccount= merchantAccountMapper.selectOne(queryMerchantAccount);
		if(null!=merchantAccount){
			merchantAccount.setFreezeBalance(queryMerchantAccount.getFreezeBalance().subtract(new BigDecimal(unFreAndOutAccountRequest.getRefoundAmt())));
			merchantAccountMapper.updateByPrimaryKeySelective(merchantAccount);
		}else{
			log.info("第三方支付-退款成功则需要解冻资金并出账接口--商户账户不存在");
			response.put("code", ResponseCode.MER_ACC_NOTEXIST.getCode());
			response.put("msg", ResponseCode.MER_ACC_NOTEXIST.getMessage());
			return response;
		}
		
		
		
		return response;
	}
	/**
	 * 余额账户支付-退款成功则需要解冻资金并出账接口
	 * 1.商户账户冻结金额扣除、个人账户余额增加
	 * 2.新增商户流水记录、解冻流水记录
	 * 3.个人账单原退款申请记录置为成功
	 * 4.新增个人账户流水记录
	 */
	public Map<String, Object> balanceUnFreAndOutAccount(UnFreAndOutAccountRequest unFreAndOutAccountRequest) {
		log.info("余额账户支付退款成功则需要解冻资金并出账接口-请求参数：{}" + EntityUtils.beanToMap(unFreAndOutAccountRequest));
		Map<String, Object> response = new HashMap<String, Object>();
		return response;
		
	}
}
