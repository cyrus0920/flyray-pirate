package com.github.wxiaoqi.security.crm.core.rest;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.github.wxiaoqi.security.common.crm.request.PersonalBaseRequest;
import com.github.wxiaoqi.security.common.crm.request.QueryPersonalBaseListRequest;
import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.crm.core.biz.PersonalBaseBiz;
import com.github.wxiaoqi.security.crm.core.entity.PersonalBase;

/**
 * 个人基础信息
 * @author centerroot
 * @time 创建时间:2018年7月16日下午6:01:37
 * @description
 */
@RestController
@RequestMapping("personalBase")
public class PersonalBaseController extends BaseController<PersonalBaseBiz,PersonalBase> {
	
	@Autowired
	private PersonalBaseBiz personalBaseBiz;
	
	/**
	 * 查询个人基础信息列表
	 * @author centerroot
	 * @time 创建时间:2018年7月16日下午6:02:04
	 * @param queryPersonalBaseListRequest
	 * @return
	 */
	@RequestMapping(value = "/queryList", method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> queryList(@RequestBody @Valid QueryPersonalBaseListRequest queryPersonalBaseListRequest){
		return personalBaseBiz.queryList(queryPersonalBaseListRequest);
	}
	
	/**
	 * 添加个人基础信息
	 * @author centerroot
	 * @time 创建时间:2018年7月16日下午6:02:15
	 * @param personalBaseRequest
	 * @return
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> add(@RequestBody @Valid PersonalBaseRequest personalBaseRequest){
		return personalBaseBiz.add(personalBaseRequest);
	}
}