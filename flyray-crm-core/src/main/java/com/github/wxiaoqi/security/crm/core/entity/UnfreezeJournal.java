package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

import com.github.wxiaoqi.security.common.entity.BaseEntity;


/**
 * 解冻流水表
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@Table(name = "unfreeze_journal")
public class UnfreezeJournal extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //流水号
    @Id
    private String journalId;
	
	    //冻结流水号
    @Column(name = "freeze_id")
    private String freezeId;
	
	    //订单号
    @Column(name = "order_no")
    private String orderNo;
	
	    //交易类型  01：充值，02：提现，
    @Column(name = "trade_type")
    private String tradeType;
	
	    //解冻金额
    @Column(name = "unfreeze_balance")
    private BigDecimal unfreezeBalance;
	
	    //创建时间
    @Column(name = "create_time")
    private Date createTime;
	

	/**
	 * 设置：流水号
	 */
	public void setJournalId(String journalId) {
		this.journalId = journalId;
	}
	/**
	 * 获取：流水号
	 */
	public String getJournalId() {
		return journalId;
	}
	/**
	 * 设置：冻结流水号
	 */
	public void setFreezeId(String freezeId) {
		this.freezeId = freezeId;
	}
	/**
	 * 获取：冻结流水号
	 */
	public String getFreezeId() {
		return freezeId;
	}
	/**
	 * 设置：订单号
	 */
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	/**
	 * 获取：订单号
	 */
	public String getOrderNo() {
		return orderNo;
	}
	/**
	 * 设置：交易类型  01：充值，02：提现，
	 */
	public void setTradeType(String tradeType) {
		this.tradeType = tradeType;
	}
	/**
	 * 获取：交易类型  01：充值，02：提现，
	 */
	public String getTradeType() {
		return tradeType;
	}
	/**
	 * 设置：解冻金额
	 */
	public void setUnfreezeBalance(BigDecimal unfreezeBalance) {
		this.unfreezeBalance = unfreezeBalance;
	}
	/**
	 * 获取：解冻金额
	 */
	public BigDecimal getUnfreezeBalance() {
		return unfreezeBalance;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
}
