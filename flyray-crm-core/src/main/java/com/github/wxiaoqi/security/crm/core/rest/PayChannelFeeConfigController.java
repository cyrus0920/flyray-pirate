package com.github.wxiaoqi.security.crm.core.rest;

import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.crm.core.biz.PayChannelFeeConfigBiz;
import com.github.wxiaoqi.security.crm.core.entity.PayChannelFeeConfig;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("payChannelFeeConfig")
public class PayChannelFeeConfigController extends BaseController<PayChannelFeeConfigBiz,PayChannelFeeConfig> {

}