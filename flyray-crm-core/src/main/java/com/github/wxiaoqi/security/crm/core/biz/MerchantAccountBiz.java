package com.github.wxiaoqi.security.crm.core.biz;

import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.crm.core.entity.MerchantAccount;
import com.github.wxiaoqi.security.crm.core.mapper.MerchantAccountMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

/**
 * 商户账户信息
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@Service
public class MerchantAccountBiz extends BaseBiz<MerchantAccountMapper,MerchantAccount> {
}