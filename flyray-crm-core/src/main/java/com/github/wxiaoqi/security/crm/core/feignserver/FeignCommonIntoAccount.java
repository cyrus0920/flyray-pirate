package com.github.wxiaoqi.security.crm.core.feignserver;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.common.crm.request.IntoAccountRequest;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.util.EntityUtils;
import com.github.wxiaoqi.security.crm.core.biz.CommonIntoAccountBiz;
import com.github.wxiaoqi.security.crm.core.biz.PayChannelConfigBiz;
import com.github.wxiaoqi.security.crm.core.entity.PayChannelConfig;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/***
 * 支付成功后调用入账相关的接口
 * */
@Api(tags="支付成功后调用的入账接口")
@Controller
@RequestMapping("feign")
public class FeignCommonIntoAccount {
	
	@Autowired
	private CommonIntoAccountBiz commonIntoAccountBiz;
	
	/**
	 * @param 
	 * @return
	 */
	@ApiOperation("第三方支付成功后调用的入账接口")
	@RequestMapping(value = "/intoAccount",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> intoAccount(@RequestBody @Valid IntoAccountRequest intoAccountRequest){
		Map<String, Object> response = commonIntoAccountBiz.intoAccount(intoAccountRequest);
		return response;
    }
	
	/**
	 * @param param
	 * @return
	 */
	@ApiOperation("账户余额支付成功后调用的入账接口")
	@RequestMapping(value = "/balancePayintoAccount",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> balancePayintoAccount(@RequestBody @Valid IntoAccountRequest intoAccountRequest){
		Map<String, Object> response = commonIntoAccountBiz.balancePayintoAccount(intoAccountRequest);
		return response;
    }
	
}
