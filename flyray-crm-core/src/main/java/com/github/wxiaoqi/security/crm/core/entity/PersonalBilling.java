package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

import com.github.wxiaoqi.security.common.entity.BaseEntity;


/**
 * 个人账单
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@Table(name = "personal_billing")
public class PersonalBilling extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //账单编号
    @Id
    private String billId;
	
	    //平台编号
    @Column(name = "platform_id")
    private String platformId;
	
	    //个人信息编号
    @Column(name = "personal_id")
    private String personalId;
	
	    //账务流水号
    @Column(name = "account_journal_id")
    private String accountJournalId;
	
	    //订单号
    @Column(name = "order_no")
    private String orderNo;
	
	    //账单类型    饮食，服装，美容，生活用品，保险，理财
    @Column(name = "bill_type")
    private String billType;
	
	    //来往标志  1：来账   2：往账
    @Column(name = "in_out_flag")
    private String inOutFlag;
	
	    //交易金额
    @Column(name = "trade_amt")
    private BigDecimal tradeAmt;
	
	    //资金来源编号
    @Column(name = "seller_id")
    private String sellerId;
	
	    //客户类型    CUST00：平台   CUST01：商户  CUST02：用户   CUST03：第三方
    @Column(name = "seller_type")
    private String sellerType;
	
	    //商品说明
    @Column(name = "produce_info")
    private String produceInfo;
	
	    //付款方式     账户类型+银行卡
    @Column(name = "pay_way")
    private String payWay;
	
	    //备注
    @Column(name = "remark")
    private String remark;
	
	    //创建时间
    @Column(name = "create_time")
    private Date createTime;
    
    /**
     * 
     * 新增 交易类型trade_type 交易类型（支付:01，退款:02，提现:03，充值:04）
     * */
    @Column(name = "trade_type")
    private String tradeType;
    
    /**
     *新增 账单状态（付款成功 00 ，充值成功 01 ，退款申请成功02，退款成功03， 提现申请成功04 ，提现处理成功 05 ）
     *
     * */
    @Column(name = "status")
    private String status;

    
	public String getTradeType() {
		return tradeType;
	}
	public void setTradeType(String tradeType) {
		this.tradeType = tradeType;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * 设置：账单编号
	 */
	public void setBillId(String billId) {
		this.billId = billId;
	}
	/**
	 * 获取：账单编号
	 */
	public String getBillId() {
		return billId;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：个人信息编号
	 */
	public void setPersonalId(String personalId) {
		this.personalId = personalId;
	}
	/**
	 * 获取：个人信息编号
	 */
	public String getPersonalId() {
		return personalId;
	}
	/**
	 * 设置：账务流水号
	 */
	public void setAccountJournalId(String accountJournalId) {
		this.accountJournalId = accountJournalId;
	}
	/**
	 * 获取：账务流水号
	 */
	public String getAccountJournalId() {
		return accountJournalId;
	}
	/**
	 * 设置：订单号
	 */
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	/**
	 * 获取：订单号
	 */
	public String getOrderNo() {
		return orderNo;
	}
	/**
	 * 设置：账单类型    饮食，服装，美容，生活用品，保险，理财
	 */
	public void setBillType(String billType) {
		this.billType = billType;
	}
	/**
	 * 获取：账单类型    饮食，服装，美容，生活用品，保险，理财
	 */
	public String getBillType() {
		return billType;
	}
	/**
	 * 设置：来往标志  1：来账   2：往账
	 */
	public void setInOutFlag(String inOutFlag) {
		this.inOutFlag = inOutFlag;
	}
	/**
	 * 获取：来往标志  1：来账   2：往账
	 */
	public String getInOutFlag() {
		return inOutFlag;
	}
	/**
	 * 设置：交易金额
	 */
	public void setTradeAmt(BigDecimal tradeAmt) {
		this.tradeAmt = tradeAmt;
	}
	/**
	 * 获取：交易金额
	 */
	public BigDecimal getTradeAmt() {
		return tradeAmt;
	}
	/**
	 * 设置：资金来源编号
	 */
	public void setSellerId(String sellerId) {
		this.sellerId = sellerId;
	}
	/**
	 * 获取：资金来源编号
	 */
	public String getSellerId() {
		return sellerId;
	}
	/**
	 * 设置：客户类型    CUST00：平台   CUST01：商户  CUST02：用户   CUST03：第三方
	 */
	public void setSellerType(String sellerType) {
		this.sellerType = sellerType;
	}
	/**
	 * 获取：客户类型    CUST00：平台   CUST01：商户  CUST02：用户   CUST03：第三方
	 */
	public String getSellerType() {
		return sellerType;
	}
	/**
	 * 设置：商品说明
	 */
	public void setProduceInfo(String produceInfo) {
		this.produceInfo = produceInfo;
	}
	/**
	 * 获取：商品说明
	 */
	public String getProduceInfo() {
		return produceInfo;
	}
	/**
	 * 设置：付款方式     账户类型+银行卡
	 */
	public void setPayWay(String payWay) {
		this.payWay = payWay;
	}
	/**
	 * 获取：付款方式     账户类型+银行卡
	 */
	public String getPayWay() {
		return payWay;
	}
	/**
	 * 设置：备注
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * 获取：备注
	 */
	public String getRemark() {
		return remark;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
}
