package com.github.wxiaoqi.security.crm.core.biz;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.crm.request.PersonalBaseRequest;
import com.github.wxiaoqi.security.common.crm.request.QueryPersonalBaseListRequest;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.util.EntityUtils;
import com.github.wxiaoqi.security.common.util.Query;
import com.github.wxiaoqi.security.common.util.SnowFlake;
import com.github.wxiaoqi.security.crm.core.entity.PersonalBase;
import com.github.wxiaoqi.security.crm.core.mapper.PersonalBaseMapper;

import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * 个人客户基础信息
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@Slf4j
@Service
public class PersonalBaseBiz extends BaseBiz<PersonalBaseMapper,PersonalBase> {
	
	/**
	 * 查询个人基础信息列表
	 * @author centerroot
	 * @time 创建时间:2018年7月16日下午6:02:38
	 * @param queryPersonalBaseListRequest
	 * @return
	 */
	public Map<String, Object> queryList(QueryPersonalBaseListRequest queryPersonalBaseListRequest){
		log.info("【查询个人基础信息列表】   请求参数：{}",EntityUtils.beanToMap(queryPersonalBaseListRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		String platformId = queryPersonalBaseListRequest.getPlatformId();
		String personalId = queryPersonalBaseListRequest.getPersonalId();
		String realName = queryPersonalBaseListRequest.getRealName();
		String authenticationStatus = queryPersonalBaseListRequest.getAuthenticationStatus();
		String status = queryPersonalBaseListRequest.getStatus();
		int page = queryPersonalBaseListRequest.getPage();
		int limit = queryPersonalBaseListRequest.getLimit();
		Map<String, Object> params = new HashMap<String, Object>();
	    params.put("page", page);
	    params.put("limit", limit);
	    Query query = new Query(params);
	    Page<Object> result = PageHelper.startPage(query.getPage(), query.getLimit());
		
		Example example = new Example(PersonalBase.class);
        Criteria criteria = example.createCriteria();
        if (!StringUtils.isEmpty(platformId)) {
        	criteria.andEqualTo("platformId", platformId);
		}
        if (!StringUtils.isEmpty(personalId)) {
        	criteria.andEqualTo("personalId", personalId);
		}
        if (!StringUtils.isEmpty(realName)) {
        	criteria.andEqualTo("realName", realName);
		}
        if (!StringUtils.isEmpty(authenticationStatus)) {
        	criteria.andEqualTo("authenticationStatus", authenticationStatus);
		}
        if (!StringUtils.isEmpty(status)) {
        	criteria.andEqualTo("status", status);
		}
        List<PersonalBase> list = mapper.selectByExample(example);
        
        TableResultResponse<PersonalBase> table = new TableResultResponse<PersonalBase>(result.getTotal(), list);
        respMap.put("body", table);
        respMap.put("code", ResponseCode.OK.getCode());
        respMap.put("msg", ResponseCode.OK.getMessage());
		log.info("【查询个人基础信息列表】   响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 添加个人基础信息
	 * @author centerroot
	 * @time 创建时间:2018年7月16日下午6:02:52
	 * @param personalBaseRequest
	 * @return
	 */
	public Map<String, Object> add(PersonalBaseRequest personalBaseRequest){
		log.info("【添加个人基础信息】   请求参数：{}",EntityUtils.beanToMap(personalBaseRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		PersonalBase personalBase = new PersonalBase();
		BeanUtils.copyProperties(personalBaseRequest,personalBase);
		String personalId = String.valueOf(SnowFlake.getId());
		personalBase.setPersonalId(personalId);
		personalBase.setAuthenticationStatus("00");
		personalBase.setStatus("00");
		
		mapper.insert(personalBase);
		respMap.put("code", ResponseCode.OK.getCode());
        respMap.put("msg", ResponseCode.OK.getMessage());
		log.info("【添加个人基础信息】   响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 小程序用户新增
	 * @param request
	 * @return
	 */
	public Map<String, Object> wechatMiniProgramAdd(Map<String, Object> request){
		log.info("【小程序用户新增】   请求参数：{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platformId = (String) request.get("platformId");
		String customerId = (String) request.get("customerId");
		String avatar = (String) request.get("avatar");
		String nickName = (String) request.get("nickName");
		PersonalBase personalBase = new PersonalBase();
		personalBase.setPlatformId(platformId);
		personalBase.setCustomerId(customerId);
		PersonalBase selectPersonalBase = mapper.selectOne(personalBase);
		if(null == selectPersonalBase){
			personalBase.setPersonalId(String.valueOf(SnowFlake.getId()));
			personalBase.setAvatar(avatar);
			personalBase.setNickName(nickName);
			mapper.insert(personalBase);
		}else{
			selectPersonalBase.setAvatar(avatar);
			selectPersonalBase.setNickName(nickName);
			mapper.updateByPrimaryKey(selectPersonalBase);
		}
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		log.info("【小程序用户新增】   响应参数：{}", response);
		return response;
	}
	
	/**
	 * 小程序用户修改
	 * @param request
	 * @return
	 */
	public Map<String, Object> wechatMiniProgramUpdate(Map<String, Object> request){
		log.info("【小程序用户修改】   请求参数：{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platformId = (String) request.get("platformId");
		String customerId = (String) request.get("customerId");
		String avatar = (String) request.get("avatar");
		String nickName = (String) request.get("nickName");
		PersonalBase personalBase = new PersonalBase();
		personalBase.setPlatformId(platformId);
		personalBase.setCustomerId(customerId);
		PersonalBase selectPersonalBase = mapper.selectOne(personalBase);
		if(null == selectPersonalBase){
			response.put("code", ResponseCode.CUST_NOTEXIST.getCode());
			response.put("msg", ResponseCode.CUST_NOTEXIST.getMessage());
		}else{
			selectPersonalBase.setAvatar(avatar);
			selectPersonalBase.setNickName(nickName);
			mapper.updateByPrimaryKey(selectPersonalBase);
			response.put("code", ResponseCode.OK.getCode());
			response.put("msg", ResponseCode.OK.getMessage());
		}
		log.info("【小程序用户修改响应】   响应参数：{}", response);
		return response;
	}
	
}