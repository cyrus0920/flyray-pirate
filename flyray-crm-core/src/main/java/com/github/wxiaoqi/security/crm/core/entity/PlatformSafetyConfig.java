package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

import com.github.wxiaoqi.security.common.entity.BaseEntity;


/**
 * 平台安全配置信息
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@Table(name = "platform_safety_config")
public class PlatformSafetyConfig extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //序号
    @Id
    private Integer id;
	
	    //平台编号
    @Column(name = "platform_id")
    private String platformId;
	
	    //加密类型  00：报文加密  01：身份证加密  02：银行卡加密  03：登录密码加密  04：支付密码加密
    @Column(name = "encryption_type")
    private String encryptionType;
	
	    //加密方式   MD5 RSA SHA256 ...
    @Column(name = "encryption_method")
    private String encryptionMethod;
	
	    //密钥（盐值）
    @Column(name = "salt_key")
    private String saltKey;
	
	    //公钥
    @Column(name = "public_key")
    private String publicKey;
	
	    //私钥
    @Column(name = "private_key")
    private String privateKey;
	
	    //创建时间
    @Column(name = "create_time")
    private Date createTime;
	
	    //更新时间
    @Column(name = "update_time")
    private Date updateTime;
	

	/**
	 * 设置：序号
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：序号
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：加密类型  00：报文加密  01：身份证加密  02：银行卡加密  03：登录密码加密  04：支付密码加密
	 */
	public void setEncryptionType(String encryptionType) {
		this.encryptionType = encryptionType;
	}
	/**
	 * 获取：加密类型  00：报文加密  01：身份证加密  02：银行卡加密  03：登录密码加密  04：支付密码加密
	 */
	public String getEncryptionType() {
		return encryptionType;
	}
	/**
	 * 设置：加密方式   MD5 RSA SHA256 ...
	 */
	public void setEncryptionMethod(String encryptionMethod) {
		this.encryptionMethod = encryptionMethod;
	}
	/**
	 * 获取：加密方式   MD5 RSA SHA256 ...
	 */
	public String getEncryptionMethod() {
		return encryptionMethod;
	}
	/**
	 * 设置：密钥（盐值）
	 */
	public void setSaltKey(String saltKey) {
		this.saltKey = saltKey;
	}
	/**
	 * 获取：密钥（盐值）
	 */
	public String getSaltKey() {
		return saltKey;
	}
	/**
	 * 设置：公钥
	 */
	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}
	/**
	 * 获取：公钥
	 */
	public String getPublicKey() {
		return publicKey;
	}
	/**
	 * 设置：私钥
	 */
	public void setPrivateKey(String privateKey) {
		this.privateKey = privateKey;
	}
	/**
	 * 获取：私钥
	 */
	public String getPrivateKey() {
		return privateKey;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：更新时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	/**
	 * 获取：更新时间
	 */
	public Date getUpdateTime() {
		return updateTime;
	}
}
