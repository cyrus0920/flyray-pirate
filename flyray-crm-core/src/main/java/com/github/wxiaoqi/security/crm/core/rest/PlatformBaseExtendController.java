package com.github.wxiaoqi.security.crm.core.rest;

import com.github.wxiaoqi.security.common.crm.request.PlatformBaseExtendRequest;
import com.github.wxiaoqi.security.common.msg.ObjectRestResponse;
import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.crm.core.biz.PlatformBaseExtendBiz;
import com.github.wxiaoqi.security.crm.core.entity.PlatformBase;
import com.github.wxiaoqi.security.crm.core.entity.PlatformBaseExtend;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@Controller
@RequestMapping("platformBaseExtend")
public class PlatformBaseExtendController extends BaseController<PlatformBaseExtendBiz,PlatformBaseExtend> {
	
	@Autowired
	private PlatformBaseExtendBiz platformBaseExtendBiz;
	/**
	 * 添加平台拓展信息
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<PlatformBaseExtend> addPlatformBaseExtend(@RequestBody PlatformBaseExtend entity) throws Exception {
		
		platformBaseExtendBiz.addPlatformBaseExtend(entity);
        return ResponseEntity.ok(entity);
    }
	/**
	 * 接收图片文件
	 */
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> upload(HttpServletRequest request) {
		return platformBaseExtendBiz.upload(request);
	}
	/**
	 * 详情
	 */
	@RequestMapping(value = "/queryInfo/{platformId}", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> queryInfo(@PathVariable String platformId) {
		return platformBaseExtendBiz.queryInfo(platformId);
	}
	/**
	 * 修改
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	public void update(@RequestBody PlatformBaseExtendRequest entity) {
		platformBaseExtendBiz.updatePlatformBase(entity);
	}
}