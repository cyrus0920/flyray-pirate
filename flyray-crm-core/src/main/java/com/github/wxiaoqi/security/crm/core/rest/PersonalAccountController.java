package com.github.wxiaoqi.security.crm.core.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.crm.core.biz.PersonalAccountBiz;
import com.github.wxiaoqi.security.crm.core.entity.PersonalAccount;

@RestController
@RequestMapping("personalAccount")
public class PersonalAccountController extends BaseController<PersonalAccountBiz,PersonalAccount> {

}