package com.github.wxiaoqi.security.crm.core.biz;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestParam;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.crm.request.CustomerBaseRequest;
import com.github.wxiaoqi.security.common.crm.request.QueryCustomerBaseListRequest;
import com.github.wxiaoqi.security.common.crm.request.ResetPasswordRequest;
import com.github.wxiaoqi.security.common.crm.request.UpdateCustomerStatusRequest;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.util.EntityUtils;
import com.github.wxiaoqi.security.common.util.Query;
import com.github.wxiaoqi.security.crm.core.entity.CustomerBase;
import com.github.wxiaoqi.security.crm.core.entity.CustomerBaseAuths;
import com.github.wxiaoqi.security.crm.core.entity.PersonalBase;
import com.github.wxiaoqi.security.crm.core.mapper.CustomerBaseAuthsMapper;
import com.github.wxiaoqi.security.crm.core.mapper.CustomerBaseMapper;

import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * 客户基础信息
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@Slf4j
@Service
public class CustomerBaseBiz extends BaseBiz<CustomerBaseMapper,CustomerBase> {

	@Autowired
	public CustomerBaseAuthsMapper customerBaseAuthsMapper;
	
	/**
	 * 查询客户基础信息列表
	 * @author centerroot
	 * @time 创建时间:2018年7月16日下午6:02:38
	 * @param queryPersonalBaseListRequest
	 * @return
	 */
	public Map<String, Object> queryList(QueryCustomerBaseListRequest queryCustomerBaseListRequest){
		log.info("【查询客户基础信息列表】   请求参数：{}",EntityUtils.beanToMap(queryCustomerBaseListRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		String platformId = queryCustomerBaseListRequest.getPlatformId();
		String customerId = queryCustomerBaseListRequest.getCustomerId();
		String authenticationStatus = queryCustomerBaseListRequest.getAuthenticationStatus();
		String status = queryCustomerBaseListRequest.getStatus();
		int page = queryCustomerBaseListRequest.getPage();
		int limit = queryCustomerBaseListRequest.getLimit();
		Map<String, Object> params = new HashMap<String, Object>();
	    params.put("page", page);
	    params.put("limit", limit);
	    Query query = new Query(params);
	    Page<Object> result = PageHelper.startPage(query.getPage(), query.getLimit());
		
		Example example = new Example(PersonalBase.class);
        Criteria criteria = example.createCriteria();
        if (!StringUtils.isEmpty(platformId)) {
        	criteria.andEqualTo("platformId", platformId);
		}
        if (!StringUtils.isEmpty(customerId)) {
        	criteria.andEqualTo("customerId", customerId);
		}
        if (!StringUtils.isEmpty(authenticationStatus)) {
        	criteria.andEqualTo("authenticationStatus", authenticationStatus);
		}
        if (!StringUtils.isEmpty(status)) {
        	criteria.andEqualTo("status", status);
		}
        List<CustomerBase> list = mapper.selectByExample(example);
        
        TableResultResponse<CustomerBase> table = new TableResultResponse<CustomerBase>(result.getTotal(), list);
        respMap.put("body", table);
        respMap.put("code", ResponseCode.OK.getCode());
        respMap.put("msg", ResponseCode.OK.getMessage());
		log.info("【查询客户基础信息列表】   响应参数：{}",respMap);
		return respMap;
	}
	
	
	
	/**
	 * 重置密码
	 * @author centerroot
	 * @time 创建时间:2018年7月18日上午11:35:00
	 * @param resetPasswordRequest
	 * @return
	 */
	public Map<String, Object> resetPassword(ResetPasswordRequest resetPasswordRequest){
		log.info("【重置密码】   请求参数：{}",EntityUtils.beanToMap(resetPasswordRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		String customerId = resetPasswordRequest.getCustomerId();
		String phone = resetPasswordRequest.getPhone();
		String password = "123456";
		int count = 0;
		if ("0".equals(resetPasswordRequest.getType())) {
			// 登录密码
			CustomerBaseAuths customerBaseAuths = new CustomerBaseAuths();
			customerBaseAuths.setCredential(password);
			
			Example example = new Example(CustomerBaseAuths.class);
	        Criteria criteria = example.createCriteria();
	        criteria.andEqualTo("customerId", customerId);
	        criteria.andEqualTo("authType", "00");// 00:登录授权
	        criteria.andEqualTo("isthird", "00");// 00:站内登录
	        count = customerBaseAuthsMapper.updateByExampleSelective(customerBaseAuths, example);
			
		}else if ("1".equals(resetPasswordRequest.getType())) {
			// 支付密码
			CustomerBase customerBase = mapper.selectByPrimaryKey(customerId);
			customerBase.setPayPassword(password);
			customerBase.setUpdateTime(new Timestamp(System.currentTimeMillis()));
			count = mapper.updateByPrimaryKey(customerBase);
		}
		
		if (count > 0) {
			respMap.put("code", ResponseCode.OK.getCode());
	        respMap.put("msg", ResponseCode.OK.getMessage());
		}else{
			respMap.put("code", ResponseCode.PWD_RESET_ERROR.getCode());
	        respMap.put("msg", ResponseCode.PWD_RESET_ERROR.getMessage());
		}
		
		log.info("【重置密码】   响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 修改客户状态
	 * @author centerroot
	 * @time 创建时间:2018年7月18日上午11:35:12
	 * @param updateCustomerStatusRequest
	 * @return
	 */
	public Map<String, Object> updateStatus(UpdateCustomerStatusRequest updateCustomerStatusRequest){
		log.info("【修改客户状态】   请求参数：{}",EntityUtils.beanToMap(updateCustomerStatusRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		String status = updateCustomerStatusRequest.getStatus();
		CustomerBase customerBase = new CustomerBase();
		customerBase.setCustomerId(updateCustomerStatusRequest.getCustomerId());
		customerBase.setUpdateTime(new Timestamp(System.currentTimeMillis()));
		if ("2".equals(updateCustomerStatusRequest.getType())) {
			// 登录密码状态
			customerBase.setPasswordStatus(status);
			mapper.updateByPrimaryKeySelective(customerBase);
		} else if ("3".equals(updateCustomerStatusRequest.getType())) {
			// 支付密码状态
			customerBase.setPayPasswordStatus(status);
			mapper.updateByPrimaryKeySelective(customerBase);
		} else if ("4".equals(updateCustomerStatusRequest.getType())) {
			// 客户状态
			customerBase.setStatus(status);
			mapper.updateByPrimaryKeySelective(customerBase);
		}
		respMap.put("code", ResponseCode.OK.getCode());
        respMap.put("msg", ResponseCode.OK.getMessage());
		log.info("【修改客户状态】   响应参数：{}",respMap);
		return respMap;
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * 查询单个客户基础信息详情
	 * @author centerroot
	 * @time 创建时间:2018年7月17日上午11:41:36
	 * @param merchantId
	 * @return
	 */
	public Map<String, Object> queryOneByKeys(@ApiParam("客户编号") @RequestParam(value = "customerId", required = true) String customerId){
		log.info("【查询单个客户基础信息详情】   请求参数：{}",customerId);
		Map<String, Object> respMap = new HashMap<String, Object>();
		CustomerBase customerBase = mapper.selectByPrimaryKey(customerId);
		respMap.put("CustomerBase", customerBase);
        respMap.put("code", ResponseCode.OK.getCode());
        respMap.put("msg", ResponseCode.OK.getMessage());
		log.info("【查询单个客户基础信息详情】   响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 添加客户基础信息
	 * @author centerroot
	 * @time 创建时间:2018年7月16日下午6:02:15
	 * @param CustomerBaseRequest
	 * @return
	 */
	public Map<String, Object> add(CustomerBaseRequest customerBaseRequest){
		log.info("【添加客户基础信息】   请求参数：{}",EntityUtils.beanToMap(customerBaseRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		CustomerBase customerBase = new CustomerBase();
		BeanUtils.copyProperties(customerBaseRequest,customerBase);
		mapper.insert(customerBase);
		respMap.put("code", ResponseCode.OK.getCode());
        respMap.put("msg", ResponseCode.OK.getMessage());
		log.info("【添加客户基础信息】   响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 修改客户基础信息
	 * @author centerroot
	 * @time 创建时间:2018年7月16日下午6:02:22
	 * @param CustomerBaseRequest
	 * @return
	 */
	public Map<String, Object> update(CustomerBaseRequest customerBaseRequest){
		log.info("【修改客户基础信息】   请求参数：{}",EntityUtils.beanToMap(customerBaseRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		CustomerBase CustomerBase = new CustomerBase();
		BeanUtils.copyProperties(customerBaseRequest,CustomerBase);
		mapper.updateByPrimaryKey(CustomerBase);
		respMap.put("code", ResponseCode.OK.getCode());
        respMap.put("msg", ResponseCode.OK.getMessage());
		log.info("【修改客户基础信息】   响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 删除客户基础信息
	 * @author centerroot
	 * @time 创建时间:2018年7月16日下午6:02:30
	 * @param personalId
	 * @return
	 */
	public Map<String, Object> delete(@ApiParam("客户编号") @RequestParam(value = "customerId", required = true) String customerId){
		log.info("【删除客户基础信息】   请求参数：{}",customerId);
		Map<String, Object> respMap = new HashMap<String, Object>();
		int count = mapper.deleteByPrimaryKey(customerId);
        respMap.put("code", ResponseCode.OK.getCode());
        respMap.put("msg", ResponseCode.OK.getMessage());
		log.info("【删除客户基础信息】   响应参数：{}",respMap);
		return respMap;
	}
}