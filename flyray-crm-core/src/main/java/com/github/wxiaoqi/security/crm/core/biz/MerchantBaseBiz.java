package com.github.wxiaoqi.security.crm.core.biz;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.crm.request.MerchantBaseRequest;
import com.github.wxiaoqi.security.common.crm.request.QueryMerchantBaseListRequest;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.util.EntityUtils;
import com.github.wxiaoqi.security.common.util.Query;
import com.github.wxiaoqi.security.common.util.SnowFlake;
import com.github.wxiaoqi.security.crm.core.entity.MerchantBase;
import com.github.wxiaoqi.security.crm.core.entity.PersonalBase;
import com.github.wxiaoqi.security.crm.core.mapper.MerchantBaseMapper;

import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * 商户客户基础信息
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@Slf4j
@Service
public class MerchantBaseBiz extends BaseBiz<MerchantBaseMapper,MerchantBase> {
	@Autowired
	private PlatformBaseBiz platformBaseBiz;
	/**
	 * 查询商户基础信息列表
	 * @author centerroot
	 * @time 创建时间:2018年7月20日上午11:32:40
	 * @param queryPersonalBaseListRequest
	 * @return
	 */
	public Map<String, Object> queryList(QueryMerchantBaseListRequest queryMerchantBaseListRequest){
		log.info("【查询商户基础信息列表】   请求参数：{}",EntityUtils.beanToMap(queryMerchantBaseListRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		String platformId = queryMerchantBaseListRequest.getPlatformId();
		String merchantId = queryMerchantBaseListRequest.getMerchantId();
		String authenticationStatus = queryMerchantBaseListRequest.getAuthenticationStatus();
		String status = queryMerchantBaseListRequest.getStatus();
		
		int page = queryMerchantBaseListRequest.getPage();
		int limit = queryMerchantBaseListRequest.getLimit();
		Map<String, Object> params = new HashMap<String, Object>();
	    params.put("page", page);
	    params.put("limit", limit);
	    Query query = new Query(params);
	    Page<Object> result = PageHelper.startPage(query.getPage(), query.getLimit());
		
		Example example = new Example(PersonalBase.class);
        Criteria criteria = example.createCriteria();
        if (!StringUtils.isEmpty(platformId)) {
        	criteria.andEqualTo("platformId", platformId);
		}
        if (!StringUtils.isEmpty(merchantId)) {
        	criteria.andEqualTo("merchantId", merchantId);
		}
        if (!StringUtils.isEmpty(authenticationStatus)) {
        	criteria.andEqualTo("authenticationStatus", authenticationStatus);
		}
        if (!StringUtils.isEmpty(status)) {
        	criteria.andEqualTo("status", status);
		}
        List<MerchantBase> list = mapper.selectByExample(example);
        
        TableResultResponse<MerchantBase> table = new TableResultResponse<MerchantBase>(result.getTotal(), list);
        respMap.put("body", table);
        respMap.put("code", ResponseCode.OK.getCode());
        respMap.put("msg", ResponseCode.OK.getMessage());
		log.info("【查询商户基础信息列表】   响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 添加商户基础信息
	 * @author centerroot
	 * @time 创建时间:2018年7月16日下午6:02:15
	 * @param MerchantBaseRequest
	 * @return
	 */
	public Map<String, Object> add(MerchantBaseRequest merchantBaseRequest){
		log.info("【添加商户基础信息】   请求参数：{}",EntityUtils.beanToMap(merchantBaseRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		MerchantBase merchantBase = new MerchantBase();
		BeanUtils.copyProperties(merchantBaseRequest,merchantBase);
		
		String merchantId = String.valueOf(SnowFlake.getId());
		merchantBase.setMerchantId(merchantId);
		merchantBase.setAuthenticationStatus("00");
		merchantBase.setStatus("00");
		
		mapper.insert(merchantBase);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("merchantName", merchantBase.getMerchantName());
		map.put("merchantId", merchantId);
		map.put("platformId", merchantBase.getPlatformId());
		map.put("type", 3);
		platformBaseBiz.commonAdd(map);
		respMap.put("code", ResponseCode.OK.getCode());
        respMap.put("msg", ResponseCode.OK.getMessage());
		log.info("【添加商户基础信息】   响应参数：{}",respMap);
		return respMap;
	}
	
}