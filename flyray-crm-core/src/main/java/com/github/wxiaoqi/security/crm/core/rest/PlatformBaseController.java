package com.github.wxiaoqi.security.crm.core.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.common.crm.request.PlatformBaseRequest;
import com.github.wxiaoqi.security.common.crm.request.QueryMerchantBaseListRequest;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.crm.core.biz.PlatformBaseBiz;
import com.github.wxiaoqi.security.crm.core.entity.PlatformBase;

@Controller
@RequestMapping("platformBase")
public class PlatformBaseController extends BaseController<PlatformBaseBiz,PlatformBase> {
	
	@Autowired
	private PlatformBaseBiz platformBaseBiz;
	/**
	 * 添加平台
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> addPlatform(@RequestBody PlatformBaseRequest entity) throws Exception {
		platformBaseBiz.addPlatform(entity);
        return ResponseEntity.ok(entity);
    }
	/**
	 * 查询类表
	 * @param bean
	 * @return
	 */
	@RequestMapping(value = "/pageList",method = RequestMethod.POST)
    @ResponseBody
    public TableResultResponse<PlatformBase> pageList(@RequestBody QueryMerchantBaseListRequest bean){
		bean.setPlatformId(setPlatformId(bean.getPlatformId()));
        return baseBiz.pageList(bean);
    }
	
	

}