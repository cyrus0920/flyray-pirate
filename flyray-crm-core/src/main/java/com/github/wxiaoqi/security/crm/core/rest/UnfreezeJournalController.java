package com.github.wxiaoqi.security.crm.core.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.crm.core.biz.UnfreezeJournalBiz;
import com.github.wxiaoqi.security.crm.core.entity.UnfreezeJournal;

@RestController
@RequestMapping("unfreezeJournal")
public class UnfreezeJournalController extends BaseController<UnfreezeJournalBiz,UnfreezeJournal> {

}