package com.github.wxiaoqi.security.crm.core.mapper;

import com.github.wxiaoqi.security.crm.core.entity.PayChannelFeeConfig;
import tk.mybatis.mapper.common.Mapper;

/**
 * 支付通道费率配置表
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-08-06 13:58:35
 */
@org.apache.ibatis.annotations.Mapper
public interface PayChannelFeeConfigMapper extends Mapper<PayChannelFeeConfig> {
	
}
