package com.github.wxiaoqi.security.crm.core.biz;

import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.crm.core.entity.PlatformAccoutConfig;
import com.github.wxiaoqi.security.crm.core.mapper.PlatformAccoutConfigMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

/**
 * 平台所支持账户配置
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@Service
public class PlatformAccoutConfigBiz extends BaseBiz<PlatformAccoutConfigMapper,PlatformAccoutConfig> {
}