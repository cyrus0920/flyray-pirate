package com.github.wxiaoqi.security.crm.core.biz;

import java.util.List;

import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.crm.request.SafetyConfigRequest;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.crm.core.entity.PayChannelFeeConfig;
import com.github.wxiaoqi.security.crm.core.entity.PlatformSafetyConfig;
import com.github.wxiaoqi.security.crm.core.mapper.PayChannelFeeConfigMapper;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * 支付通道费率配置表
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-08-06 13:58:35
 */
@Service
public class PayChannelFeeConfigBiz extends BaseBiz<PayChannelFeeConfigMapper,PayChannelFeeConfig> {
	
	/**
	 * 列表
	 * @param bean
	 * @return
	 */
	public TableResultResponse<PayChannelFeeConfig> payChannelFeeList(SafetyConfigRequest bean){
		 Example example = new Example(PlatformSafetyConfig.class);
		 Criteria criteria = example.createCriteria();
		 criteria.andEqualTo("platformId",bean.getPlatformId());
		 Page<PayChannelFeeConfig> result = PageHelper.startPage(bean.getPage(), bean.getLimit());
		 List<PayChannelFeeConfig> list = mapper.selectByExample(example);
		 return new TableResultResponse<PayChannelFeeConfig>(result.getTotal(), list);
	}
}