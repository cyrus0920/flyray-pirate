package com.github.wxiaoqi.security.crm.core.biz;

import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.crm.core.entity.PersonalAccountJournal;
import com.github.wxiaoqi.security.crm.core.mapper.PersonalAccountJournalMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

/**
 * 个人账户流水（充、转、提、退、冻结流水）
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@Service
public class PersonalAccountJournalBiz extends BaseBiz<PersonalAccountJournalMapper,PersonalAccountJournal> {
}