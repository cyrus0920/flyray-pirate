package com.github.wxiaoqi.security.crm.core.feignserver;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.common.crm.request.IntoAccountRequest;
import com.github.wxiaoqi.security.common.crm.request.RefundFreezeRequest;
import com.github.wxiaoqi.security.common.crm.request.RefundUnFreezeRequest;
import com.github.wxiaoqi.security.common.crm.request.UnFreAndOutAccountRequest;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.util.EntityUtils;
import com.github.wxiaoqi.security.crm.core.biz.RefundFreezeUnFreezeBiz;
import com.github.wxiaoqi.security.crm.core.biz.RefundUnFreAndOutAccountBiz;
import com.github.wxiaoqi.security.crm.core.biz.PayChannelConfigBiz;
import com.github.wxiaoqi.security.crm.core.entity.PayChannelConfig;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/***
 * 退款相关的接口
 * */
@Api(tags="退款调用的入账接口")
@Controller
@RequestMapping("feign")
public class FeignCommonRefund {
	
	@Autowired
	private RefundFreezeUnFreezeBiz refundFreezeUnFreezeBiz;
	
	@Autowired
	private RefundUnFreAndOutAccountBiz refundUnFreAndOutAccountBiz;
	/**
	 * 发起退款时冻结资金接口，此接口是公共的接口。
	 * 包含了第三方支付和余额账户支付两种情况
	 * @param 
	 * @return RefundFreezeUnFreeze
	 */
	@ApiOperation("发起退款时冻结资金接口")
	@RequestMapping(value = "/refundFreeze",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> refundFreeze(@RequestBody @Valid RefundFreezeRequest refundFreezeRequest){
		Map<String, Object> response = refundFreezeUnFreezeBiz.freeze(refundFreezeRequest);
		return response;
    }
	
	/**
	 * 
	 * 退款失败时解冻资金接口，此接口是公共的接口。
	 * 包含了第三方支付和余额账户支付情况
	 * @param param
	 * @return
	 */
	@ApiOperation("退款失败时解冻资金接口")
	@RequestMapping(value = "/refundUnFreeze",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> refundUnFreeze(@RequestBody @Valid RefundUnFreezeRequest refundUnFreezeRequest){
		Map<String, Object> response = refundFreezeUnFreezeBiz.refundUnFreeze(refundUnFreezeRequest);
		return response;
    }
	
	/***
	 * 第三方支付退款时需要审核时
	 * 若退款成功则需要解冻资金并出账的接口
	 * */
	@ApiOperation("退款成功后(第三方支付)解冻资金并出账接口")
	@RequestMapping(value = "/unFreAndOutAccount",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> unFreAndOutAccount(@RequestBody @Valid UnFreAndOutAccountRequest unFreAndOutAccountRequest){
		Map<String, Object> response = refundUnFreAndOutAccountBiz.unFreAndOutAccount(unFreAndOutAccountRequest);
		return response;
    }
	
	/***
	 * 余额账户支付退款时需要审核时
	 * 若退款成功则需要解冻资金并出账的接口
	 * */
	@ApiOperation("退款成功后(余额账户支付)解冻资金并出账接口")
	@RequestMapping(value = "/balanceUnFreAndOutAccount",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> balanceUnFreAndOutAccount(@RequestBody @Valid UnFreAndOutAccountRequest unFreAndOutAccountRequest){
		Map<String, Object> response = refundUnFreAndOutAccountBiz.balanceUnFreAndOutAccount(unFreAndOutAccountRequest);
		return response;
    }
	
	
	/**
	 * 第三方支付退款不需要审核时
	 * 直接出账的接口
	 * */
	
	/**
	 * 余额账户支付 退款不需要审核时
	 * 直接出账的接口
	 * */
	
}
