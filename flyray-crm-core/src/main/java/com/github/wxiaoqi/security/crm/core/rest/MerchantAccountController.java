package com.github.wxiaoqi.security.crm.core.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.crm.core.biz.MerchantAccountBiz;
import com.github.wxiaoqi.security.crm.core.entity.MerchantAccount;

@RestController
@RequestMapping("merchantAccount")
public class MerchantAccountController extends BaseController<MerchantAccountBiz,MerchantAccount> {

}