package com.github.wxiaoqi.security.auth.common.util.jwt;

/**
 * Created by ace on 2017/9/10.
 */
public interface IJWTInfo {
    /**
     * 获取用户名
     * @return
     */
    String getUniqueName();

    /**
     * 获取用户ID
     * @return
     */
    String getId();

    /**
     * 获取名称
     * @return
     */
    String getName();
    
    /**
     * 获取平台编号
     * */
    String getPlamId();
    /**
     * 获取签名盐值
     * */
    String getSaltKey();
}
