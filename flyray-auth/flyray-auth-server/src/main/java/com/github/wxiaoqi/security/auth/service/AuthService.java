package com.github.wxiaoqi.security.auth.service;


import com.github.wxiaoqi.security.auth.util.user.JwtAuthenticationRequest;

public interface AuthService {
    String login(JwtAuthenticationRequest authenticationRequest) throws Exception;
    String refresh(String oldToken) throws Exception;
    void validate(String token) throws Exception;
    /**
     * 生成crm外部提供的服务crmtoken
     * @param 平台编号 platformId,盐值saltValue
     * */
    String createCrmToken(String platformId,String saltValue)throws Exception ;
}
