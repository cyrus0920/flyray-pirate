package com.github.wxiaoqi.security.auth.util.user;

import java.io.Serializable;

public class JwtAuthenticationRequest implements Serializable {

	private static final long serialVersionUID = -8445943548965154778L;

	private String username;
	private String password;
	private String platformId;
	private String saltKey;

	public JwtAuthenticationRequest(String username, String password, String platformId, String saltKey) {
		this.username = username;
		this.password = password;
		this.platformId = platformId;
		this.saltKey = saltKey;
	}

	public JwtAuthenticationRequest() {
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPlatformId() {
		return platformId;
	}

	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}

	public String getSaltKey() {
		return saltKey;
	}

	public void setSaltKey(String saltKey) {
		this.saltKey = saltKey;
	}

}
