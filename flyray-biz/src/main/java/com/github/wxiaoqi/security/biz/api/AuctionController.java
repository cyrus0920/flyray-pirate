package com.github.wxiaoqi.security.biz.api;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.biz.biz.auction.AuctionGoodsInfoBiz;
import com.github.wxiaoqi.security.biz.biz.auction.AuctionRecordingInfoBiz;
import com.github.wxiaoqi.security.common.cms.request.AuctionGoodsDetailParam;
import com.github.wxiaoqi.security.common.cms.request.AuctionParam;
import com.github.wxiaoqi.security.common.cms.request.AuctionRecordingParam;
import com.github.wxiaoqi.security.common.cms.request.BaseParam;
import com.github.wxiaoqi.security.common.util.EntityUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * 竞拍相关接口
 * @author he
 *
 */
@Slf4j
@Api(tags="竞拍管理")
@Controller
@RequestMapping("api/auction")
public class AuctionController {
	
	@Autowired
	private AuctionGoodsInfoBiz auctionGoodsInfoBiz;
	@Autowired
	private AuctionRecordingInfoBiz auctionRecordingInfoBiz;
	
	/**
	 * 竞拍商品信息查询
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("竞拍商品信息查询")
	@RequestMapping(value = "/query",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> query(@RequestBody BaseParam baseParam) throws Exception {
		Map<String, Object> response = auctionGoodsInfoBiz.query(EntityUtils.beanToMap(baseParam));
		return response;
    }
	
	/**
	 * 竞拍商品信息详情查询
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("竞拍商品信息详情查询")
	@RequestMapping(value = "/queryDetail",method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> queryDetail(@RequestBody AuctionGoodsDetailParam auctionGoodsDetailParam) throws Exception {
		Map<String, Object> response = auctionGoodsInfoBiz.queryDetail(EntityUtils.beanToMap(auctionGoodsDetailParam));
		return response;
	}
	
	/**
	 * 发起竞拍
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("发起竞拍")
	@RequestMapping(value = "/toAuction",method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> toAuction(@RequestBody AuctionParam auctionParam) throws Exception {
		Map<String, Object> response = auctionRecordingInfoBiz.toAuction(EntityUtils.beanToMap(auctionParam));
		return response;
	}
	
	/**
	 * 竞拍记录查询
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("竞拍记录查询")
	@RequestMapping(value = "/queryRecordingInfo",method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> queryRecordingInfo(@RequestBody AuctionRecordingParam auctionRecordingParam) throws Exception {
		Map<String, Object> response = auctionRecordingInfoBiz.queryRecordingInfo(EntityUtils.beanToMap(auctionRecordingParam));
		return response;
	}

}
