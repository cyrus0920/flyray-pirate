package com.github.wxiaoqi.security.biz.biz.auction;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.biz.entity.auction.AuctionGoodsInfo;
import com.github.wxiaoqi.security.biz.mapper.AuctionGoodsInfoMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.util.Query;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * 竞拍商品信息表
 *
 * @author he
 * @date 2018-07-16 11:38:52
 */
@Service
public class AuctionGoodsInfoBiz extends BaseBiz<AuctionGoodsInfoMapper,AuctionGoodsInfo> {
	
	private static final Logger logger = (Logger) LoggerFactory.getLogger(AuctionGoodsInfoBiz.class);

	@Autowired
	private AuctionGoodsInfoMapper auctionGoodsInfoMapper;
	
	/**
	 * 竞拍商品信息查询
	 * @param request
	 * @return
	 */
	public Map<String, Object> query(Map<String, Object> request){
		logger.info("竞拍商品信息查询请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platFormId");
		String merId = (String) request.get("merId");
		Example example = new Example(AuctionGoodsInfo.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("platformId", platFormId);
		criteria.andEqualTo("merId", merId);
		example.setOrderByClause("create_time desc");
		List<AuctionGoodsInfo> goodsInfoList = auctionGoodsInfoMapper.selectByExample(example);
		response.put("goodsInfoList", goodsInfoList);
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		logger.info("竞拍商品信息查询响应。。。。。。{}", response);
		return response;
	}
	
	/**
	 * 竞拍商品信息详情查询
	 * @param request
	 * @return
	 */
	public Map<String, Object> queryDetail(Map<String, Object> request){
		logger.info("竞拍商品信息详情查询请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platFormId");
		String merId = (String) request.get("merId");
		String goodsId = (String) request.get("goodsId");
		AuctionGoodsInfo auctionGoodsInfo = new AuctionGoodsInfo();
		auctionGoodsInfo.setPlatformId(platFormId);
		auctionGoodsInfo.setMerId(merId);
		auctionGoodsInfo.setGoodsId(goodsId);
		AuctionGoodsInfo selectGoodsInfo = auctionGoodsInfoMapper.selectOne(auctionGoodsInfo);
		Date deadlineTime = selectGoodsInfo.getDeadlineTime();
		if(deadlineTime.before(new Date())){
			response.put("auctionSwitch", "1");//竞拍关闭
		}
		response.put("auctionGoodsInfo", selectGoodsInfo);
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		logger.info("竞拍商品信息详情查询响应。。。。。。{}", response);
		return response;
	}
	
	/**
	 * 分页查询竞拍商品
	 * @param param
	 * @return
	 */
	public Map<String, Object> queryAuctionGoodsPage(Map<String, Object> param) {
		logger.info("分页查询竞拍商品。。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		try {
			Query query = new Query(param);
			Page<AuctionGoodsInfo> result = PageHelper.startPage(query.getPage(), query.getLimit());
			List<AuctionGoodsInfo> list = mapper.queryAuctionGoods(param);
			TableResultResponse<AuctionGoodsInfo> table = new TableResultResponse<>(result.getTotal(), list);
			respMap.put("code", "00");
			respMap.put("msg", "查询成功");
			respMap.put("body", table);
		} catch (Exception e) {
			respMap.put("code", "01");
			respMap.put("msg", "查询异常");
		}
		return respMap;
	}
	
	/**
	 * 删除商品
	 * @param param
	 * @return
	 */
	public Map<String, Object> deleteGoodsInfo(Map<String, Object> param) {
		logger.info("删除商品。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		try {
			int id = (int) param.get("id");
			AuctionGoodsInfo auctionGoodsInfo = mapper.selectByPrimaryKey(Long.valueOf(id));
			if (auctionGoodsInfo != null) {
				mapper.delete(auctionGoodsInfo);
				respMap.put("code", "00");
				respMap.put("msg", "删除成功");
			}else {
				respMap.put("code", "01");
				respMap.put("msg", "商品不存在");
			}
		} catch (Exception e) {
			respMap.put("code", "01");
			respMap.put("msg", "操作异常");
		}
		return respMap;
	}
	
	/**
	 * 修改商品
	 * @param param
	 * @return
	 */
	public Map<String, Object> updateGoodsInfo(Map<String, Object> param) {
		logger.info("修改商品。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		try {
			int id = (int) param.get("id");
			String goodsName = (String) param.get("goodsName");
			String goodsDescription = (String) param.get("goodsDescription");
			String goodsImg = (String) param.get("goodsImg");
			String startingPrice = (String) param.get("startingPrice");
			String deadlineTime = (String) param.get("deadlineTime");
			AuctionGoodsInfo auctionGoodsInfo = mapper.selectByPrimaryKey(Long.valueOf(id));
			if (auctionGoodsInfo != null) {
				auctionGoodsInfo.setCurrentPrice(new BigDecimal(startingPrice));
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				auctionGoodsInfo.setDeadlineTime(sdf.parse(deadlineTime));
				auctionGoodsInfo.setGoodsDescription(goodsDescription);
				auctionGoodsInfo.setGoodsImg(goodsImg);
				auctionGoodsInfo.setGoodsName(goodsName);
				auctionGoodsInfo.setStartingPrice(new BigDecimal(startingPrice));
				mapper.updateByPrimaryKey(auctionGoodsInfo);
				respMap.put("code", "00");
				respMap.put("msg", "修改成功");
			}else {
				respMap.put("code", "01");
				respMap.put("msg", "商品不存在");
			}
		} catch (Exception e) {
			respMap.put("code", "01");
			respMap.put("msg", "操作异常");
		}
		return respMap;
	}
}