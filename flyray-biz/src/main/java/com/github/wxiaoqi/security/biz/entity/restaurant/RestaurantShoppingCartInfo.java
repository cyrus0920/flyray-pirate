package com.github.wxiaoqi.security.biz.entity.restaurant;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 购物车信息表
 * 
 * @author he
 * @date 2018-06-29 10:31:27
 */
@Table(name = "restaurant_shopping_cart_info")
public class RestaurantShoppingCartInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	//平台编号
	@Column(name = "platform_id")
	private String platformId;

	//商户账号
	@Column(name = "mer_id")
	private String merId;

	//用户账号
	@Column(name = "per_id")
	private String perId;
	
	//餐桌id
	@Column(name = "table_id")
	private Integer tableId;

	//菜品id
	@Column(name = "dishes_id")
	private Integer dishesId;

	//菜品名称
	@Column(name = "dishes_name")
	private String dishesName;

	//数量
	@Column(name = "dishes_num")
	private Integer dishesNum;

	//总价
	@Column(name = "dishes_price")
	private String dishesPrice;

	//状态
	@Column(name = "status")
	private String status;
	
	//类型 1点餐 2外卖
	@Column(name = "type")
	private String type;


	/**
	 * 设置：
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：商户账号
	 */
	public void setMerId(String merId) {
		this.merId = merId;
	}
	/**
	 * 获取：商户账号
	 */
	public String getMerId() {
		return merId;
	}
	/**
	 * 设置：用户账号
	 */
	public void setPerId(String perId) {
		this.perId = perId;
	}
	/**
	 * 获取：用户账号
	 */
	public String getPerId() {
		return perId;
	}
	public Integer getTableId() {
		return tableId;
	}
	public void setTableId(Integer tableId) {
		this.tableId = tableId;
	}
	/**
	 * 设置：菜品id
	 */
	public void setDishesId(Integer dishesId) {
		this.dishesId = dishesId;
	}
	/**
	 * 获取：菜品id
	 */
	public Integer getDishesId() {
		return dishesId;
	}
	/**
	 * 设置：菜品名称
	 */
	public void setDishesName(String dishesName) {
		this.dishesName = dishesName;
	}
	/**
	 * 获取：菜品名称
	 */
	public String getDishesName() {
		return dishesName;
	}
	/**
	 * 设置：数量
	 */
	public void setDishesNum(Integer dishesNum) {
		this.dishesNum = dishesNum;
	}
	/**
	 * 获取：数量
	 */
	public Integer getDishesNum() {
		return dishesNum;
	}
	/**
	 * 设置：总价
	 */
	public void setDishesPrice(String dishesPrice) {
		this.dishesPrice = dishesPrice;
	}
	/**
	 * 获取：总价
	 */
	public String getDishesPrice() {
		return dishesPrice;
	}
	/**
	 * 设置：状态
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * 获取：状态
	 */
	public String getStatus() {
		return status;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
}
