package com.github.wxiaoqi.security.biz.rest.cms;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.biz.biz.fightGroup.FightGroupInfoBiz;
import com.github.wxiaoqi.security.biz.entity.fightGroup.FightGroupInfo;
import com.github.wxiaoqi.security.common.rest.BaseController;

import lombok.extern.slf4j.Slf4j;

/**
 * 小程序拼团团信息相关接口
 * @author he
 *
 */
@Slf4j
@Controller
@RequestMapping("fightGroup/groupInfo")
public class FightGroupInfoRestController extends BaseController<FightGroupInfoBiz, FightGroupInfo> {
	
	@Autowired
	private FightGroupInfoBiz fightGroupInfoBiz;
	
	/**
	 * 查询拼团团信息列表
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Map<String, Object> query(@RequestBody Map<String, Object> param) {
		log.info("查询拼团团信息列表------start------{}", param);
		Map<String, Object> respMap = fightGroupInfoBiz.queryFightGroupInfoPage(param);
		if (respMap.get("code").equals("00")) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("查询拼团团信息列表-----end-------{}", respMap);
		return respMap;
	}
	
}
