package com.github.wxiaoqi.security.biz.biz.restaurant;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.biz.entity.restaurant.RestaurantAppraisalDetailInfo;
import com.github.wxiaoqi.security.biz.mapper.RestaurantAppraisalDetailInfoMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.util.Query;

/**
 * 菜品评价明细表
 *
 * @author he
 * @date 2018-06-29 10:31:27
 */
@Service
public class RestaurantAppraisalDetailInfoBiz extends BaseBiz<RestaurantAppraisalDetailInfoMapper,RestaurantAppraisalDetailInfo> {

	private static final Logger logger = (Logger) LoggerFactory.getLogger(RestaurantAppraisalDetailInfoBiz.class);

	@Autowired
	private RestaurantAppraisalDetailInfoMapper restaurantAppraisalDetailInfoMapper;

	/**
	 * 菜品评价信息查询
	 * @param request
	 * @return
	 */
	public Map<String, Object> queryAppraisalDetailInfo(Map<String, Object> request){
		logger.info("菜品评价信息查询请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platFormId");
		String merId = (String) request.get("merId");
		String dishesId = (String) request.get("dishesId");
		RestaurantAppraisalDetailInfo restaurantAppraisalDetailInfo = new RestaurantAppraisalDetailInfo();
		restaurantAppraisalDetailInfo.setPlatformId(platFormId);
		restaurantAppraisalDetailInfo.setMerId(merId);
		restaurantAppraisalDetailInfo.setDishesId(Integer.valueOf(dishesId));
		//查询全部评价
		List<RestaurantAppraisalDetailInfo> detailAllList = restaurantAppraisalDetailInfoMapper.queryDetailInfoList(restaurantAppraisalDetailInfo);
		response.put("detailAllList", detailAllList);

		//查询很满意评价
		restaurantAppraisalDetailInfo.setAppraisalGrade("1");//很满意
		List<RestaurantAppraisalDetailInfo> verySatisfiedList = restaurantAppraisalDetailInfoMapper.queryDetailInfoList(restaurantAppraisalDetailInfo);
		response.put("verySatisfiedList", verySatisfiedList);

		//查询满意评价
		restaurantAppraisalDetailInfo.setAppraisalGrade("2");//满意
		List<RestaurantAppraisalDetailInfo> satisfiedList = restaurantAppraisalDetailInfoMapper.queryDetailInfoList(restaurantAppraisalDetailInfo);
		response.put("satisfiedList", satisfiedList);

		//查询一般评价
		restaurantAppraisalDetailInfo.setAppraisalGrade("3");//一般
		List<RestaurantAppraisalDetailInfo> generalList = restaurantAppraisalDetailInfoMapper.queryDetailInfoList(restaurantAppraisalDetailInfo);
		response.put("generalList", generalList);

		//查询不满意评价
		restaurantAppraisalDetailInfo.setAppraisalGrade("4");//不满意
		List<RestaurantAppraisalDetailInfo> notSatisfiedList = restaurantAppraisalDetailInfoMapper.queryDetailInfoList(restaurantAppraisalDetailInfo);
		response.put("notSatisfiedList", notSatisfiedList);
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		logger.info("菜品评价信息查询响应。。。。。。{}", response);
		return response;
	}
	
	/**
	 * 查询评价详情信息列表
	 * @param param
	 * @return
	 */
	public Map<String, Object> queryRestaurantAppraisalDetailInfoPage(Map<String, Object> param) {
		logger.info("查询评价详情信息列表。。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		try {
			Query query = new Query(param);
			Page<RestaurantAppraisalDetailInfo> result = PageHelper.startPage(query.getPage(), query.getLimit());
			List<RestaurantAppraisalDetailInfo> list = mapper.queryRestaurantAppraisalDetailInfo(param);
			TableResultResponse<RestaurantAppraisalDetailInfo> table = new TableResultResponse<>(result.getTotal(), list);
			respMap.put("code", "00");
			respMap.put("msg", "查询成功");
			respMap.put("body", table);
		} catch (Exception e) {
			respMap.put("code", "01");
			respMap.put("msg", "查询异常");
		}
		return respMap;
	}

}