package com.github.wxiaoqi.security.biz.biz.auction;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.biz.entity.auction.AuctionGoodsInfo;
import com.github.wxiaoqi.security.biz.entity.auction.AuctionRecordingInfo;
import com.github.wxiaoqi.security.biz.mapper.AuctionGoodsInfoMapper;
import com.github.wxiaoqi.security.biz.mapper.AuctionRecordingInfoMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.util.Query;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * 竞拍记录表
 *
 * @author he
 * @date 2018-07-17 13:44:31
 */
@Service
public class AuctionRecordingInfoBiz extends BaseBiz<AuctionRecordingInfoMapper,AuctionRecordingInfo> {
	
	private static final Logger logger = (Logger) LoggerFactory.getLogger(AuctionRecordingInfoBiz.class);
	
	@Autowired
	private AuctionRecordingInfoMapper auctionRecordingInfoMapper;
	@Autowired
	private AuctionGoodsInfoMapper auctionGoodsInfoMapper;
	
	/**
	 * 发起竞拍
	 * @param request
	 * @return
	 */
	public Map<String, Object> toAuction(Map<String, Object> request){
		logger.info("发起竞拍请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platFormId");
		String merId = (String) request.get("merId");
		String perId = (String) request.get("perId");
		String name = (String) request.get("name");
		String userHeadPortrait = (String) request.get("userHeadPortrait");
		String price = (String) request.get("price");
		String goodsId = (String) request.get("goodsId");
		AuctionRecordingInfo auctionRecordingInfo = new AuctionRecordingInfo();
		auctionRecordingInfo.setPlatformId(platFormId);
		auctionRecordingInfo.setMerId(merId);
		auctionRecordingInfo.setPerId(perId);
		auctionRecordingInfo.setGoodsId(goodsId);
		auctionRecordingInfo.setName(name);
		auctionRecordingInfo.setUserHeadPortrait(userHeadPortrait);
		auctionRecordingInfo.setAmt(new BigDecimal(price));
		auctionRecordingInfo.setCreateTime(new Date());
		auctionRecordingInfoMapper.insert(auctionRecordingInfo);
		
		AuctionGoodsInfo auctionGoodsInfo = new AuctionGoodsInfo();
		auctionGoodsInfo.setPlatformId(platFormId);
		auctionGoodsInfo.setMerId(merId);
		auctionGoodsInfo.setGoodsId(goodsId);
		AuctionGoodsInfo selectGoodsInfo = auctionGoodsInfoMapper.selectOne(auctionGoodsInfo);
		selectGoodsInfo.setCurrentPrice(new BigDecimal(price));
		auctionGoodsInfoMapper.updateByPrimaryKey(selectGoodsInfo);
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		logger.info("发起竞拍响应。。。。。。{}", response);
		return response;
	}
	
	/**
	 * 竞拍记录查询
	 * @param request
	 * @return
	 */
	public Map<String, Object> queryRecordingInfo(Map<String, Object> request){
		logger.info("竞拍记录查询请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platFormId");
		String merId = (String) request.get("merId");
		String goodsId = (String) request.get("goodsId");
		Example example = new Example(AuctionRecordingInfo.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("platformId", platFormId);
		criteria.andEqualTo("merId", merId);
		criteria.andEqualTo("goodsId", goodsId);
		example.setOrderByClause("create_time desc");
		List<AuctionRecordingInfo> recordingList = auctionRecordingInfoMapper.selectByExample(example);
		response.put("recordingList", recordingList);
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		logger.info("竞拍记录查询响应。。。。。。{}", response);
		return response;
	}
	
	/**
	 * 分页查询竞拍记录
	 * @param param
	 * @return
	 */
	public Map<String, Object> queryAuctionRecordingPage(Map<String, Object> param) {
		logger.info("分页查询竞拍记录。。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		try {
			Query query = new Query(param);
			Page<AuctionRecordingInfo> result = PageHelper.startPage(query.getPage(), query.getLimit());
			List<AuctionRecordingInfo> list = mapper.queryAuctionRecording(param);
			TableResultResponse<AuctionRecordingInfo> table = new TableResultResponse<>(result.getTotal(), list);
			respMap.put("code", "00");
			respMap.put("msg", "查询成功");
			respMap.put("body", table);
		} catch (Exception e) {
			respMap.put("code", "01");
			respMap.put("msg", "查询异常");
		}
		return respMap;
	}
}