package com.github.wxiaoqi.security.biz.feign;

import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 支付相关接口调用
 * @author he
 *
 */
@Async
@FeignClient(value = "flyray-crm-core")
public interface CrmFeign {
	
	/**
	 * 第三方支付入账
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "feign/payment/inAccounting",method = RequestMethod.POST)
	public Map<String, Object> inAccounting(@RequestBody Map<String, Object> params);
	
	/**
	 * 第三方退款出账
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "feign/payment/outAccounting",method = RequestMethod.POST)
	public Map<String, Object> outAccounting(@RequestBody Map<String, Object> params);

}
