package com.github.wxiaoqi.security.biz.biz.restaurant;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.biz.entity.restaurant.RestaurantInfo;
import com.github.wxiaoqi.security.biz.entity.restaurant.RestaurantTableInfo;
import com.github.wxiaoqi.security.biz.mapper.RestaurantInfoMapper;
import com.github.wxiaoqi.security.biz.mapper.RestaurantTableInfoMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.util.EntityUtils;
import com.github.wxiaoqi.security.common.util.Query;

/**
 * 餐桌信息表
 *
 * @author he
 * @date 2018-06-29 10:31:27
 */
@Service
public class RestaurantTableInfoBiz extends BaseBiz<RestaurantTableInfoMapper,RestaurantTableInfo> {

	private static final Logger logger = (Logger) LoggerFactory.getLogger(RestaurantTableInfoBiz.class);

	@Autowired
	private RestaurantInfoMapper restaurantInfoMapper;
	@Autowired
	private RestaurantTableInfoMapper restaurantTableInfoMapper;

	/**
	 * 餐桌信息查询
	 * @param request
	 * @return
	 */
	public Map<String, Object> queryTableInfo(Map<String, Object> request){
		logger.info("餐桌信息查询请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platFormId");
		String merId = (String) request.get("merId");
		RestaurantInfo restaurantInfo = new RestaurantInfo();
		restaurantInfo.setPlatformId(platFormId);
		restaurantInfo.setMerId(merId);
		RestaurantInfo selectRestaurantInfo = restaurantInfoMapper.selectOne(restaurantInfo);
		Map<String, Object> map = EntityUtils.beanToMap(selectRestaurantInfo);
		RestaurantTableInfo restaurantTableInfo = new RestaurantTableInfo();
		restaurantTableInfo.setPlatformId(platFormId);
		restaurantTableInfo.setMerId(merId);
		List<RestaurantTableInfo> selectRestaurantTableInfo = restaurantTableInfoMapper.select(restaurantTableInfo);
		response.put("info", selectRestaurantTableInfo);
		response.put("company", map);
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		logger.info("餐桌信息查询响应。。。。。。{}", response);
		return response;
	}
	
	/**
	 * 查询餐桌信息列表
	 * @param param
	 * @return
	 */
	public Map<String, Object> queryRestaurantTableInfoPage(Map<String, Object> param) {
		logger.info("查询餐桌信息列表。。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		try {
			Query query = new Query(param);
			Page<RestaurantTableInfo> result = PageHelper.startPage(query.getPage(), query.getLimit());
			List<RestaurantTableInfo> list = mapper.queryRestaurantTableInfo(param);
			TableResultResponse<RestaurantTableInfo> table = new TableResultResponse<>(result.getTotal(), list);
			respMap.put("code", "00");
			respMap.put("msg", "查询成功");
			respMap.put("body", table);
		} catch (Exception e) {
			respMap.put("code", "01");
			respMap.put("msg", "查询异常");
		}
		return respMap;
	}
	
	/**
	 * 删除餐桌
	 * @param param
	 * @return
	 */
	public Map<String, Object> deleteRestaurantTableInfo(Map<String, Object> param) {
		logger.info("删除餐桌。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		try {
			int id = (int) param.get("id");
			RestaurantTableInfo restaurantTableInfo = mapper.selectByPrimaryKey(Long.valueOf(id));
			if (restaurantTableInfo != null) {
				mapper.delete(restaurantTableInfo);
				respMap.put("code", "00");
				respMap.put("msg", "删除成功");
			}else {
				respMap.put("code", "01");
				respMap.put("msg", "餐桌不存在");
			}
		} catch (Exception e) {
			respMap.put("code", "01");
			respMap.put("msg", "操作异常");
		}
		return respMap;
	}
	
	/**
	 * 修改餐桌
	 * @param param
	 * @return
	 */
	public Map<String, Object> updateRestaurantTableInfo(Map<String, Object> param) {
		logger.info("修改餐桌。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		try {
			int id = (int) param.get("id");
			String tableName = (String) param.get("tableName");
			String tablePeople = (String) param.get("tablePeople");
			String status = (String) param.get("status");
			RestaurantTableInfo restaurantTableInfo = mapper.selectByPrimaryKey(Long.valueOf(id));
			if (restaurantTableInfo != null) {
				restaurantTableInfo.setTableName(tableName);
				restaurantTableInfo.setTablePeople(tablePeople);
				restaurantTableInfo.setStatus(status);
				mapper.updateByPrimaryKey(restaurantTableInfo);
				respMap.put("code", "00");
				respMap.put("msg", "修改成功");
			}else {
				respMap.put("code", "01");
				respMap.put("msg", "餐厅不存在");
			}
		} catch (Exception e) {
			respMap.put("code", "01");
			respMap.put("msg", "操作异常");
		}
		return respMap;
	}
}