package com.github.wxiaoqi.security.biz.biz.refund;

import java.math.BigDecimal;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.biz.entity.CmsScenesOrder;
import com.github.wxiaoqi.security.biz.entity.auction.AuctionOrder;
import com.github.wxiaoqi.security.biz.feign.CrmFeign;
import com.github.wxiaoqi.security.biz.mapper.AuctionOrderMapper;
import com.github.wxiaoqi.security.biz.mapper.CmsScenesOrderMapper;

/**
 * 退款回调处理
 * @author he
 *
 */
@Service
public class RefundCallBackBiz {

	private static final Logger logger = (Logger) LoggerFactory.getLogger(RefundCallBackBiz.class);

	@Autowired
	private CrmFeign crmFeign;
	@Autowired
	private CmsScenesOrderMapper cmsScenesOrderMapper;
	@Autowired
	private AuctionOrderMapper auctionOrderMapper;

	/**
	 * 退款完成回调
	 * @param request
	 * @return
	 */
	public void refundCallBack(Map<String, Object> params){
		logger.info("退款完成回调开始.......{}",params);
		String platformId = (String) params.get("platformId");
		String orderNo = (String) params.get("orderNo");
		String payOrderNo = (String) params.get("payOrderNo");
		String txStatus = (String) params.get("txStatus");
		String amount = (String) params.get("amount");
		BigDecimal amountDecimal = new BigDecimal(amount);

		CmsScenesOrder cmsScenesOrder = new CmsScenesOrder();
		cmsScenesOrder.setPlatformId(platformId);
		cmsScenesOrder.setPayOrderNo(payOrderNo);
		CmsScenesOrder selectOrder = cmsScenesOrderMapper.selectOne(cmsScenesOrder);
		if(null == selectOrder){
			logger.info("支付完成回调-订单不存在.......");
			return;
		}
		if(0 != selectOrder.getTxAmt().compareTo(amountDecimal)){
			logger.info("支付完成回调-金额不一致.......");
			return;
		}
		selectOrder.setRefundOrderNo(orderNo);
		cmsScenesOrderMapper.updateByPrimaryKey(selectOrder);

		//退款成功出账
		if("00".equals(txStatus)){
			crmFeign.outAccounting(params);
		}

		//更新场景订单
		String scenesCode = selectOrder.getScenesCode();
		if("1".equals(scenesCode)){
			//竞拍
			this.updateAuctionOrder(params);
		}else if("2".equals(scenesCode)){
			//拼团

		}else if("3".equals(scenesCode)){
			//店内点餐

		}else if("4".equals(scenesCode)){
			//预订点餐

		}else if("5".equals(scenesCode)){
			//外卖

		}
	}
	
	/**
	 * 竞拍订单处理
	 * @param params
	 */
	private void updateAuctionOrder(Map<String, Object> params){
		String txStatus = (String) params.get("txStatus");
		AuctionOrder auctionOrder = new AuctionOrder();
		auctionOrder.setPlatformId((String) params.get("platformId"));
		auctionOrder.setMerId((String) params.get("merId"));
		auctionOrder.setPayOrderNo((String)params.get("payOrderNo"));
		auctionOrder.setStatus("00");//支付成功
		AuctionOrder queryAuctionOrder = auctionOrderMapper.selectOne(auctionOrder);
		if(null != queryAuctionOrder){
			if("00".equals(txStatus)){
				queryAuctionOrder.setStatus("01");//退款成功
			}else{
				queryAuctionOrder.setStatus("02");//退款失败
			}
		}
		auctionOrderMapper.updateByPrimaryKey(queryAuctionOrder);
	}

}
