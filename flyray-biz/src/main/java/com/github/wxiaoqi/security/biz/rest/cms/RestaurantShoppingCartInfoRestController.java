package com.github.wxiaoqi.security.biz.rest.cms;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.biz.biz.restaurant.RestaurantShoppingCartInfoBiz;
import com.github.wxiaoqi.security.biz.entity.restaurant.RestaurantShoppingCartInfo;
import com.github.wxiaoqi.security.common.rest.BaseController;

import lombok.extern.slf4j.Slf4j;

/**
 * 小程序点餐购物车信息相关接口
 * @author he
 *
 */
@Slf4j
@Controller
@RequestMapping("restaurant/shoppingCart")
public class RestaurantShoppingCartInfoRestController extends BaseController<RestaurantShoppingCartInfoBiz, RestaurantShoppingCartInfo> {
	
	@Autowired
	private RestaurantShoppingCartInfoBiz restaurantShoppingCartInfoBiz;
	
	/**
	 * 查询点餐购物车信息列表
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Map<String, Object> query(@RequestBody Map<String, Object> param) {
		log.info("查询点餐购物车信息列表------start------{}", param);
		Map<String, Object> respMap = restaurantShoppingCartInfoBiz.queryRestaurantShoppingCartInfoPage(param);
		if (respMap.get("code").equals("00")) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("查询点餐购物车信息列表-----end-------{}", respMap);
		return respMap;
	}

}
