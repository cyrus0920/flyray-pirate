package com.github.wxiaoqi.security.biz.biz.restaurant;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.biz.entity.restaurant.RestaurantDishesCategoryInfo;
import com.github.wxiaoqi.security.biz.entity.restaurant.RestaurantDishesInfo;
import com.github.wxiaoqi.security.biz.entity.restaurant.RestaurantShoppingCartInfo;
import com.github.wxiaoqi.security.biz.mapper.RestaurantDishesCategoryInfoMapper;
import com.github.wxiaoqi.security.biz.mapper.RestaurantDishesInfoMapper;
import com.github.wxiaoqi.security.biz.mapper.RestaurantShoppingCartInfoMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.util.EntityUtils;
import com.github.wxiaoqi.security.common.util.Query;

/**
 * 购物车信息表
 *
 * @author he
 * @date 2018-06-29 10:31:27
 */
@Service
public class RestaurantShoppingCartInfoBiz extends BaseBiz<RestaurantShoppingCartInfoMapper,RestaurantShoppingCartInfo> {

	private static final Logger logger = (Logger) LoggerFactory.getLogger(RestaurantShoppingCartInfoBiz.class);

	@Autowired
	private RestaurantShoppingCartInfoMapper restaurantShoppingCartInfoMapper;
	@Autowired
	private RestaurantDishesInfoMapper restaurantDishesInfoMapper;
	@Autowired
	private RestaurantDishesCategoryInfoMapper restaurantDishesCategoryInfoMapper;

	/**
	 * 点餐购物车/已选菜单信息查询
	 * @param request
	 * @return
	 */
	public Map<String, Object> queryOrderShoppingCartInfo(Map<String, Object> request){
		logger.info("点餐购物车/已选菜单信息查询请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platFormId");
		String merId = (String) request.get("merId");
		String tableId = (String) request.get("tableId");
		RestaurantShoppingCartInfo restaurantShoppingCartInfo = new RestaurantShoppingCartInfo();
		restaurantShoppingCartInfo.setPlatformId(platFormId);
		restaurantShoppingCartInfo.setMerId(merId);
		restaurantShoppingCartInfo.setTableId(Integer.valueOf(tableId));
		restaurantShoppingCartInfo.setStatus("1");//已选择
		restaurantShoppingCartInfo.setType("1");//点餐
		//查询点餐购物车菜品列表
		List<RestaurantShoppingCartInfo> shoppingCartList = restaurantShoppingCartInfoMapper.select(restaurantShoppingCartInfo);
		//查询点餐购物车菜品总价
		String shoppingCartPrice = restaurantShoppingCartInfoMapper.queryCountByPrice(restaurantShoppingCartInfo);
		//查询点餐购物车菜品数量
		Integer shoppingCartCount = restaurantShoppingCartInfoMapper.queryCountByCount(restaurantShoppingCartInfo);
		response.put("shoppingCartPrice", shoppingCartPrice);
		response.put("shoppingCartList", shoppingCartList);
		if(null == shoppingCartCount){
			response.put("shoppingCartCount", 0);
		}else{
			response.put("shoppingCartCount", shoppingCartCount);
		}

		restaurantShoppingCartInfo.setStatus("2");//已提交
		//查询已选菜单列表
		List<RestaurantShoppingCartInfo> submitList = restaurantShoppingCartInfoMapper.select(restaurantShoppingCartInfo);
		List<Map<String, Object>> submitMapList = new ArrayList<Map<String, Object>>();
		for(RestaurantShoppingCartInfo cartInfo:submitList){
			Map<String, Object> map = EntityUtils.beanToMap(cartInfo);
			Integer dishesId = cartInfo.getDishesId();
			RestaurantDishesInfo restaurantDishesInfo = new RestaurantDishesInfo();
			restaurantDishesInfo.setDishesId(String.valueOf(dishesId));
			RestaurantDishesInfo selectDishesInfo = restaurantDishesInfoMapper.selectOne(restaurantDishesInfo);
			Integer categoryId = selectDishesInfo.getCategoryId();
			RestaurantDishesCategoryInfo restaurantDishesCategoryInfo = new RestaurantDishesCategoryInfo();
			restaurantDishesCategoryInfo.setCategoryId(String.valueOf(categoryId));
			RestaurantDishesCategoryInfo selectCategoryInfo = restaurantDishesCategoryInfoMapper.selectOne(restaurantDishesCategoryInfo);
			map.put("imageUrl", selectDishesInfo.getImageUrl());
			map.put("picUrl", selectDishesInfo.getImageUrl());
			map.put("categoryName", selectCategoryInfo.getName());
			map.put("goodsName", cartInfo.getDishesName());
			map.put("retailPrice", cartInfo.getDishesPrice());
			map.put("number", cartInfo.getDishesNum());
			submitMapList.add(map);
		}
		//查询已选菜单总价
		String submitPrice = restaurantShoppingCartInfoMapper.queryCountByPrice(restaurantShoppingCartInfo);
		response.put("submitPrice", submitPrice);
		response.put("submitList", submitMapList);
		response.put("submitCount", submitList.size());
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		logger.info("点餐购物车/已选菜单信息查询响应。。。。。。{}", response);
		return response;
	}

	/**
	 * 点餐购物车增加
	 * @param request
	 * @return
	 */
	public Map<String, Object> orderShoppingCartAdd(Map<String, Object> request){
		logger.info("点餐购物车增加请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platFormId");
		String merId = (String) request.get("merId");
		String tableId = (String) request.get("tableId");
		String dishesId = (String) request.get("dishesId");
		//根据菜品id查询出菜品单价
		RestaurantDishesInfo restaurantDishesInfo = new RestaurantDishesInfo();
		restaurantDishesInfo.setDishesId(dishesId);
		RestaurantDishesInfo selectDishesInfo = restaurantDishesInfoMapper.selectOne(restaurantDishesInfo);
		String price = selectDishesInfo.getPrice();
		//根据条件查询，如果存在，则在原先基础上增加；如果不存在，则新增
		RestaurantShoppingCartInfo restaurantShoppingCartInfo = new RestaurantShoppingCartInfo();
		restaurantShoppingCartInfo.setPlatformId(platFormId);
		restaurantShoppingCartInfo.setMerId(merId);
		restaurantShoppingCartInfo.setTableId(Integer.valueOf(tableId));
		restaurantShoppingCartInfo.setDishesId(Integer.valueOf(dishesId));
		restaurantShoppingCartInfo.setStatus("1");//已选择
		restaurantShoppingCartInfo.setType("1");//点餐
		RestaurantShoppingCartInfo selectOne = restaurantShoppingCartInfoMapper.selectOne(restaurantShoppingCartInfo);
		if(null != selectOne){
			Integer dishesNum = selectOne.getDishesNum();
			String dishesPrice = selectOne.getDishesPrice();
			BigDecimal dishesPriceDecimal = new BigDecimal(dishesPrice);
			BigDecimal priceDecimal = new BigDecimal(price);
			BigDecimal addPrice = dishesPriceDecimal.add(priceDecimal);
			selectOne.setDishesNum(dishesNum+1);
			selectOne.setDishesPrice(String.valueOf(addPrice));
			restaurantShoppingCartInfoMapper.updateByPrimaryKey(selectOne);
		}else{
			restaurantShoppingCartInfo.setDishesNum(1);
			restaurantShoppingCartInfo.setDishesPrice(price);
			restaurantShoppingCartInfo.setDishesName(selectDishesInfo.getDishesName());
			restaurantShoppingCartInfoMapper.insert(restaurantShoppingCartInfo);
		}
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		logger.info("点餐购物车增加响应。。。。。。{}", response);
		return response;
	}

	/**
	 * 点餐购物车减少
	 * @param request
	 * @return
	 */
	public Map<String, Object> orderShoppingCartCut(Map<String, Object> request){
		logger.info("点餐购物车减少请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platFormId");
		String merId = (String) request.get("merId");
		String tableId = (String) request.get("tableId");
		String dishesId = (String) request.get("dishesId");
		//根据菜品id查询出菜品单价
		RestaurantDishesInfo restaurantDishesInfo = new RestaurantDishesInfo();
		restaurantDishesInfo.setDishesId(dishesId);
		RestaurantDishesInfo selectDishesInfo = restaurantDishesInfoMapper.selectOne(restaurantDishesInfo);
		String price = selectDishesInfo.getPrice();
		//根据条件查询，如果数量大于1，则在原基础上减少；如果数量等于1，直接删除
		RestaurantShoppingCartInfo restaurantShoppingCartInfo = new RestaurantShoppingCartInfo();
		restaurantShoppingCartInfo.setPlatformId(platFormId);
		restaurantShoppingCartInfo.setMerId(merId);
		restaurantShoppingCartInfo.setTableId(Integer.valueOf(tableId));
		restaurantShoppingCartInfo.setDishesId(Integer.valueOf(dishesId));
		restaurantShoppingCartInfo.setStatus("1");//已选择
		restaurantShoppingCartInfo.setType("1");//点餐
		RestaurantShoppingCartInfo selectOne = restaurantShoppingCartInfoMapper.selectOne(restaurantShoppingCartInfo);
		if(selectOne.getDishesNum() > 1){
			Integer dishesNum = selectOne.getDishesNum();
			String dishesPrice = selectOne.getDishesPrice();
			BigDecimal dishesPriceDecimal = new BigDecimal(dishesPrice);
			BigDecimal priceDecimal = new BigDecimal(price);
			BigDecimal addPrice = dishesPriceDecimal.subtract(priceDecimal);
			selectOne.setDishesNum(dishesNum-1);
			selectOne.setDishesPrice(String.valueOf(addPrice));
			restaurantShoppingCartInfoMapper.updateByPrimaryKey(selectOne);
		}else{
			restaurantShoppingCartInfoMapper.delete(selectOne);
		}
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		logger.info("点餐购物车减少响应。。。。。。{}", response);
		return response;
	}

	/**
	 * 点餐购物车删除菜系
	 * @param request
	 * @return
	 */
	public Map<String, Object> orderShoppingCartDel(Map<String, Object> request){
		logger.info("点餐购物车删除菜系请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platFormId");
		String merId = (String) request.get("merId");
		String tableId = (String) request.get("tableId");
		String dishesId = (String) request.get("dishesId");
		RestaurantShoppingCartInfo restaurantShoppingCartInfo = new RestaurantShoppingCartInfo();
		restaurantShoppingCartInfo.setPlatformId(platFormId);
		restaurantShoppingCartInfo.setMerId(merId);
		restaurantShoppingCartInfo.setTableId(Integer.valueOf(tableId));
		restaurantShoppingCartInfo.setDishesId(Integer.valueOf(dishesId));
		restaurantShoppingCartInfo.setStatus("1");//已选择
		restaurantShoppingCartInfo.setType("1");//点餐
		RestaurantShoppingCartInfo selectOne = restaurantShoppingCartInfoMapper.selectOne(restaurantShoppingCartInfo);
		restaurantShoppingCartInfoMapper.delete(selectOne);
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		logger.info("点餐购物车删除菜系响应。。。。。。{}", response);
		return response;
	}

	/**
	 * 点餐购物车清空
	 * @param request
	 * @return
	 */
	public Map<String, Object> orderShoppingCartClear(Map<String, Object> request){
		logger.info("点餐购物车清空请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platFormId");
		String merId = (String) request.get("merId");
		String tableId = (String) request.get("tableId");
		RestaurantShoppingCartInfo restaurantShoppingCartInfo = new RestaurantShoppingCartInfo();
		restaurantShoppingCartInfo.setPlatformId(platFormId);
		restaurantShoppingCartInfo.setMerId(merId);
		restaurantShoppingCartInfo.setTableId(Integer.valueOf(tableId));
		restaurantShoppingCartInfo.setStatus("1");//已选择
		restaurantShoppingCartInfo.setType("1");//点餐
		List<RestaurantShoppingCartInfo> selectList = restaurantShoppingCartInfoMapper.select(restaurantShoppingCartInfo);
		for(RestaurantShoppingCartInfo oneInfo:selectList){
			restaurantShoppingCartInfoMapper.delete(oneInfo);
		}
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		logger.info("点餐购物车清空响应。。。。。。{}", response);
		return response;
	}

	/**
	 * 点餐购物车提交
	 * @param request
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> orderShoppingCartSubmit(Map<String, Object> request){
		logger.info("点餐购物车提交请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platFormId");
		String merId = (String) request.get("merId");

		String orderInfo = (String) request.get("orderInfo");
		JSONObject orderInfoObject = (JSONObject) JSONObject.parse(orderInfo);
		String tableId = (String) orderInfoObject.get("tableId");
		List<Map<String,Object>> disgesInfo = (List<Map<String, Object>>) orderInfoObject.get("disgesInfo");
		for(Map<String,Object> disgesMap:disgesInfo){
			Integer dishesId = (Integer) disgesMap.get("dishesId");
			RestaurantShoppingCartInfo restaurantShoppingCartInfo = new RestaurantShoppingCartInfo();
			restaurantShoppingCartInfo.setPlatformId(platFormId);
			restaurantShoppingCartInfo.setMerId(merId);
			restaurantShoppingCartInfo.setTableId(Integer.valueOf(tableId));
			restaurantShoppingCartInfo.setDishesId(Integer.valueOf(dishesId));
			restaurantShoppingCartInfo.setStatus("1");//已选择
			restaurantShoppingCartInfo.setType("1");//点餐
			RestaurantShoppingCartInfo selectOne = restaurantShoppingCartInfoMapper.selectOne(restaurantShoppingCartInfo);
			selectOne.setStatus("2");//已提交
			restaurantShoppingCartInfoMapper.updateByPrimaryKey(selectOne);
		}
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		logger.info("点餐购物车提交响应。。。。。。{}", response);
		return response;
	}

	/**
	 * 预订/外卖购物车信息查询
	 * @param request
	 * @return
	 */
	public Map<String, Object> queryReservationShoppingCartInfo(Map<String, Object> request){
		logger.info("预订/外卖购物车信息查询请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platFormId");
		String merId = (String) request.get("merId");
		String perId = (String) request.get("perId");
		String type = (String) request.get("type");
		RestaurantShoppingCartInfo restaurantShoppingCartInfo = new RestaurantShoppingCartInfo();
		restaurantShoppingCartInfo.setPlatformId(platFormId);
		restaurantShoppingCartInfo.setMerId(merId);
		restaurantShoppingCartInfo.setPerId(perId);
		restaurantShoppingCartInfo.setStatus("1");//已选择
		restaurantShoppingCartInfo.setType(type);//1点餐 2外卖
		//查询预订购物车菜品列表
		List<RestaurantShoppingCartInfo> shoppingCartList = restaurantShoppingCartInfoMapper.select(restaurantShoppingCartInfo);
		List<Map<String, Object>> dishesList = new ArrayList<Map<String, Object>>();
		for(RestaurantShoppingCartInfo cartInfo: shoppingCartList){
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("goodsName", cartInfo.getDishesName());
			map.put("number", cartInfo.getDishesNum());
			map.put("retailPrice", cartInfo.getDishesPrice());
			//根据菜品id查询出菜品单价
			RestaurantDishesInfo restaurantDishesInfo = new RestaurantDishesInfo();
			restaurantDishesInfo.setDishesId(String.valueOf(cartInfo.getDishesId()));
			RestaurantDishesInfo selectDishesInfo = restaurantDishesInfoMapper.selectOne(restaurantDishesInfo);
			map.put("picUrl", selectDishesInfo.getImageUrl());
			dishesList.add(map);
		}
		
		//查询预订购物车菜品总价
		String shoppingCartPrice = restaurantShoppingCartInfoMapper.queryCountByPrice(restaurantShoppingCartInfo);
		//查询预订购物车菜品数量
		Integer shoppingCartCount = restaurantShoppingCartInfoMapper.queryCountByCount(restaurantShoppingCartInfo);
		response.put("shoppingCartPrice", shoppingCartPrice);
		response.put("shoppingCartList", dishesList);
		response.put("shoppingCartsList", shoppingCartList);
		if(null == shoppingCartCount){
			response.put("shoppingCartCount", 0);
		}else{
			response.put("shoppingCartCount", shoppingCartCount);
		}
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		logger.info("预订/外卖购物车信息查询响应。。。。。。{}", response);
		return response;
	}

	/**
	 * 预订/外卖购物车增加
	 * @param request
	 * @return
	 */
	public Map<String, Object> reservationShoppingCartAdd(Map<String, Object> request){
		logger.info("预订/外卖购物车增加请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platFormId");
		String merId = (String) request.get("merId");
		String perId = (String) request.get("perId");
		String dishesId = (String) request.get("dishesId");
		String type = (String) request.get("type");
		//根据菜品id查询出菜品单价
		RestaurantDishesInfo restaurantDishesInfo = new RestaurantDishesInfo();
		restaurantDishesInfo.setDishesId(dishesId);
		RestaurantDishesInfo selectDishesInfo = restaurantDishesInfoMapper.selectOne(restaurantDishesInfo);
		String price = selectDishesInfo.getPrice();
		//根据条件查询，如果存在，则在原先基础上增加；如果不存在，则新增
		RestaurantShoppingCartInfo restaurantShoppingCartInfo = new RestaurantShoppingCartInfo();
		restaurantShoppingCartInfo.setPlatformId(platFormId);
		restaurantShoppingCartInfo.setMerId(merId);
		restaurantShoppingCartInfo.setPerId(perId);
		restaurantShoppingCartInfo.setDishesId(Integer.valueOf(dishesId));
		restaurantShoppingCartInfo.setStatus("1");//已选择
		restaurantShoppingCartInfo.setType(type);//1点餐 2外卖
		RestaurantShoppingCartInfo selectOne = restaurantShoppingCartInfoMapper.selectOne(restaurantShoppingCartInfo);
		if(null != selectOne){
			Integer dishesNum = selectOne.getDishesNum();
			String dishesPrice = selectOne.getDishesPrice();
			BigDecimal dishesPriceDecimal = new BigDecimal(dishesPrice);
			BigDecimal priceDecimal = new BigDecimal(price);
			BigDecimal addPrice = dishesPriceDecimal.add(priceDecimal);
			selectOne.setDishesNum(dishesNum+1);
			selectOne.setDishesPrice(String.valueOf(addPrice));
			restaurantShoppingCartInfoMapper.updateByPrimaryKey(selectOne);
		}else{
			restaurantShoppingCartInfo.setDishesNum(1);
			restaurantShoppingCartInfo.setDishesPrice(price);
			restaurantShoppingCartInfo.setDishesName(selectDishesInfo.getDishesName());
			restaurantShoppingCartInfoMapper.insert(restaurantShoppingCartInfo);
		}
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		logger.info("预订/外卖购物车增加响应。。。。。。{}", response);
		return response;
	}

	/**
	 * 预订/外卖购物车减少
	 * @param request
	 * @return
	 */
	public Map<String, Object> reservationShoppingCartCut(Map<String, Object> request){
		logger.info("预订/外卖购物车减少请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platFormId");
		String merId = (String) request.get("merId");
		String perId = (String) request.get("perId");
		String dishesId = (String) request.get("dishesId");
		String type = (String) request.get("type");
		//根据菜品id查询出菜品单价
		RestaurantDishesInfo restaurantDishesInfo = new RestaurantDishesInfo();
		restaurantDishesInfo.setDishesId(dishesId);
		RestaurantDishesInfo selectDishesInfo = restaurantDishesInfoMapper.selectOne(restaurantDishesInfo);
		String price = selectDishesInfo.getPrice();
		//根据条件查询，如果数量大于1，则在原基础上减少；如果数量等于1，直接删除
		RestaurantShoppingCartInfo restaurantShoppingCartInfo = new RestaurantShoppingCartInfo();
		restaurantShoppingCartInfo.setPlatformId(platFormId);
		restaurantShoppingCartInfo.setMerId(merId);
		restaurantShoppingCartInfo.setPerId(perId);
		restaurantShoppingCartInfo.setDishesId(Integer.valueOf(dishesId));
		restaurantShoppingCartInfo.setStatus("1");//已选择
		restaurantShoppingCartInfo.setType(type);//1点餐 2外卖
		RestaurantShoppingCartInfo selectOne = restaurantShoppingCartInfoMapper.selectOne(restaurantShoppingCartInfo);
		if(selectOne.getDishesNum() > 1){
			Integer dishesNum = selectOne.getDishesNum();
			String dishesPrice = selectOne.getDishesPrice();
			BigDecimal dishesPriceDecimal = new BigDecimal(dishesPrice);
			BigDecimal priceDecimal = new BigDecimal(price);
			BigDecimal addPrice = dishesPriceDecimal.subtract(priceDecimal);
			selectOne.setDishesNum(dishesNum-1);
			selectOne.setDishesPrice(String.valueOf(addPrice));
			restaurantShoppingCartInfoMapper.updateByPrimaryKey(selectOne);
		}else{
			restaurantShoppingCartInfoMapper.delete(selectOne);
		}
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		logger.info("预订/外卖购物车减少响应。。。。。。{}", response);
		return response;
	}

	/**
	 * 预订/外卖购物车删除菜系
	 * @param request
	 * @return
	 */
	public Map<String, Object> reservationShoppingCartDel(Map<String, Object> request){
		logger.info("预订/外卖购物车删除菜系请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platFormId");
		String merId = (String) request.get("merId");
		String perId = (String) request.get("perId");
		String dishesId = (String) request.get("dishesId");
		String type = (String) request.get("type");
		RestaurantShoppingCartInfo restaurantShoppingCartInfo = new RestaurantShoppingCartInfo();
		restaurantShoppingCartInfo.setPlatformId(platFormId);
		restaurantShoppingCartInfo.setMerId(merId);
		restaurantShoppingCartInfo.setPerId(perId);
		restaurantShoppingCartInfo.setDishesId(Integer.valueOf(dishesId));
		restaurantShoppingCartInfo.setStatus("1");//已选择
		restaurantShoppingCartInfo.setType(type);//1点餐 2外卖
		RestaurantShoppingCartInfo selectOne = restaurantShoppingCartInfoMapper.selectOne(restaurantShoppingCartInfo);
		restaurantShoppingCartInfoMapper.delete(selectOne);
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		logger.info("预订/外卖购物车删除菜系响应。。。。。。{}", response);
		return response;
	}

	/**
	 * 预订/外卖购物车清空
	 * @param request
	 * @return
	 */
	public Map<String, Object> reservationShoppingCartClear(Map<String, Object> request){
		logger.info("预订/外卖购物车清空请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platFormId");
		String merId = (String) request.get("merId");
		String perId = (String) request.get("perId");
		String type = (String) request.get("type");
		RestaurantShoppingCartInfo restaurantShoppingCartInfo = new RestaurantShoppingCartInfo();
		restaurantShoppingCartInfo.setPlatformId(platFormId);
		restaurantShoppingCartInfo.setMerId(merId);
		restaurantShoppingCartInfo.setPerId(perId);
		restaurantShoppingCartInfo.setStatus("1");//已选择
		restaurantShoppingCartInfo.setType(type);//1点餐 2外卖
		List<RestaurantShoppingCartInfo> selectList = restaurantShoppingCartInfoMapper.select(restaurantShoppingCartInfo);
		for(RestaurantShoppingCartInfo oneInfo:selectList){
			restaurantShoppingCartInfoMapper.delete(oneInfo);
		}
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		logger.info("预订/外卖购物车清空响应。。。。。。{}", response);
		return response;
	}
	
	/**
	 * 查询购物车信息列表
	 * @param param
	 * @return
	 */
	public Map<String, Object> queryRestaurantShoppingCartInfoPage(Map<String, Object> param) {
		logger.info("查询购物车信息列表。。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		try {
			Query query = new Query(param);
			Page<RestaurantShoppingCartInfo> result = PageHelper.startPage(query.getPage(), query.getLimit());
			List<RestaurantShoppingCartInfo> list = mapper.queryRestaurantShoppingCartInfo(param);
			TableResultResponse<RestaurantShoppingCartInfo> table = new TableResultResponse<>(result.getTotal(), list);
			respMap.put("code", "00");
			respMap.put("msg", "查询成功");
			respMap.put("body", table);
		} catch (Exception e) {
			respMap.put("code", "01");
			respMap.put("msg", "查询异常");
		}
		return respMap;
	}

}