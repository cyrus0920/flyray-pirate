package com.github.wxiaoqi.security.biz.rest.cms;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.biz.biz.auction.AuctionRecordingInfoBiz;
import com.github.wxiaoqi.security.biz.entity.auction.AuctionRecordingInfo;
import com.github.wxiaoqi.security.common.rest.BaseController;

import lombok.extern.slf4j.Slf4j;

/**
 * 小程序竞拍记录相关接口
 * @author he
 *
 */
@Slf4j
@Controller
@RequestMapping("auction/recording")
public class AuctionRecordingRestController extends BaseController<AuctionRecordingInfoBiz, AuctionRecordingInfo> {
	
	@Autowired
	private AuctionRecordingInfoBiz auctionRecordingInfoBiz;
	
	/**
	 * 查询竞拍记录列表
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Map<String, Object> query(@RequestBody Map<String, Object> param) {
		log.info("查询竞拍记录列表------start------{}", param);
		Map<String, Object> respMap = auctionRecordingInfoBiz.queryAuctionRecordingPage(param);
		if (respMap.get("code").equals("00")) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("查询竞拍记录列表-----end-------{}", respMap);
		return respMap;
	}

}
