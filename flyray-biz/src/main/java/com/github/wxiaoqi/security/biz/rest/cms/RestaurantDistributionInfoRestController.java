package com.github.wxiaoqi.security.biz.rest.cms;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.biz.biz.restaurant.RestaurantDistributionInfoBiz;
import com.github.wxiaoqi.security.biz.entity.restaurant.RestaurantDistributionInfo;
import com.github.wxiaoqi.security.common.rest.BaseController;

import lombok.extern.slf4j.Slf4j;

/**
 * 小程序配送信息相关接口
 * @author he
 *
 */
@Slf4j
@Controller
@RequestMapping("restaurant/distribution")
public class RestaurantDistributionInfoRestController extends BaseController<RestaurantDistributionInfoBiz, RestaurantDistributionInfo> {
	
	@Autowired
	private RestaurantDistributionInfoBiz restaurantDistributionInfoBiz;
	
	/**
	 * 查询配送信息列表
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Map<String, Object> query(@RequestBody Map<String, Object> param) {
		log.info("查询配送信息列表------start------{}", param);
		Map<String, Object> respMap = restaurantDistributionInfoBiz.queryRestaurantDistributionInfoPage(param);
		if (respMap.get("code").equals("00")) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("查询配送信息列表-----end-------{}", respMap);
		return respMap;
	}

}
