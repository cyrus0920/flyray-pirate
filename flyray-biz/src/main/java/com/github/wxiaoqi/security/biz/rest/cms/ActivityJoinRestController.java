package com.github.wxiaoqi.security.biz.rest.cms;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.biz.biz.activity.ActivityCustomerBiz;

import lombok.extern.slf4j.Slf4j;

/**
 * 小程序活动相关接口
 * @author Administrator
 *
 */
@Slf4j
@Controller
@RequestMapping("activity/join")
public class ActivityJoinRestController {

	
	@Autowired
	private ActivityCustomerBiz customerBiz;
	
	
	
	@ResponseBody
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Map<String, Object> query(@RequestBody Map<String, Object>  param) {
		log.info("查询活动参加记录------start------{}", param);
		Map<String, Object> respMap = customerBiz.queryActivityJoin(param);
		if (respMap.get("code").equals("00")) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("查询活动参加记录-----end------{}", respMap);
		return respMap;
	}
	
}
