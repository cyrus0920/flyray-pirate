package com.github.wxiaoqi.security.biz.biz.restaurant;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.auth.common.util.SnowFlake;
import com.github.wxiaoqi.security.biz.entity.restaurant.RestaurantDishesInfo;
import com.github.wxiaoqi.security.biz.mapper.RestaurantDishesInfoMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.util.Query;

import lombok.extern.slf4j.Slf4j;

/**
 * 菜品信息表
 *
 * @author he
 * @date 2018-06-29 10:31:27
 */
@Slf4j
@Service
public class RestaurantDishesInfoBiz extends BaseBiz<RestaurantDishesInfoMapper,RestaurantDishesInfo> {
	
	
	@Autowired
	private RestaurantDishesInfoMapper restaurantDishesInfoMapper;
	
	/**
	 * 菜品详情查询
	 * @param request
	 * @return
	 */
	public Map<String, Object> queryDishesDetailInfo(Map<String, Object> request){
		log.info("菜品详情查询请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platFormId");
		String merId = (String) request.get("merId");
		String dishesId = (String) request.get("dishesId");
		RestaurantDishesInfo restaurantDishesInfo = new RestaurantDishesInfo();
		restaurantDishesInfo.setPlatformId(platFormId);
		restaurantDishesInfo.setMerId(merId);
		restaurantDishesInfo.setDishesId(dishesId);
		RestaurantDishesInfo selectDishesInfo = restaurantDishesInfoMapper.selectOne(restaurantDishesInfo);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("map", selectDishesInfo);
 		response.put("dishesInfo", map);
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		log.info("菜品详情查询响应。。。。。。{}", response);
		return response;
	}
	
	/**
	 * 查询菜品列表
	 * @param param
	 * @return
	 */
	public Map<String, Object> queryRestaurantDishesInfoPage(Map<String, Object> param) {
		log.info("查询菜品列表。。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		try {
			Query query = new Query(param);
			Page<RestaurantDishesInfo> result = PageHelper.startPage(query.getPage(), query.getLimit());
			List<RestaurantDishesInfo> list = mapper.queryRestaurantDishesInfo(param);
			TableResultResponse<RestaurantDishesInfo> table = new TableResultResponse<>(result.getTotal(), list);
			respMap.put("code", "00");
			respMap.put("msg", "查询成功");
			respMap.put("body", table);
		} catch (Exception e) {
			respMap.put("code", "01");
			respMap.put("msg", "查询异常");
		}
		return respMap;
	}
	
	/**
	 * 删除
	 * @param param
	 * @return
	 */
	public Map<String, Object> deleteRestaurantDishe(Map<String, Object> param) {
		log.info("删除菜品。。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		try {
			Integer id = (Integer) param.get("id");
			RestaurantDishesInfo dishesInfo = mapper.selectByPrimaryKey(Long.valueOf(id));
			if (dishesInfo != null) {
				mapper.delete(dishesInfo);
				respMap.put("code", "00");
				respMap.put("msg", "请求成功");
			}else {
				respMap.put("code", "01");
				respMap.put("msg", "记录不存在");
			}
		} catch (Exception e) {
			e.printStackTrace();
			respMap.put("code", "01");
			respMap.put("msg", "请求异常");
		}
		return respMap;
	}
	
	
	/**
	 * 添加
	 * @param param
	 * @return
	 */
	public Map<String, Object> addRestaurantDishe(Map<String, Object> param) {
		log.info("添加菜品。。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		try {
			String platformId = (String) param.get("platformId");
			String merId = (String) param.get("merId");
			String categoryId = (String) param.get("categoryId");
			String dishesName = (String) param.get("dishesName");
			String instructions = (String) param.get("instructions");
			String imageUrl = (String) param.get("imageUrl");
			String price = (String) param.get("price");
			String isSpecification = (String) param.get("isSpecification");
			String type = (String) param.get("type");
			RestaurantDishesInfo dishesInfo = new RestaurantDishesInfo();
			dishesInfo.setDishesId(String.valueOf(SnowFlake.getId()));
			dishesInfo.setPlatformId(platformId);
			dishesInfo.setMerId(merId);
			dishesInfo.setCategoryId(Integer.valueOf(categoryId));
			dishesInfo.setDishesName(dishesName);
			dishesInfo.setInstructions(instructions);
			dishesInfo.setImageUrl(imageUrl);
			dishesInfo.setOrderNum(0);
			dishesInfo.setPrice(price);
			dishesInfo.setIsSpecification(isSpecification);
			dishesInfo.setType(type);
			mapper.insert(dishesInfo);
			respMap.put("code", "00");
			respMap.put("msg", "请求成功");
		} catch (Exception e) {
			e.printStackTrace();
			respMap.put("code", "01");
			respMap.put("msg", "请求异常");
		}
		return respMap;
	}
	
	
	/**
	 * 修改
	 * @param param
	 * @return
	 */
	public Map<String, Object> updateRestaurantDishe(Map<String, Object> param) {
		log.info("修改菜品。。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		try {
			String categoryId = (String) param.get("categoryId");
			String dishesName = (String) param.get("dishesName");
			String instructions = (String) param.get("instructions");
			String imageUrl = (String) param.get("imageUrl");
			String price = (String) param.get("price");
			String isSpecification = (String) param.get("isSpecification");
			String type = (String) param.get("type");
			Integer id = (Integer) param.get("id");
			RestaurantDishesInfo dishesInfo = mapper.selectByPrimaryKey(Long.valueOf(id));
			if (dishesInfo != null) {
				dishesInfo.setCategoryId(Integer.valueOf(categoryId));
				dishesInfo.setDishesName(dishesName);
				dishesInfo.setInstructions(instructions);
				dishesInfo.setImageUrl(imageUrl);
				dishesInfo.setPrice(price);
				dishesInfo.setIsSpecification(isSpecification);
				dishesInfo.setType(type);
				mapper.updateByPrimaryKeySelective(dishesInfo);
				respMap.put("code", "00");
				respMap.put("msg", "请求成功");
			}else {
				respMap.put("code", "01");
				respMap.put("msg", "记录不存在");
			}
			respMap.put("code", "00");
			respMap.put("msg", "请求成功");
		} catch (Exception e) {
			e.printStackTrace();
			respMap.put("code", "01");
			respMap.put("msg", "请求异常");
		}
		return respMap;
	}
}