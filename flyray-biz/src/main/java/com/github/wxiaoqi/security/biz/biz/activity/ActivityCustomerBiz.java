package com.github.wxiaoqi.security.biz.biz.activity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.biz.entity.activity.ActivityCustomer;
import com.github.wxiaoqi.security.biz.entity.activity.ActivityInfo;
import com.github.wxiaoqi.security.biz.mapper.ActivityCustomerMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.util.Query;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 *
 * @author chj
 * @email 
 * @date 2018-07-09 14:10:43
 */
@Slf4j
@Service
public class ActivityCustomerBiz extends BaseBiz<ActivityCustomerMapper,ActivityCustomer> {

	
	
	/**
	 * 查询参加活动信息
	 * @param param
	 * @return
	 */
	public Map<String, Object> queryActivityJoin(Map<String, Object> param) {
		log.info("查询参加活动信息。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		try {
			Query query = new Query(param);
			Page<ActivityCustomer> result = PageHelper.startPage(query.getPage(), query.getLimit());
			List<ActivityCustomer> list = mapper.queryActivityJoins(param);
			TableResultResponse<ActivityCustomer> table = new TableResultResponse<>(result.getTotal(), list);
			respMap.put("code", "00");
			respMap.put("msg", "查询成功");
			respMap.put("body", table);
		} catch (Exception e) {
			respMap.put("code", "01");
			respMap.put("msg", "查询异常");
		}
		return respMap;
	}
}