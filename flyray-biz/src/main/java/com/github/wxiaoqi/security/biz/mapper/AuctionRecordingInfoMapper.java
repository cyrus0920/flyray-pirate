package com.github.wxiaoqi.security.biz.mapper;

import java.util.List;
import java.util.Map;

import com.github.wxiaoqi.security.biz.entity.auction.AuctionRecordingInfo;

import tk.mybatis.mapper.common.Mapper;

/**
 * 竞拍记录表
 * 
 * @author he
 * @date 2018-07-17 13:44:31
 */
@org.apache.ibatis.annotations.Mapper
public interface AuctionRecordingInfoMapper extends Mapper<AuctionRecordingInfo> {
	
	List<AuctionRecordingInfo> queryAuctionRecording(Map<String, Object> map);
	
}
