package com.github.wxiaoqi.security.biz.rest.cms;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.biz.biz.fightGroup.FightGroupMemberInfoBiz;
import com.github.wxiaoqi.security.biz.entity.fightGroup.FightGroupMemberInfo;
import com.github.wxiaoqi.security.common.rest.BaseController;

import lombok.extern.slf4j.Slf4j;

/**
 * 小程序拼团团成员信息相关接口
 * @author he
 *
 */
@Slf4j
@Controller
@RequestMapping("fightGroup/memberInfo")
public class FightGroupMemberInfoRestController extends BaseController<FightGroupMemberInfoBiz, FightGroupMemberInfo> {
	
	@Autowired
	private FightGroupMemberInfoBiz fightGroupMemberInfoBiz;
	
	/**
	 * 查询拼团团信息列表
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Map<String, Object> query(@RequestBody Map<String, Object> param) {
		log.info("查询拼团团成员信息列表------start------{}", param);
		Map<String, Object> respMap = fightGroupMemberInfoBiz.queryFightGroupMemberInfoPage(param);
		if (respMap.get("code").equals("00")) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("查询拼团团成员信息列表-----end-------{}", respMap);
		return respMap;
	}

}
