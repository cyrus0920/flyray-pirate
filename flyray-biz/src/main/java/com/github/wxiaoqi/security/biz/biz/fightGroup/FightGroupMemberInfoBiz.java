package com.github.wxiaoqi.security.biz.biz.fightGroup;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.biz.entity.fightGroup.FightGroupMemberInfo;
import com.github.wxiaoqi.security.biz.mapper.FightGroupMemberInfoMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.util.Query;

/**
 * 团成员信息表
 *
 * @author he
 * @date 2018-07-12 10:17:33
 */
@Service
public class FightGroupMemberInfoBiz extends BaseBiz<FightGroupMemberInfoMapper,FightGroupMemberInfo> {
	
	private static final Logger logger = (Logger) LoggerFactory.getLogger(FightGroupMemberInfoBiz.class);
	
	/**
	 * 查询拼团团成员信息列表
	 * @param param
	 * @return
	 */
	public Map<String, Object> queryFightGroupMemberInfoPage(Map<String, Object> param) {
		logger.info("查询拼团团成员信息列表。。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		try {
			Query query = new Query(param);
			Page<FightGroupMemberInfo> result = PageHelper.startPage(query.getPage(), query.getLimit());
			List<FightGroupMemberInfo> list = mapper.queryFightGroupMemberInfo(param);
			TableResultResponse<FightGroupMemberInfo> table = new TableResultResponse<>(result.getTotal(), list);
			respMap.put("code", "00");
			respMap.put("msg", "查询成功");
			respMap.put("body", table);
		} catch (Exception e) {
			respMap.put("code", "01");
			respMap.put("msg", "查询异常");
		}
		return respMap;
	}
}