package com.github.wxiaoqi.security.biz.biz.restaurant;

import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.biz.entity.restaurant.RestaurantPictureInfo;
import com.github.wxiaoqi.security.biz.mapper.RestaurantPictureInfoMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

/**
 * 餐厅照片信息表
 *
 * @author Mr.AG
 * @date 2018-06-29 10:31:27
 */
@Service
public class RestaurantPictureInfoBiz extends BaseBiz<RestaurantPictureInfoMapper,RestaurantPictureInfo> {
}