package com.github.wxiaoqi.security.biz.api;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.biz.biz.pay.PayBiz;
import com.github.wxiaoqi.security.common.cms.request.MinePayParam;
import com.github.wxiaoqi.security.common.util.EntityUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 支付相关接口
 * @author he
 *
 */
@Api(tags="支付管理")
@Controller
@RequestMapping("api/pay")
public class PayController extends BaseController {
	
	@Autowired
	private PayBiz payBiz;
	
	/**
	 * 小程序支付
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("小程序支付")
	@RequestMapping(value = "/miniPay",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> miniPay(@RequestBody @Valid MinePayParam minePayParam) throws Exception {
		Map<String, Object> response = payBiz.miniPay(EntityUtils.beanToMap(minePayParam));
		return response;
    }

}
