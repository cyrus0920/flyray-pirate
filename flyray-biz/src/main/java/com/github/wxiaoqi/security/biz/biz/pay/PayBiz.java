package com.github.wxiaoqi.security.biz.biz.pay;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.alibaba.fastjson.JSONObject;
import com.github.wxiaoqi.security.auth.common.util.SnowFlake;
import com.github.wxiaoqi.security.biz.entity.CmsScenesOrder;
import com.github.wxiaoqi.security.biz.entity.auction.AuctionOrder;
import com.github.wxiaoqi.security.biz.feign.PayFeign;
import com.github.wxiaoqi.security.biz.mapper.AuctionOrderMapper;
import com.github.wxiaoqi.security.biz.mapper.CmsScenesOrderMapper;
import com.github.wxiaoqi.security.common.msg.ResponseCode;

/**
 * 支付
 * @author he
 *
 */
@Service
public class PayBiz {
	
	private static final Logger logger = (Logger) LoggerFactory.getLogger(PayBiz.class);
	
	@Autowired
	private PayFeign payFeign;
	@Autowired
	private CmsScenesOrderMapper cmsScenesOrderMapper;
	@Autowired
	private AuctionOrderMapper auctionOrderMapper;
	
	/**
	 * 小程序支付
	 * @param request
	 * @return
	 */
	public Map<String, Object> miniPay(@RequestBody Map<String, Object> params){
		
		logger.info("小程序支付请求.......{}",params);
		Map<String, Object> respMap = new HashMap<String, Object>();
		
		//创建订单
		String payOrderNo = String.valueOf(SnowFlake.getId());
		params.put("payOrderNo", payOrderNo);
		Map<String, Object> createPayOrderRespMap = createPayOrder(params);
		if(!ResponseCode.OK.getCode().equals(createPayOrderRespMap.get("code"))){
			respMap.put("success", false);
			respMap.put("code", ResponseCode.PAY_FAIL.getCode());
			respMap.put("msg", ResponseCode.PAY_FAIL.getMessage());
			return respMap;
		}
		
		//发起小程序支付
		Map<String, Object> doPayMap = doPay(params);
		if(!ResponseCode.OK.getCode().equals(doPayMap.get("code"))){
			respMap.put("success", false);
			respMap.put("code", ResponseCode.PAY_FAIL.getCode());
			respMap.put("msg", ResponseCode.PAY_FAIL.getMessage());
			return respMap;
		}
		
		//创建场景订单
		this.createScenesOrder(params);
		
		respMap.put("payObject", doPayMap.get("payObject"));
		respMap.put("success", true);
		respMap.put("code", ResponseCode.OK.getCode());
		respMap.put("msg", ResponseCode.OK.getMessage());
		return respMap;
	}
	
	/**
	 * 创建订单
	 * @param params
	 * @return
	 */
	private Map<String, Object> createPayOrder(Map<String, Object> params){
		Map<String, Object> createPayOrderMap = new HashMap<String, Object>();
		createPayOrderMap.put("platformId", (String)params.get("platformId"));
		createPayOrderMap.put("merId", (String)params.get("merId"));
		createPayOrderMap.put("perId", (String)params.get("perId"));
		createPayOrderMap.put("payOrderNo", (String)params.get("payOrderNo"));
		createPayOrderMap.put("orderAmt", (String)params.get("orderAmt"));
		createPayOrderMap.put("payAmt", (String)params.get("payAmt"));
		createPayOrderMap.put("body", (String)params.get("body"));
		createPayOrderMap.put("payCode", (String)params.get("payCode"));//1支付，2充值
		logger.info("创建订单请求.......{}",createPayOrderMap);
		Map<String, Object> createRespMap = payFeign.createPayOrder(createPayOrderMap);
		logger.info("创建订单响应.......{}",createRespMap);
		return createRespMap;
	}
	
	/**
	 * 小程序支付
	 * @param params
	 * @return
	 */
	private Map<String, Object> doPay(Map<String, Object> params){
		Map<String, Object> doPayMap = new HashMap<String, Object>();
		doPayMap.put("platformId", (String)params.get("platformId"));
		doPayMap.put("payChannelNo", "wechat003");//微信公众号/小程序支付
		doPayMap.put("payCompanyNo", "WECHAT");//微信
		doPayMap.put("payMethod", "02");//微信
		Map<String, Object> extValue = new HashMap<String, Object>();
		extValue.put("openId", (String)params.get("openId"));
		doPayMap.put("extValue", extValue);
		logger.info("小程序支付请求.......{}",doPayMap);
		Map<String, Object> doPayRespMap = payFeign.doPay(doPayMap);
		logger.info("小程序支付响应.......{}",doPayRespMap);
		return doPayRespMap;
	}

	/**
	 * 创建场景订单
	 * @param params
	 * @return
	 */
	private void createScenesOrder(Map<String, Object> params){
		//添加场景总订单表信息
		CmsScenesOrder cmsScenesOrder = new CmsScenesOrder();
		cmsScenesOrder.setPlatformId((String)params.get("platformId"));
		cmsScenesOrder.setMerId((String)params.get("merId"));
		cmsScenesOrder.setPerId((String)params.get("perId"));
		cmsScenesOrder.setPayOrderNo((String)params.get("payOrderNo"));
		String scenesCode = (String) params.get("scenesCode");
		cmsScenesOrder.setScenesCode(scenesCode);
		cmsScenesOrder.setTxAmt(new BigDecimal((String)params.get("payAmt")));
		cmsScenesOrderMapper.insert(cmsScenesOrder);
		//添加各场景相关订单表
		if("1".equals(scenesCode)){
			//竞拍
			this.createAuctionOrder(params);
		}else if("2".equals(scenesCode)){
			//拼团
			
		}else if("3".equals(scenesCode)){
			//店内点餐
			
		}else if("4".equals(scenesCode)){
			//预订点餐
			
		}else if("5".equals(scenesCode)){
			//外卖
			
		}
	}
	
	/**
	 * 创建竞拍订单
	 * @param params
	 */
	private void createAuctionOrder(Map<String, Object> params){
		String extMap = (String) params.get("extMap");
		JSONObject jsonExtMap = (JSONObject) JSONObject.parse(extMap);
		AuctionOrder auctionOrder = new AuctionOrder();
		auctionOrder.setPlatformId((String) params.get("platformId"));
		auctionOrder.setMerId((String) params.get("merId"));
		auctionOrder.setGoodsId((String)jsonExtMap.get("goodsId"));
		auctionOrder.setPayOrderNo((String) params.get("payOrderNo"));
		auctionOrder.setCreateTime(new Date());
		auctionOrder.setPerId((String) params.get("perId"));
		auctionOrder.setTxAmt(new BigDecimal((String) params.get("payAmt")));
		auctionOrderMapper.insert(auctionOrder);
	}
}
