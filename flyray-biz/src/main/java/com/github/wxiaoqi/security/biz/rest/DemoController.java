package com.github.wxiaoqi.security.biz.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;

/**
 *　@author bolei
 *　@date Apr 4, 2018
　*　@description 
**/

@RestController
@RequestMapping("demo")
public class DemoController {
	
	@ApiOperation(value = "测试使用", notes = "")
	@RequestMapping(value = "/test", method = RequestMethod.GET)
    public String getTest() {
        return "ok";
    }
}
