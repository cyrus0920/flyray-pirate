package com.github.wxiaoqi.security.biz.rest.cms;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.auth.common.util.SnowFlake;
import com.github.wxiaoqi.security.biz.biz.auction.AuctionGoodsInfoBiz;
import com.github.wxiaoqi.security.biz.entity.auction.AuctionGoodsInfo;
import com.github.wxiaoqi.security.common.rest.BaseController;

import lombok.extern.slf4j.Slf4j;

/**
 * 小程序竞拍商品相关接口
 * @author he
 *
 */
@Slf4j
@Controller
@RequestMapping("auction/goods")
public class AuctionGoodsRestController extends BaseController<AuctionGoodsInfoBiz, AuctionGoodsInfo> {
	
	@Autowired
	private AuctionGoodsInfoBiz auctionGoodsInfoBiz;
	
	/**
	 * 查询竞拍商品列表
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Map<String, Object> query(@RequestBody Map<String, Object> param) {
		log.info("查询竞拍商品列表------start------{}", param);
		Map<String, Object> respMap = auctionGoodsInfoBiz.queryAuctionGoodsPage(param);
		if (respMap.get("code").equals("00")) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("查询竞拍商品列表-----end-------{}", respMap);
		return respMap;
	}
	
	/**
	 * 添加竞拍商品
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public Map<String, Object> add(@RequestBody Map<String, Object> param) {
		log.info("添加竞拍商品------start------{}", param);
		Map<String, Object> respMap = new HashMap<>();
		try {
			String id = String.valueOf(SnowFlake.getId());
			String platformId = (String) param.get("platformId");
			String goodsName = (String) param.get("goodsName");
			String goodsDescription = (String) param.get("goodsDescription");
			String goodsImg = (String) param.get("goodsImg");
			String startingPrice = (String) param.get("startingPrice");
			String deadlineTime = (String) param.get("deadlineTime");
			AuctionGoodsInfo auctionGoodsInfo = new AuctionGoodsInfo();
			auctionGoodsInfo.setCreateTime(new Date());
			auctionGoodsInfo.setCurrentPrice(new BigDecimal(startingPrice));
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			auctionGoodsInfo.setDeadlineTime(sdf.parse(deadlineTime));
			auctionGoodsInfo.setGoodsDescription(goodsDescription);
			auctionGoodsInfo.setGoodsId(id);
			auctionGoodsInfo.setGoodsImg(goodsImg);
			auctionGoodsInfo.setGoodsName(goodsName);
			auctionGoodsInfo.setPlatformId(platformId);
			auctionGoodsInfo.setStartingPrice(new BigDecimal(startingPrice));
			auctionGoodsInfoBiz.insert(auctionGoodsInfo);
			respMap.put("code", "00");
			respMap.put("msg", "添加成功");
		} catch (Exception e) {
			respMap.put("code", "01");
			respMap.put("msg", "添加异常");
		}
		log.info("添加竞拍商品-----end-------------{}");
		return respMap;
	}
	
	/**
	 * 删除商品
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public Map<String, Object> delete(@RequestBody Map<String, Object> param) {
		log.info("删除商品------start------{}", param);
		Map<String, Object> respMap = auctionGoodsInfoBiz.deleteGoodsInfo(param);
		if (respMap.get("code").equals("00")) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("删除商品------end------{}", respMap);
		return respMap;
	}
	
	/**
	 * 修改商品
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public Map<String, Object> update(@RequestBody Map<String, Object> param) {
		log.info("修改商品------start------{}", param);
		Map<String, Object> respMap = auctionGoodsInfoBiz.updateGoodsInfo(param);
		if (respMap.get("code").equals("00")) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("修改商品------end------{}", respMap);
		return respMap;
	}

}
