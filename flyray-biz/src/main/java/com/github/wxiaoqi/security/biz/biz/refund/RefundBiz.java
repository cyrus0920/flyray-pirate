package com.github.wxiaoqi.security.biz.biz.refund;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.auth.common.util.SnowFlake;
import com.github.wxiaoqi.security.biz.feign.PayFeign;
import com.github.wxiaoqi.security.common.msg.ResponseCode;

/**
 * 退款
 * @author he
 *
 */
@Service
public class RefundBiz {

	private static final Logger logger = (Logger) LoggerFactory.getLogger(RefundBiz.class);

	@Autowired
	private PayFeign payFeign;

	/**
	 * 公共退款发起
	 * @return
	 */
	public Map<String, Object> commonRefund(Map<String, Object> params){

		logger.info("公共退款发起请求.......{}",params);
		Map<String, Object> respMap = new HashMap<String, Object>();

		//退款申请
		String refundOrderNo = String.valueOf(SnowFlake.getId());
		params.put("refundOrderNo", refundOrderNo);
		Map<String, Object> refundApplyMap = refundApply(params);
		if(!ResponseCode.OK.getCode().equals(refundApplyMap.get("code"))){
			respMap.put("success", false);
			respMap.put("code", ResponseCode.REFUND_FAIL.getCode());
			respMap.put("msg", ResponseCode.REFUND_FAIL.getMessage());
			return respMap;
		}

		//发起退款审核
		Map<String, Object> refundMap = refund(params);
		if(!ResponseCode.OK.getCode().equals(refundMap.get("code"))){
			respMap.put("success", false);
			respMap.put("code", ResponseCode.PAY_FAIL.getCode());
			respMap.put("msg", ResponseCode.PAY_FAIL.getMessage());
			return respMap;
		}

		//处理场景订单
		this.scenesOrderDealWith(params);

		respMap.put("success", true);
		respMap.put("code", ResponseCode.OK.getCode());
		respMap.put("msg", ResponseCode.OK.getMessage());
		return respMap;
	}

	/**
	 * 退款申请
	 * @param params
	 * @return
	 */
	private Map<String, Object> refundApply(Map<String, Object> params){
		Map<String, Object> refundApplyMap = new HashMap<String, Object>();
		refundApplyMap.put("platformId", (String)params.get("platformId"));
		refundApplyMap.put("payOrderNo", (String)params.get("payOrderNo"));
		refundApplyMap.put("refundOrderNo", (String)params.get("refundOrderNo"));
		refundApplyMap.put("refundAmt", (String)params.get("refundAmt"));
		logger.info("退款申请请求.......{}",refundApplyMap);
		Map<String, Object> applyRespMap = payFeign.refundApply(refundApplyMap);
		logger.info("退款申请响应.......{}",applyRespMap);
		return applyRespMap;
	}

	/**
	 * 退款审核
	 * @param params
	 * @return
	 */
	private Map<String, Object> refund(Map<String, Object> params){
		Map<String, Object> refundMap = new HashMap<String, Object>();
		refundMap.put("platformId", (String)params.get("platformId"));
		refundMap.put("refundOrderNo", (String)params.get("refundOrderNo"));
		logger.info("退款审核请求.......{}",refundMap);
		Map<String, Object> refundRespMap = payFeign.refund(refundMap);
		logger.info("退款审核响应.......{}",refundRespMap);
		return refundRespMap;
	}

	/**
	 * 场景订单处理
	 * @param params
	 */
	private void scenesOrderDealWith(Map<String, Object> params){
		String scenesCode = (String)params.get("scenesCode");
		//处理各场景相关订单表
		if("1".equals(scenesCode)){
			//竞拍
			//发起退款不处理，退款成功回调再做更新
		}else if("2".equals(scenesCode)){
			//拼团

		}else if("3".equals(scenesCode)){
			//店内点餐

		}else if("4".equals(scenesCode)){
			//预订点餐

		}else if("5".equals(scenesCode)){
			//外卖

		}
	}

}
