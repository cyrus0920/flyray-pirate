package com.github.wxiaoqi.security.biz.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 场景订单
 * @author he
 * @date 2018-08-09 16:34:06
 */
@Table(name = "cms_scenes_order")
public class CmsScenesOrder implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer id;
	
	//平台编号
	@Column(name = "platform_id")
	private String platformId;
	
	//商户编号
	@Column(name = "mer_id")
	private String merId;
	
	//用户编号
	@Column(name = "per_id")
	private String perId;

	//商户订单号
	@Column(name = "pay_order_no")
	private String payOrderNo;
	
	//退款订单号
	@Column(name = "refund_order_no")
	private String refundOrderNo;

	//场景编号
	@Column(name = "scenes_code")
	private String scenesCode;

	//支付金额
	@Column(name = "tx_amt")
	private BigDecimal txAmt;


	/**
	 * 设置：
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Integer getId() {
		return id;
	}
	public String getPlatformId() {
		return platformId;
	}
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	public String getMerId() {
		return merId;
	}
	public void setMerId(String merId) {
		this.merId = merId;
	}
	public String getPerId() {
		return perId;
	}
	public void setPerId(String perId) {
		this.perId = perId;
	}
	public String getPayOrderNo() {
		return payOrderNo;
	}
	public void setPayOrderNo(String payOrderNo) {
		this.payOrderNo = payOrderNo;
	}
	public String getRefundOrderNo() {
		return refundOrderNo;
	}
	public void setRefundOrderNo(String refundOrderNo) {
		this.refundOrderNo = refundOrderNo;
	}
	/**
	 * 设置：场景编号
	 */
	public void setScenesCode(String scenesCode) {
		this.scenesCode = scenesCode;
	}
	/**
	 * 获取：场景编号
	 */
	public String getScenesCode() {
		return scenesCode;
	}
	/**
	 * 设置：支付金额
	 */
	public void setTxAmt(BigDecimal txAmt) {
		this.txAmt = txAmt;
	}
	/**
	 * 获取：支付金额
	 */
	public BigDecimal getTxAmt() {
		return txAmt;
	}
}
