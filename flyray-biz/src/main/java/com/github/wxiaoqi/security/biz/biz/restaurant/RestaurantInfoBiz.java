package com.github.wxiaoqi.security.biz.biz.restaurant;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.biz.entity.restaurant.RestaurantInfo;
import com.github.wxiaoqi.security.biz.entity.restaurant.RestaurantPictureInfo;
import com.github.wxiaoqi.security.biz.mapper.RestaurantInfoMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.util.EntityUtils;
import com.github.wxiaoqi.security.common.util.Query;

/**
 * 餐厅信息表
 *
 * @author he
 * @date 2018-06-29 10:32:56
 */
@Service
public class RestaurantInfoBiz extends BaseBiz<RestaurantInfoMapper,RestaurantInfo> {
	
	private static final Logger logger = (Logger) LoggerFactory.getLogger(RestaurantInfoBiz.class);
	
	@Autowired
	private RestaurantInfoMapper restaurantInfoMapper;
	@Autowired
	private RestaurantPictureInfoBiz restaurantPictureInfoBiz;
	
	/**
	 * 餐厅信息查询
	 * @param request
	 * @return
	 */
	public Map<String, Object> queryRestaurantInfo(Map<String, Object> request){
		logger.info("餐厅信息查询请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platFormId");
		String merId = (String) request.get("merId");
		RestaurantInfo restaurantInfo = new RestaurantInfo();
		restaurantInfo.setPlatformId(platFormId);
		restaurantInfo.setMerId(merId);
		List<RestaurantInfo> restaurantInfoList = restaurantInfoMapper.select(restaurantInfo);
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		for(RestaurantInfo restaurantInfos:restaurantInfoList){
			RestaurantPictureInfo restaurantPictureInfo = new RestaurantPictureInfo();
			restaurantPictureInfo.setMerId(restaurantInfos.getMerId());
			restaurantPictureInfo.setPlatformId(restaurantInfos.getPlatformId());
			List<RestaurantPictureInfo> selectList = restaurantPictureInfoBiz.selectList(restaurantPictureInfo);
			Map<String, Object> map = EntityUtils.beanToMap(restaurantInfos);
			map.put("pictureList", selectList);
			list.add(map);
		}
		response.put("info", list);
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		logger.info("餐厅信息查询响应。。。。。。{}", response);
		return response;
	}
	
	/**
	 * 查询餐厅信息列表
	 * @param param
	 * @return
	 */
	public Map<String, Object> queryRestaurantInfoPage(Map<String, Object> param) {
		logger.info("查询餐厅信息列表。。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		try {
			Query query = new Query(param);
			Page<RestaurantInfo> result = PageHelper.startPage(query.getPage(), query.getLimit());
			List<RestaurantInfo> list = mapper.queryRestaurantInfo(param);
			TableResultResponse<RestaurantInfo> table = new TableResultResponse<>(result.getTotal(), list);
			respMap.put("code", "00");
			respMap.put("msg", "查询成功");
			respMap.put("body", table);
		} catch (Exception e) {
			respMap.put("code", "01");
			respMap.put("msg", "查询异常");
		}
		return respMap;
	}
	
	/**
	 * 删除餐厅
	 * @param param
	 * @return
	 */
	public Map<String, Object> deleteRestaurantInfo(Map<String, Object> param) {
		logger.info("删除餐厅。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		try {
			int id = (int) param.get("id");
			RestaurantInfo restaurantInfo = mapper.selectByPrimaryKey(Long.valueOf(id));
			if (restaurantInfo != null) {
				mapper.delete(restaurantInfo);
				respMap.put("code", "00");
				respMap.put("msg", "删除成功");
			}else {
				respMap.put("code", "01");
				respMap.put("msg", "商品不存在");
			}
		} catch (Exception e) {
			respMap.put("code", "01");
			respMap.put("msg", "操作异常");
		}
		return respMap;
	}
	
	/**
	 * 修改餐厅
	 * @param param
	 * @return
	 */
	public Map<String, Object> updateRestaurantInfo(Map<String, Object> param) {
		logger.info("修改餐厅。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		try {
			int id = (int) param.get("id");
			String name = (String) param.get("name");
			String logoImg = (String) param.get("logoImg");
			String area = (String) param.get("area");
			String latitude = (String) param.get("latitude");
			String longitude = (String) param.get("longitude");
			String conService = (String) param.get("conService");
			String perCapitaSpending = (String) param.get("perCapitaSpending");
			String bulletin = (String) param.get("bulletin");
			String conMobile = (String) param.get("conMobile");
			String openBtime = (String) param.get("openBtime");
			String openEtime = (String) param.get("openEtime");
			String introduce = (String) param.get("introduce");
			String qualification = (String) param.get("qualification");
			String isOrder = (String) param.get("isOrder");
			String isReservation = (String) param.get("isReservation");
			String isTakeaway = (String) param.get("isTakeaway");
			RestaurantInfo restaurantInfo = mapper.selectByPrimaryKey(Long.valueOf(id));
			if (restaurantInfo != null) {
				restaurantInfo.setArea(area);
				restaurantInfo.setBulletin(bulletin);
				restaurantInfo.setConMobile(conMobile);
				restaurantInfo.setConService(conService);
				restaurantInfo.setIntroduce(introduce);
				restaurantInfo.setIsOrder(isOrder);
				restaurantInfo.setIsReservation(isReservation);
				restaurantInfo.setIsTakeaway(isTakeaway);
				restaurantInfo.setLatitude(latitude);
				restaurantInfo.setLogoImg(logoImg);
				restaurantInfo.setLongitude(longitude);
				restaurantInfo.setName(name);
				SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
				restaurantInfo.setOpenBtime(new Time(sdf.parse(openBtime).getTime()));
				restaurantInfo.setOpenEtime(new Time(sdf.parse(openEtime).getTime()));
				restaurantInfo.setPerCapitaSpending(perCapitaSpending);
				restaurantInfo.setQualification(qualification);
				mapper.updateByPrimaryKey(restaurantInfo);
				respMap.put("code", "00");
				respMap.put("msg", "修改成功");
			}else {
				respMap.put("code", "01");
				respMap.put("msg", "餐厅不存在");
			}
		} catch (Exception e) {
			respMap.put("code", "01");
			respMap.put("msg", "操作异常");
		}
		return respMap;
	}
	
}