package com.github.wxiaoqi.security.biz.mapper;

import com.github.wxiaoqi.security.biz.entity.CmsScenesOrder;

import tk.mybatis.mapper.common.Mapper;

/**
 * 支付订单
 * @author he
 * @date 2018-08-09 16:34:06
 */
@org.apache.ibatis.annotations.Mapper
public interface CmsScenesOrderMapper extends Mapper<CmsScenesOrder> {
	
}
