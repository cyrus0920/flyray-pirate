package com.github.wxiaoqi.security.biz.rest.cms;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.auth.common.util.SnowFlake;
import com.github.wxiaoqi.security.biz.biz.restaurant.RestaurantTableInfoBiz;
import com.github.wxiaoqi.security.biz.entity.restaurant.RestaurantInfo;
import com.github.wxiaoqi.security.biz.entity.restaurant.RestaurantTableInfo;
import com.github.wxiaoqi.security.common.rest.BaseController;

import lombok.extern.slf4j.Slf4j;

/**
 * 小程序点餐餐桌信息相关接口
 * @author he
 *
 */
@Slf4j
@Controller
@RequestMapping("restaurant/tableInfo")
public class RestaurantTableInfoRestController extends BaseController<RestaurantTableInfoBiz, RestaurantTableInfo> {
	
	@Autowired
	private RestaurantTableInfoBiz restaurantTableInfoBiz;
	
	/**
	 * 查询点餐餐桌信息列表
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Map<String, Object> query(@RequestBody Map<String, Object> param) {
		log.info("查询点餐餐桌信息列表------start------{}", param);
		Map<String, Object> respMap = restaurantTableInfoBiz.queryRestaurantTableInfoPage(param);
		if (respMap.get("code").equals("00")) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("查询点餐餐桌信息列表-----end-------{}", respMap);
		return respMap;
	}
	
	/**
	 * 添加餐桌
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public Map<String, Object> add(@RequestBody Map<String, Object> param) {
		log.info("添加餐桌------start------{}", param);
		Map<String, Object> respMap = new HashMap<>();
		try {
			String id = String.valueOf(SnowFlake.getId());
			String platformId = (String) param.get("platformId");
			String tableName = (String) param.get("tableName");
			String tablePeople = (String) param.get("tablePeople");
			String status = (String) param.get("status");
			RestaurantTableInfo restaurantTableInfo = new RestaurantTableInfo();
			restaurantTableInfo.setTableId(id);
			restaurantTableInfo.setPlatformId(platformId);
			restaurantTableInfo.setTableName(tableName);
			restaurantTableInfo.setTablePeople(tablePeople);
			restaurantTableInfo.setStatus(status);
			restaurantTableInfoBiz.insert(restaurantTableInfo);
			respMap.put("code", "00");
			respMap.put("msg", "添加成功");
		} catch (Exception e) {
			respMap.put("code", "01");
			respMap.put("msg", "添加异常");
		}
		log.info("添加餐桌-----end-------------{}", respMap);
		return respMap;
	}
	
	/**
	 * 删除餐桌
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public Map<String, Object> delete(@RequestBody Map<String, Object> param) {
		log.info("删除餐桌------start------{}", param);
		Map<String, Object> respMap = restaurantTableInfoBiz.deleteRestaurantTableInfo(param);
		if (respMap.get("code").equals("00")) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("删除餐桌------end------{}", respMap);
		return respMap;
	}

	/**
	 * 修改餐桌
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public Map<String, Object> update(@RequestBody Map<String, Object> param) {
		log.info("修改餐桌------start------{}", param);
		Map<String, Object> respMap = restaurantTableInfoBiz.updateRestaurantTableInfo(param);
		if (respMap.get("code").equals("00")) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("修改餐桌------end------{}", respMap);
		return respMap;
	}

}
