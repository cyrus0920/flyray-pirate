package com.github.wxiaoqi.security.biz.biz.fightGroup;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.biz.entity.fightGroup.FightGroupInfo;
import com.github.wxiaoqi.security.biz.entity.fightGroup.FightGroupMemberInfo;
import com.github.wxiaoqi.security.biz.mapper.FightGroupInfoMapper;
import com.github.wxiaoqi.security.biz.mapper.FightGroupMemberInfoMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.util.Query;
import com.github.wxiaoqi.security.common.util.SnowFlake;

/**
 * 团信息表
 *
 * @author he
 * @date 2018-07-12 10:17:33
 */
@Service
public class FightGroupInfoBiz extends BaseBiz<FightGroupInfoMapper,FightGroupInfo> {

	private static final Logger logger = (Logger) LoggerFactory.getLogger(FightGroupInfoBiz.class);

	@Autowired
	private FightGroupInfoMapper fightGroupInfoMapper;
	@Autowired
	private FightGroupMemberInfoMapper fightGroupMemberInfoMapper;

	/**
	 * 参团
	 * @param request
	 * @return
	 */
	public Map<String, Object> joinGroup(Map<String, Object> request){
		logger.info("参团请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platFormId");
		String merId = (String) request.get("merId");
		String groupId = (String) request.get("groupId");
		String perId = (String) request.get("perId");
		String goodsId = (String) request.get("goodsId");
		String name = (String) request.get("name");
		String userHeadPortrait = (String) request.get("userHeadPortrait");
		FightGroupInfo fightGroupInfo = new FightGroupInfo();
		fightGroupInfo.setPlatformId(platFormId);
		fightGroupInfo.setMerId(merId);
		fightGroupInfo.setGroupId(Integer.valueOf(groupId));
		fightGroupInfo.setGoodsId(Integer.valueOf(goodsId));
		FightGroupInfo groupInfo = fightGroupInfoMapper.selectOne(fightGroupInfo);
		Integer joinedGroupNum = groupInfo.getJoinedGroupNum();
		groupInfo.setJoinedGroupNum(joinedGroupNum + 1);
		fightGroupInfoMapper.updateByPrimaryKey(groupInfo);

		FightGroupMemberInfo fightGroupMemberInfo = new FightGroupMemberInfo();
		fightGroupMemberInfo.setPlatformId(platFormId);
		fightGroupMemberInfo.setMerId(merId);
		fightGroupMemberInfo.setPerId(perId);
		fightGroupMemberInfo.setGroupId(Integer.valueOf(groupId));
		fightGroupMemberInfo.setName(name);
		fightGroupMemberInfo.setUserHeadPortrait(userHeadPortrait);
		fightGroupMemberInfo.setCreateTime(new Date());
		fightGroupMemberInfoMapper.insert(fightGroupMemberInfo);
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		logger.info("参团响应。。。。。。{}", response);
		return response;
	}

	/**
	 * 创建团
	 * @param request
	 * @return
	 */
	public Map<String, Object> createGroup(Map<String, Object> request){
		logger.info("创建团请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platFormId");
		String merId = (String) request.get("merId");
		String groupsId = (String) request.get("groupsId");
		String perId = (String) request.get("perId");
		String goodsId = (String) request.get("goodsId");
		String name = (String) request.get("name");
		String userHeadPortrait = (String) request.get("userHeadPortrait");
		FightGroupInfo fightGroupInfo = new FightGroupInfo();
		fightGroupInfo.setPlatformId(platFormId);
		fightGroupInfo.setMerId(merId);
		fightGroupInfo.setHeadPerId(perId);
		fightGroupInfo.setJoinedGroupNum(1);
		fightGroupInfo.setGroupsId(Integer.valueOf(groupsId));
		fightGroupInfo.setCreateTime(new Date());
		fightGroupInfo.setGoodsId(Integer.valueOf(goodsId));
		long groupId = SnowFlake.getId();
		fightGroupInfo.setGroupId((int)groupId);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date parse;
		try {
			parse = sdf.parse(getFetureDate(7));
			Timestamp date = new Timestamp(parse.getTime());
			fightGroupInfo.setEndDate(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		fightGroupInfoMapper.insert(fightGroupInfo);
		
		FightGroupMemberInfo fightGroupMemberInfo = new FightGroupMemberInfo();
		fightGroupMemberInfo.setPlatformId(platFormId);
		fightGroupMemberInfo.setMerId(merId);
		fightGroupMemberInfo.setPerId(perId);
		fightGroupMemberInfo.setGroupId((int)groupId);
		fightGroupMemberInfo.setName(name);
		fightGroupMemberInfo.setUserHeadPortrait(userHeadPortrait);
		fightGroupMemberInfo.setCreateTime(new Date());
		fightGroupMemberInfoMapper.insert(fightGroupMemberInfo);

		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		logger.info("创建团响应。。。。。。{}", response);
		return response;
	}

	/**
	 * 获取的n天后的日期
	 * @param past
	 * @return
	 */
	public static String getFetureDate(int past) {  
		Calendar calendar = Calendar.getInstance();  
		calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) + past);  
		Date today = calendar.getTime();  
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");  
		String result = format.format(today);  
		return result;  
	} 
	
	/**
	 * 查询拼团团信息列表
	 * @param param
	 * @return
	 */
	public Map<String, Object> queryFightGroupInfoPage(Map<String, Object> param) {
		logger.info("查询拼团团信息列表。。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		try {
			Query query = new Query(param);
			Page<FightGroupInfo> result = PageHelper.startPage(query.getPage(), query.getLimit());
			List<FightGroupInfo> list = mapper.queryFightGroupInfo(param);
			TableResultResponse<FightGroupInfo> table = new TableResultResponse<>(result.getTotal(), list);
			respMap.put("code", "00");
			respMap.put("msg", "查询成功");
			respMap.put("body", table);
		} catch (Exception e) {
			respMap.put("code", "01");
			respMap.put("msg", "查询异常");
		}
		return respMap;
	}
}