package com.github.wxiaoqi.security.biz.rest.cms;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.biz.biz.activity.ActivityInfoBiz;
import com.github.wxiaoqi.security.biz.entity.activity.ActivityInfo;
import com.github.wxiaoqi.security.common.cms.request.CmsQueryActivityParam;
import com.github.wxiaoqi.security.common.cms.request.CmsSaveActivityParam;
import com.github.wxiaoqi.security.common.rest.BaseController;

import lombok.extern.slf4j.Slf4j;

/**
 * 小程序活动相关接口
 * @author Administrator
 *
 */
@Slf4j
@Controller
@RequestMapping("activities")
public class ActivityRestController extends BaseController<ActivityInfoBiz, ActivityInfo> {

	@Autowired
	private ActivityInfoBiz activityBiz;
	
	/**
	 * 查询所有活动列表
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Map<String, Object> query(@RequestBody CmsQueryActivityParam param) {
		log.info("查询所有活动------start------{}", param);
		Map<String, Object> respMap = activityBiz.queryActivitiesWithPage(param);
		if (respMap.get("code").equals("00")) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("查询所有活动------end------{}", respMap);
		return respMap;
	}
	
	/**
	 * 添加活动
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public Map<String, Object> add(@RequestBody CmsSaveActivityParam param) {
		log.info("添加活动------start------{}", param);
		Map<String, Object> respMap = activityBiz.addActivity(param);
		if (respMap.get("code").equals("00")) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("添加活动---- end --{}", respMap);
		return respMap;
	}
	
	
	/**
	 * 修改
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public Map<String, Object> update(@RequestBody CmsSaveActivityParam param) {
		log.info("修改活动------start------{}", param);
		Map<String, Object> respMap = activityBiz.updateActivity(param);
		if (respMap.get("code").equals("00")) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("修改活动---- end --{}", respMap);
		return respMap;
	}
	
	
	/**
	 * 关闭活动
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public Map<String, Object> delete(@RequestBody CmsSaveActivityParam param) {
		log.info("关闭活动------start------{}", param);
		Map<String, Object> respMap = activityBiz.deleteActivity(param);
		if (respMap.get("code").equals("00")) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("关闭活动------end------{}", respMap);
		return respMap;
	}
}
