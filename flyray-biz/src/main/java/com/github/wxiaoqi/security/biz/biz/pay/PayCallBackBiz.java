package com.github.wxiaoqi.security.biz.biz.pay;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.github.wxiaoqi.security.biz.biz.refund.RefundBiz;
import com.github.wxiaoqi.security.biz.entity.CmsScenesOrder;
import com.github.wxiaoqi.security.biz.entity.auction.AuctionOrder;
import com.github.wxiaoqi.security.biz.feign.CrmFeign;
import com.github.wxiaoqi.security.biz.mapper.AuctionOrderMapper;
import com.github.wxiaoqi.security.biz.mapper.CmsScenesOrderMapper;
import com.github.wxiaoqi.security.common.msg.ResponseCode;

/**
 * 支付回调处理
 * @author he
 *
 */
@Service
public class PayCallBackBiz {

	private static final Logger logger = (Logger) LoggerFactory.getLogger(PayCallBackBiz.class);

	@Autowired
	private CrmFeign crmFeign;
	@Autowired
	private CmsScenesOrderMapper cmsScenesOrderMapper;
	@Autowired
	private AuctionOrderMapper auctionOrderMapper;
	@Autowired
	private RefundBiz refundBiz;

	/**
	 * 支付完成回调
	 * @param request
	 * @return
	 */
	public void payCallBack(Map<String, Object> params){
		logger.info("支付完成回调开始.......{}",params);
		String platformId = (String) params.get("platformId");
		String orderNo = (String) params.get("orderNo");
		String txStatus = (String) params.get("txStatus");
		String amount = (String) params.get("amount");
		BigDecimal amountDecimal = new BigDecimal(amount);
		CmsScenesOrder cmsScenesOrder = new CmsScenesOrder();
		cmsScenesOrder.setPlatformId(platformId);
		cmsScenesOrder.setPayOrderNo(orderNo);
		CmsScenesOrder selectOrder = cmsScenesOrderMapper.selectOne(cmsScenesOrder);
		if(null == selectOrder){
			logger.info("支付完成回调-订单不存在.......");
			return;
		}
		if(0 != selectOrder.getTxAmt().compareTo(amountDecimal)){
			logger.info("支付完成回调-金额不一致.......");
			return;
		}

		//支付成功入账
		if("00".equals(txStatus)){
			crmFeign.inAccounting(params);
		}

		//更新场景订单
		String scenesCode = selectOrder.getScenesCode();
		if("1".equals(scenesCode)){
			//创建支付成功竞拍订单
			this.updateAuctionOrder(params);
		}else if("2".equals(scenesCode)){
			//拼团

		}else if("3".equals(scenesCode)){
			//店内点餐

		}else if("4".equals(scenesCode)){
			//预订点餐

		}else if("5".equals(scenesCode)){
			//外卖

		}
	}

	/**
	 * 竞拍订单处理
	 * @param params
	 */
	private void updateAuctionOrder(Map<String, Object> params){
		if("00".equals((String) params.get("txStatus"))){
			
			//根据订单号查询竞拍订单
			AuctionOrder queryAuctionOrder = new AuctionOrder();
			queryAuctionOrder.setPlatformId((String) params.get("platformId"));
			queryAuctionOrder.setMerId((String) params.get("merId"));
			queryAuctionOrder.setPayOrderNo((String) params.get("orderNo"));
			AuctionOrder queryOrder = auctionOrderMapper.queryLastOne(queryAuctionOrder);
			
			//查询该商品最后一条付款成功的记录
			AuctionOrder auctionOrder = new AuctionOrder();
			auctionOrder.setPlatformId((String) params.get("platformId"));
			auctionOrder.setMerId((String) params.get("merId"));
			auctionOrder.setGoodsId(queryOrder.getGoodsId());
			auctionOrder.setStatus("00");//支付成功
			AuctionOrder lastAuctionOrder = auctionOrderMapper.queryLastOne(auctionOrder);
			if(null == lastAuctionOrder){
				//记录为空，则更新为序列为1的记录
				queryOrder.setNumber(1);
			}else{
				//如果不为空，则更新为序列加1的记录，并将原记录发起退款
				queryOrder.setNumber(lastAuctionOrder.getNumber() + 1);
				//发起退款
				Map<String, Object> refundMap = new HashMap<String, Object>();
				refundMap.put("platformId", (String) params.get("platformId"));
				refundMap.put("payOrderNo", (String) params.get("orderNo"));
				refundMap.put("refundAmt", (String) params.get("orderAmt"));
				Map<String, Object> refundRespMap = refundBiz.commonRefund(refundMap);
				if(!ResponseCode.OK.getCode().equals(refundRespMap.get("code"))){
					queryOrder.setStatus("02");//退款失败
				}else{
					queryOrder.setStatus("00");//支付成功
				}
			}
			queryOrder.setCreateTime(new Date());
			auctionOrderMapper.updateByPrimaryKey(queryOrder);
		}
	}
}
