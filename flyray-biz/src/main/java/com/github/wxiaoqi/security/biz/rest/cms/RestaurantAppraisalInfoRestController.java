package com.github.wxiaoqi.security.biz.rest.cms;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.biz.biz.restaurant.RestaurantAppraisalInfoBiz;
import com.github.wxiaoqi.security.biz.entity.restaurant.RestaurantAppraisalInfo;
import com.github.wxiaoqi.security.common.rest.BaseController;

import lombok.extern.slf4j.Slf4j;

/**
 * 小程序点餐评论信息相关接口
 * @author he
 *
 */
@Slf4j
@Controller
@RequestMapping("restaurant/appraisal")
public class RestaurantAppraisalInfoRestController extends BaseController<RestaurantAppraisalInfoBiz, RestaurantAppraisalInfo> {
	
	@Autowired
	private RestaurantAppraisalInfoBiz restaurantAppraisalInfoBiz;
	
	/**
	 * 查询点餐评论信息列表
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Map<String, Object> query(@RequestBody Map<String, Object> param) {
		log.info("查询点餐评论信息列表------start------{}", param);
		Map<String, Object> respMap = restaurantAppraisalInfoBiz.queryRestaurantAppraisalInfoPage(param);
		if (respMap.get("code").equals("00")) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("查询点餐评论信息列表-----end-------{}", respMap);
		return respMap;
	}

}
