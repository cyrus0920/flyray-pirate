package com.github.wxiaoqi.security.biz.biz.fightGroup;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.biz.entity.fightGroup.FightGroupCategoryInfo;
import com.github.wxiaoqi.security.biz.entity.fightGroup.FightGroupGoodsInfo;
import com.github.wxiaoqi.security.biz.entity.fightGroup.FightGroupGoodsPicture;
import com.github.wxiaoqi.security.biz.entity.fightGroup.FightGroupInfo;
import com.github.wxiaoqi.security.biz.entity.fightGroup.FightGroupMemberInfo;
import com.github.wxiaoqi.security.biz.mapper.FightGroupCategoryInfoMapper;
import com.github.wxiaoqi.security.biz.mapper.FightGroupGoodsInfoMapper;
import com.github.wxiaoqi.security.biz.mapper.FightGroupGoodsPictureMapper;
import com.github.wxiaoqi.security.biz.mapper.FightGroupInfoMapper;
import com.github.wxiaoqi.security.biz.mapper.FightGroupMemberInfoMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.util.EntityUtils;
import com.github.wxiaoqi.security.common.util.Query;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * 拼团商品信息
 *
 * @author he
 * @date 2018-07-10 09:35:10
 */
@Service
public class FightGroupGoodsInfoBiz extends BaseBiz<FightGroupGoodsInfoMapper,FightGroupGoodsInfo> {

	private static final Logger logger = (Logger) LoggerFactory.getLogger(FightGroupGoodsInfoBiz.class);

	@Autowired
	private FightGroupGoodsInfoMapper fightGroupGoodsInfoMapper;
	@Autowired
	private FightGroupGoodsPictureMapper fightGroupGoodsPictureMapper;
	@Autowired
	private FightGroupCategoryInfoMapper fightGroupCategoryInfoMapper;
	@Autowired
	private FightGroupInfoMapper fightGroupInfoMapper;
	@Autowired
	private FightGroupMemberInfoMapper fightGroupMemberInfoMapper;

	/**
	 * 查询拼团商品信息列表
	 * @param request
	 * @return
	 */
	public Map<String, Object> queryFightGroupGoodsInfo(Map<String, Object> request){
		logger.info("查询拼团商品信息列表请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platFormId");
		String merId = (String) request.get("merId");
		FightGroupGoodsInfo fightGroupGoodsInfo = new FightGroupGoodsInfo();
		fightGroupGoodsInfo.setPlatformId(platFormId);
		fightGroupGoodsInfo.setMerId(merId);
		List<FightGroupGoodsInfo> goodsInfoList = fightGroupGoodsInfoMapper.select(fightGroupGoodsInfo);
		response.put("goodsInfoList", goodsInfoList);
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		logger.info("查询拼团商品信息列表响应。。。。。。{}", response);
		return response;
	}

	/**
	 * 查询拼团商品信息明细
	 * @param request
	 * @return
	 */
	public Map<String, Object> queryFightGroupGoodsDetailInfo(Map<String, Object> request){
		logger.info("查询拼团商品信息明细请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platFormId");
		String merId = (String) request.get("merId");
		String goodsId = (String) request.get("goodsId");
		String perId = (String) request.get("perId");
		FightGroupGoodsInfo fightGroupGoodsInfo = new FightGroupGoodsInfo();
		fightGroupGoodsInfo.setPlatformId(platFormId);
		fightGroupGoodsInfo.setMerId(merId);
		fightGroupGoodsInfo.setGoodsId(goodsId);
		FightGroupGoodsInfo goodsInfoInfo = fightGroupGoodsInfoMapper.selectOne(fightGroupGoodsInfo);
		FightGroupGoodsPicture fightGroupGoodsPicture = new FightGroupGoodsPicture();
		fightGroupGoodsPicture.setPlatformId(platFormId);
		fightGroupGoodsPicture.setMerId(merId);
		fightGroupGoodsPicture.setGoodsId(Integer.valueOf(goodsId));
		List<FightGroupGoodsPicture> pictureInfo = fightGroupGoodsPictureMapper.select(fightGroupGoodsPicture);

		//查询团类目
		FightGroupCategoryInfo fightGroupCategoryInfo = new FightGroupCategoryInfo();
		fightGroupCategoryInfo.setPlatformId(platFormId);
		fightGroupCategoryInfo.setMerId(merId);
		fightGroupCategoryInfo.setGoodsId(Integer.valueOf(goodsId));
		List<FightGroupCategoryInfo> groupCategoryInfo = fightGroupCategoryInfoMapper.select(fightGroupCategoryInfo);

		//查询已有团
		Example example = new Example(FightGroupInfo.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("platformId", platFormId);
		criteria.andEqualTo("merId", merId);
		criteria.andEqualTo("goodsId", goodsId);
		example.setOrderByClause("create_time desc");
		List<FightGroupInfo> selectByExample = fightGroupInfoMapper.selectByExample(example);
		List<Map<String, Object>> groupList = new ArrayList<Map<String, Object>>();
		for(FightGroupInfo fightGroupInfo: selectByExample){
			Integer groupId = fightGroupInfo.getGroupId();
			String headPerId = fightGroupInfo.getHeadPerId();
			Integer groupsId = fightGroupInfo.getGroupsId();
			fightGroupCategoryInfo.setGroupsId(groupsId);
			FightGroupCategoryInfo selectCategoryInfo = fightGroupCategoryInfoMapper.selectOne(fightGroupCategoryInfo);

			//查询队长信息
			FightGroupMemberInfo fightGroupMemberInfo = new FightGroupMemberInfo();
			fightGroupMemberInfo.setPlatformId(platFormId);
			fightGroupMemberInfo.setMerId(merId);
			fightGroupMemberInfo.setGroupId(groupId);
			fightGroupMemberInfo.setPerId(headPerId);
			FightGroupMemberInfo selectMemberInfo = fightGroupMemberInfoMapper.selectOne(fightGroupMemberInfo);

			//查询用户是否参团
			fightGroupMemberInfo.setPerId(perId);
			FightGroupMemberInfo selectByPerId = fightGroupMemberInfoMapper.selectOne(fightGroupMemberInfo);
			Timestamp endDate = fightGroupInfo.getEndDate();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Map<String, Object> fightGroupMap = EntityUtils.beanToMap(fightGroupInfo);
			Map<String, Object> memberMap = EntityUtils.beanToMap(selectMemberInfo);
			fightGroupMap.putAll(memberMap);
			if(null != selectByPerId){
				//标识已参团
				fightGroupMap.put("isJoin", "00");//00表示已参团
			}
			fightGroupMap.put("endDate", sdf.format(endDate));
			fightGroupMap.put("groupName", selectCategoryInfo.getGroupName());
			fightGroupMap.put("peopleNum", selectCategoryInfo.getPeopleNum());
			groupList.add(fightGroupMap);
		}

		response.put("groupList", groupList);
		response.put("groupCategoryInfo", groupCategoryInfo);
		response.put("goodsInfo", goodsInfoInfo);
		response.put("pictureInfo", pictureInfo);
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		logger.info("查询拼团商品信息明细响应。。。。。。{}", response);
		return response;
	}
	
	/**
	 * 查询拼团商品信息列表
	 * @param param
	 * @return
	 */
	public Map<String, Object> queryFightGroupGoodsInfoPage(Map<String, Object> param) {
		logger.info("查询拼团商品信息列表。。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		try {
			Query query = new Query(param);
			Page<FightGroupGoodsInfo> result = PageHelper.startPage(query.getPage(), query.getLimit());
			List<FightGroupGoodsInfo> list = mapper.queryFightGroupGoodsInfo(param);
			TableResultResponse<FightGroupGoodsInfo> table = new TableResultResponse<>(result.getTotal(), list);
			respMap.put("code", "00");
			respMap.put("msg", "查询成功");
			respMap.put("body", table);
		} catch (Exception e) {
			respMap.put("code", "01");
			respMap.put("msg", "查询异常");
		}
		return respMap;
	}
	
	/**
	 * 删除商品
	 * @param param
	 * @return
	 */
	public Map<String, Object> deleteGoodsInfo(Map<String, Object> param) {
		logger.info("删除商品。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		try {
			int id = (int) param.get("id");
			FightGroupGoodsInfo fightGroupGoodsInfo = mapper.selectByPrimaryKey(Long.valueOf(id));
			if (fightGroupGoodsInfo != null) {
				mapper.delete(fightGroupGoodsInfo);
				respMap.put("code", "00");
				respMap.put("msg", "删除成功");
			}else {
				respMap.put("code", "01");
				respMap.put("msg", "商品不存在");
			}
		} catch (Exception e) {
			respMap.put("code", "01");
			respMap.put("msg", "操作异常");
		}
		return respMap;
	}
	
	/**
	 * 修改商品
	 * @param param
	 * @return
	 */
	public Map<String, Object> updateGoodsInfo(Map<String, Object> param) {
		logger.info("修改商品。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		try {
			int id = (int) param.get("id");
			String goodsName = (String) param.get("goodsName");
			String goodsDescription = (String) param.get("goodsDescription");
			String goodsImg = (String) param.get("goodsImg");
			String originalPrice = (String) param.get("originalPrice");
			String discountPrice = (String) param.get("discountPrice");
			String effectiveTimeDescription = (String) param.get("effectiveTimeDescription");
			String appointmentReminder = (String) param.get("appointmentReminder");
			String tips = (String) param.get("tips");
			String groupDescription = (String) param.get("groupDescription");
			FightGroupGoodsInfo fightGroupGoodsInfo = mapper.selectByPrimaryKey(Long.valueOf(id));
			if (fightGroupGoodsInfo != null) {
				fightGroupGoodsInfo.setAppointmentReminder(appointmentReminder);
				fightGroupGoodsInfo.setDiscountPrice(new BigDecimal(discountPrice));
				fightGroupGoodsInfo.setEffectiveTimeDescription(effectiveTimeDescription);
				fightGroupGoodsInfo.setGoodsDescription(goodsDescription);
				fightGroupGoodsInfo.setGoodsImg(goodsImg);
				fightGroupGoodsInfo.setGoodsName(goodsName);
				fightGroupGoodsInfo.setGroupDescription(groupDescription);
				fightGroupGoodsInfo.setOriginalPrice(new BigDecimal(originalPrice));
				fightGroupGoodsInfo.setTips(tips);
				mapper.updateByPrimaryKey(fightGroupGoodsInfo);
				respMap.put("code", "00");
				respMap.put("msg", "修改成功");
			}else {
				respMap.put("code", "01");
				respMap.put("msg", "商品不存在");
			}
		} catch (Exception e) {
			respMap.put("code", "01");
			respMap.put("msg", "操作异常");
		}
		return respMap;
	}

}