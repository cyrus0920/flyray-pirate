package com.github.wxiaoqi.security.biz.rest.cms;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.biz.biz.restaurant.RestaurantAppraisalDetailInfoBiz;
import com.github.wxiaoqi.security.biz.entity.restaurant.RestaurantAppraisalDetailInfo;
import com.github.wxiaoqi.security.common.rest.BaseController;

import lombok.extern.slf4j.Slf4j;

/**
 * 小程序点餐评论详情信息相关接口
 * @author he
 *
 */
@Slf4j
@Controller
@RequestMapping("restaurant/appraisalDetail")
public class RestaurantAppraisalDetailInfoRestController extends BaseController<RestaurantAppraisalDetailInfoBiz, RestaurantAppraisalDetailInfo> {
	
	@Autowired
	private RestaurantAppraisalDetailInfoBiz restaurantAppraisalDetailInfoBiz;
	
	/**
	 * 查询点餐评论详情信息列表
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Map<String, Object> query(@RequestBody Map<String, Object> param) {
		log.info("查询点餐评论详情信息列表------start------{}", param);
		Map<String, Object> respMap = restaurantAppraisalDetailInfoBiz.queryRestaurantAppraisalDetailInfoPage(param);
		if (respMap.get("code").equals("00")) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("查询点餐评论详情信息列表-----end-------{}", respMap);
		return respMap;
	}

}
