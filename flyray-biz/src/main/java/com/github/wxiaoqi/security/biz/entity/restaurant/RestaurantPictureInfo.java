package com.github.wxiaoqi.security.biz.entity.restaurant;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 餐厅照片信息表
 * 
 * @author he
 * @date 2018-06-29 10:31:27
 */
@Table(name = "restaurant_picture_info")
public class RestaurantPictureInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	//平台编号
	@Column(name = "platform_id")
	private String platformId;

	//商户账号
	@Column(name = "mer_id")
	private String merId;

	//商家照片
	@Column(name = "picture")
	private String picture;


	/**
	 * 设置：
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：商户账号
	 */
	public void setMerId(String merId) {
		this.merId = merId;
	}
	/**
	 * 获取：商户账号
	 */
	public String getMerId() {
		return merId;
	}
	/**
	 * 设置：商家照片
	 */
	public void setPicture(String picture) {
		this.picture = picture;
	}
	/**
	 * 获取：商家照片
	 */
	public String getPicture() {
		return picture;
	}
}
