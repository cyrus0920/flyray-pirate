package com.github.wxiaoqi.security.biz.biz.restaurant;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.auth.common.util.SnowFlake;
import com.github.wxiaoqi.security.biz.entity.restaurant.RestaurantDishesCategoryInfo;
import com.github.wxiaoqi.security.biz.entity.restaurant.RestaurantDishesInfo;
import com.github.wxiaoqi.security.biz.entity.restaurant.RestaurantInfo;
import com.github.wxiaoqi.security.biz.entity.restaurant.RestaurantShoppingCartInfo;
import com.github.wxiaoqi.security.biz.mapper.RestaurantDishesCategoryInfoMapper;
import com.github.wxiaoqi.security.biz.mapper.RestaurantDishesInfoMapper;
import com.github.wxiaoqi.security.biz.mapper.RestaurantInfoMapper;
import com.github.wxiaoqi.security.biz.mapper.RestaurantShoppingCartInfoMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.util.EntityUtils;
import com.github.wxiaoqi.security.common.util.Query;

import lombok.extern.slf4j.Slf4j;

/**
 * 菜品类目表
 *
 * @author he
 * @date 2018-06-29 10:31:27
 */
@Slf4j
@Service
public class RestaurantDishesCategoryInfoBiz extends BaseBiz<RestaurantDishesCategoryInfoMapper,RestaurantDishesCategoryInfo> {
	

	@Autowired
	private RestaurantDishesCategoryInfoMapper restaurantDishesCategoryInfoMapper;
	@Autowired
	private RestaurantDishesInfoMapper restaurantDishesInfoMapper;
	@Autowired
	private RestaurantInfoMapper restaurantInfoMapper;
	@Autowired
	private RestaurantShoppingCartInfoMapper restaurantShoppingCartInfoMapper;
	
	/**
	 * 点餐餐品信息查询
	 * @param request
	 * @return
	 */
	public Map<String, Object> queryOrderDishesInfo(Map<String, Object> request){
		log.info("点餐餐品信息查询请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platFormId");
		String merId = (String) request.get("merId");
		String tableId = (String) request.get("tableId");
		RestaurantDishesCategoryInfo restaurantDishesCategoryInfo = new RestaurantDishesCategoryInfo();
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		restaurantDishesCategoryInfo.setPlatformId(platFormId);
		restaurantDishesCategoryInfo.setMerId(merId);
		List<RestaurantDishesCategoryInfo> select = restaurantDishesCategoryInfoMapper.select(restaurantDishesCategoryInfo);
		for(RestaurantDishesCategoryInfo infos:select){
			String id = infos.getCategoryId();
			RestaurantDishesInfo restaurantDishesInfo = new RestaurantDishesInfo();
			restaurantDishesInfo.setPlatformId(platFormId);
			restaurantDishesInfo.setMerId(merId);
			restaurantDishesInfo.setCategoryId(Integer.valueOf(id));
			List<RestaurantDishesInfo> dishesList = restaurantDishesInfoMapper.queryOrderDishes(restaurantDishesInfo);
			List<Map<String, Object>> dishesMapList = new ArrayList<Map<String, Object>>();
			//查询每一种菜当前用户购物车数量
			for(RestaurantDishesInfo dishesInfo:dishesList){
				RestaurantShoppingCartInfo restaurantShoppingCartInfo = new RestaurantShoppingCartInfo();
				restaurantShoppingCartInfo.setPlatformId(platFormId);
				restaurantShoppingCartInfo.setMerId(merId);
				restaurantShoppingCartInfo.setDishesId(Integer.valueOf(dishesInfo.getDishesId()));
				restaurantShoppingCartInfo.setTableId(Integer.valueOf(tableId));
				restaurantShoppingCartInfo.setStatus("1");//已选择
				restaurantShoppingCartInfo.setType("1");//点餐
				RestaurantShoppingCartInfo selectOne = restaurantShoppingCartInfoMapper.selectOne(restaurantShoppingCartInfo);
				Map<String, Object> map = EntityUtils.beanToMap(dishesInfo);
				if(null != selectOne){
					map.put("dishesNum", selectOne.getDishesNum());
				}else{
					map.put("dishesNum", 0);
				}
				dishesMapList.add(map);
			}
			Map<String, Object> dishesMap = EntityUtils.beanToMap(infos);
			dishesMap.put("dishList", dishesMapList);
			list.add(dishesMap);
		}
		//查询餐厅信息
		RestaurantInfo restaurantInfo = new RestaurantInfo();
		restaurantInfo.setPlatformId(platFormId);
		restaurantInfo.setMerId(merId);
		List<RestaurantInfo> restaurantInfoList = restaurantInfoMapper.select(restaurantInfo);
		response.put("dishesList", list);
		response.put("restaurantInfoList", restaurantInfoList);
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		log.info("点餐餐品信息查询响应。。。。。。{}", response);
		return response;
	}
	
	/**
	 * 预约餐品信息查询
	 * @param request
	 * @return
	 */
	public Map<String, Object> queryReservationDishesInfo(Map<String, Object> request){
		log.info("预约餐品信息查询请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platFormId");
		String merId = (String) request.get("merId");
		String perId = (String) request.get("perId");
		RestaurantDishesCategoryInfo restaurantDishesCategoryInfo = new RestaurantDishesCategoryInfo();
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		restaurantDishesCategoryInfo.setPlatformId(platFormId);
		restaurantDishesCategoryInfo.setMerId(merId);
		List<RestaurantDishesCategoryInfo> select = restaurantDishesCategoryInfoMapper.select(restaurantDishesCategoryInfo);
		for(RestaurantDishesCategoryInfo infos:select){
			String id = infos.getCategoryId();
			RestaurantDishesInfo restaurantDishesInfo = new RestaurantDishesInfo();
			restaurantDishesInfo.setPlatformId(platFormId);
			restaurantDishesInfo.setMerId(merId);
			restaurantDishesInfo.setCategoryId(Integer.valueOf(id));
			List<RestaurantDishesInfo> dishesList = restaurantDishesInfoMapper.queryOrderDishes(restaurantDishesInfo);
			List<Map<String, Object>> dishesMapList = new ArrayList<Map<String, Object>>();
			//查询每一种菜当前用户购物车数量
			for(RestaurantDishesInfo dishesInfo:dishesList){
				RestaurantShoppingCartInfo restaurantShoppingCartInfo = new RestaurantShoppingCartInfo();
				restaurantShoppingCartInfo.setPlatformId(platFormId);
				restaurantShoppingCartInfo.setMerId(merId);
				restaurantShoppingCartInfo.setDishesId(Integer.valueOf(dishesInfo.getDishesId()));
				restaurantShoppingCartInfo.setPerId(perId);
				restaurantShoppingCartInfo.setStatus("1");//已选择
				restaurantShoppingCartInfo.setType("1");//点餐
				RestaurantShoppingCartInfo selectOne = restaurantShoppingCartInfoMapper.selectOne(restaurantShoppingCartInfo);
				Map<String, Object> map = EntityUtils.beanToMap(dishesInfo);
				if(null != selectOne){
					map.put("dishesNum", selectOne.getDishesNum());
				}else{
					map.put("dishesNum", 0);
				}
				dishesMapList.add(map);
			}
			Map<String, Object> dishesMap = EntityUtils.beanToMap(infos);
			dishesMap.put("dishList", dishesMapList);
			list.add(dishesMap);
		}
		//查询餐厅信息
		RestaurantInfo restaurantInfo = new RestaurantInfo();
		restaurantInfo.setPlatformId(platFormId);
		restaurantInfo.setMerId(merId);
		List<RestaurantInfo> restaurantInfoList = restaurantInfoMapper.select(restaurantInfo);
		response.put("dishesList", list);
		response.put("restaurantInfoList", restaurantInfoList);
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		log.info("预约餐品信息查询响应。。。。。。{}", response);
		return response;
	}
	
	/**
	 * 外卖餐品信息查询
	 * @param request
	 * @return
	 */
	public Map<String, Object> queryTakeawayDishesInfo(Map<String, Object> request){
		log.info("外卖餐品信息查询请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platFormId");
		String merId = (String) request.get("merId");
		String perId = (String) request.get("perId");
		RestaurantDishesCategoryInfo restaurantDishesCategoryInfo = new RestaurantDishesCategoryInfo();
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		restaurantDishesCategoryInfo.setPlatformId(platFormId);
		restaurantDishesCategoryInfo.setMerId(merId);
		List<RestaurantDishesCategoryInfo> select = restaurantDishesCategoryInfoMapper.select(restaurantDishesCategoryInfo);
		for(RestaurantDishesCategoryInfo infos:select){
			String id = infos.getCategoryId();
			RestaurantDishesInfo restaurantDishesInfo = new RestaurantDishesInfo();
			restaurantDishesInfo.setPlatformId(platFormId);
			restaurantDishesInfo.setMerId(merId);
			restaurantDishesInfo.setCategoryId(Integer.valueOf(id));
			List<RestaurantDishesInfo> dishesList = restaurantDishesInfoMapper.queryTakeawayDishes(restaurantDishesInfo);
			List<Map<String, Object>> dishesMapList = new ArrayList<Map<String, Object>>();
			//查询每一种菜当前用户购物车数量
			for(RestaurantDishesInfo dishesInfo:dishesList){
				RestaurantShoppingCartInfo restaurantShoppingCartInfo = new RestaurantShoppingCartInfo();
				restaurantShoppingCartInfo.setPlatformId(platFormId);
				restaurantShoppingCartInfo.setMerId(merId);
				restaurantShoppingCartInfo.setDishesId(Integer.valueOf(dishesInfo.getDishesId()));
				restaurantShoppingCartInfo.setPerId(perId);
				restaurantShoppingCartInfo.setStatus("1");//已选择
				restaurantShoppingCartInfo.setType("2");//外卖
				RestaurantShoppingCartInfo selectOne = restaurantShoppingCartInfoMapper.selectOne(restaurantShoppingCartInfo);
				Map<String, Object> map = EntityUtils.beanToMap(dishesInfo);
				if(null != selectOne){
					map.put("dishesNum", selectOne.getDishesNum());
				}else{
					map.put("dishesNum", 0);
				}
				dishesMapList.add(map);
			}
			Map<String, Object> dishesMap = EntityUtils.beanToMap(infos);
			dishesMap.put("dishList", dishesMapList);
			list.add(dishesMap);
		}
		//查询餐厅信息
		RestaurantInfo restaurantInfo = new RestaurantInfo();
		restaurantInfo.setPlatformId(platFormId);
		restaurantInfo.setMerId(merId);
		List<RestaurantInfo> restaurantInfoList = restaurantInfoMapper.select(restaurantInfo);
		response.put("dishesList", list);
		response.put("restaurantInfoList", restaurantInfoList);
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		log.info("外卖餐品信息查询响应。。。。。。{}", response);
		return response;
	}
	
	/**
	 * 查询菜品类目列表
	 * @param param
	 * @return
	 */
	public Map<String, Object> queryRestaurantDishesCategoryInfoPage(Map<String, Object> param) {
		log.info("查询菜品类目列表。。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		try {
			Query query = new Query(param);
			Page<RestaurantDishesCategoryInfo> result = PageHelper.startPage(query.getPage(), query.getLimit());
			List<RestaurantDishesCategoryInfo> list = mapper.queryRestaurantDishesCategoryInfo(param);
			TableResultResponse<RestaurantDishesCategoryInfo> table = new TableResultResponse<>(result.getTotal(), list);
			respMap.put("code", "00");
			respMap.put("msg", "查询成功");
			respMap.put("body", table);
		} catch (Exception e) {
			respMap.put("code", "01");
			respMap.put("msg", "查询异常");
		}
		return respMap;
	}
	
	/**
	 * 删除
	 * @param param
	 * @return
	 */
	public Map<String, Object> deleteDishesCategory(Map<String, Object> param) {
		log.info("删除菜品类目。。。start。。。。。。。。。。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		try {
			Integer id = (Integer) param.get("id");
			RestaurantDishesCategoryInfo categoryInfo = mapper.selectByPrimaryKey(Long.valueOf(id));
			if (categoryInfo != null) {
				mapper.delete(categoryInfo);
				respMap.put("code", "00");
				respMap.put("msg", "请求成功");
			}else {
				respMap.put("code", "01");
				respMap.put("msg", "记录不存在");
			}
		} catch (Exception e) {
			e.printStackTrace();
			respMap.put("code", "01");
			respMap.put("msg", "请求异常");
		}
		log.info("删除菜品类目。。。end。。。。。。。。。。。。{}"+respMap);
		return respMap;
	}
	
	/**
	 * 添加
	 * @param param
	 * @return
	 */
	public Map<String, Object> addDishesCategory(Map<String, Object> param) {
		log.info("添加菜品类目。。。start。。。。。。。。。。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		try {
			RestaurantDishesCategoryInfo categoryInfo = new RestaurantDishesCategoryInfo();
			String platformId = (String) param.get("platformId");
			String merId = (String) param.get("merId");
			String name = (String) param.get("name");
			categoryInfo.setCategoryId(String.valueOf(SnowFlake.getId()));
			categoryInfo.setMerId(merId);
			categoryInfo.setPlatformId(platformId);
			categoryInfo.setName(name);
			mapper.insert(categoryInfo);
			respMap.put("code", "00");
			respMap.put("msg", "请求成功");
		} catch (Exception e) {
			e.printStackTrace();
			respMap.put("code", "01");
			respMap.put("msg", "请求异常");
		}
		log.info("添加菜品类目。。。end。。。。。。。。。。。。{}"+respMap);
		return respMap;
	}
	
	/**
	 * 修改
	 * @param param
	 * @return
	 */
	public Map<String, Object> updateDishesCategory(Map<String, Object> param) {
		log.info("修改菜品类目。。。start。。。。。。。。。。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		try {
			Integer id = (Integer) param.get("id");
			String name = (String) param.get("name");
			RestaurantDishesCategoryInfo categoryInfo = mapper.selectByPrimaryKey(Long.valueOf(id));
			if (categoryInfo != null) {
				categoryInfo.setName(name);
				mapper.updateByPrimaryKeySelective(categoryInfo);
				respMap.put("code", "00");
				respMap.put("msg", "请求成功");
			}else {
				respMap.put("code", "01");
				respMap.put("msg", "记录不存在");
			}
		} catch (Exception e) {
			e.printStackTrace();
			respMap.put("code", "01");
			respMap.put("msg", "请求异常");
		}
		log.info("修改菜品类目。。。end。。。。。。。。。。。。{}"+respMap);
		return respMap;
	}
	
	
}