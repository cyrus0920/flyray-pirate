package com.github.wxiaoqi.security.biz.rest.cms;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.auth.common.util.SnowFlake;
import com.github.wxiaoqi.security.biz.biz.fightGroup.FightGroupGoodsInfoBiz;
import com.github.wxiaoqi.security.biz.entity.fightGroup.FightGroupGoodsInfo;
import com.github.wxiaoqi.security.common.rest.BaseController;

import lombok.extern.slf4j.Slf4j;

/**
 * 小程序拼团商品信息相关接口
 * @author he
 *
 */
@Slf4j
@Controller
@RequestMapping("fightGroup/goodsInfo")
public class FightGroupGoodsInfoRestController extends BaseController<FightGroupGoodsInfoBiz, FightGroupGoodsInfo> {
	
	@Autowired
	private FightGroupGoodsInfoBiz fightGroupGoodsInfoBiz;
	
	/**
	 * 查询拼团商品信息列表
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Map<String, Object> query(@RequestBody Map<String, Object> param) {
		log.info("查询拼团商品信息列表------start------{}", param);
		Map<String, Object> respMap = fightGroupGoodsInfoBiz.queryFightGroupGoodsInfoPage(param);
		if (respMap.get("code").equals("00")) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("查询拼团商品信息列表-----end-------{}", respMap);
		return respMap;
	}
	
	/**
	 * 添加拼团商品
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public Map<String, Object> add(@RequestBody Map<String, Object> param) {
		log.info("添加拼团商品------start------{}", param);
		Map<String, Object> respMap = new HashMap<>();
		try {
			String id = String.valueOf(SnowFlake.getId());
			String platformId = (String) param.get("platformId");
			String appointmentReminder = (String) param.get("appointmentReminder");
			String discountPrice = (String) param.get("discountPrice");
			String effectiveTimeDescription = (String) param.get("effectiveTimeDescription");
			String goodsDescription = (String) param.get("goodsDescription");
			String goodsImg = (String) param.get("goodsImg");
			String goodsName = (String) param.get("goodsName");
			String groupDescription = (String) param.get("groupDescription");
			String originalPrice = (String) param.get("originalPrice");
			String tips = (String) param.get("tips");
			FightGroupGoodsInfo fightGroupGoodsInfo = new FightGroupGoodsInfo();
			fightGroupGoodsInfo.setAppointmentReminder(appointmentReminder);
			fightGroupGoodsInfo.setDiscountPrice(new BigDecimal(discountPrice));
			fightGroupGoodsInfo.setEffectiveTimeDescription(effectiveTimeDescription);
			fightGroupGoodsInfo.setGoodsDescription(goodsDescription);
			fightGroupGoodsInfo.setGoodsId(id);
			fightGroupGoodsInfo.setGoodsImg(goodsImg);
			fightGroupGoodsInfo.setGoodsName(goodsName);
			fightGroupGoodsInfo.setGroupDescription(groupDescription);
			fightGroupGoodsInfo.setOriginalPrice(new BigDecimal(originalPrice));
			fightGroupGoodsInfo.setPlatformId(platformId);
			fightGroupGoodsInfo.setSoldNum(0);
			fightGroupGoodsInfo.setTips(tips);
			fightGroupGoodsInfoBiz.insert(fightGroupGoodsInfo);
			respMap.put("code", "00");
			respMap.put("msg", "添加成功");
		} catch (Exception e) {
			respMap.put("code", "01");
			respMap.put("msg", "添加异常");
		}
		log.info("添加拼团商品-----end-------------{}");
		return respMap;
	}
	
	/**
	 * 删除商品
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public Map<String, Object> delete(@RequestBody Map<String, Object> param) {
		log.info("删除商品------start------{}", param);
		Map<String, Object> respMap = fightGroupGoodsInfoBiz.deleteGoodsInfo(param);
		if (respMap.get("code").equals("00")) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("删除商品------end------{}", respMap);
		return respMap;
	}

	/**
	 * 修改商品
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public Map<String, Object> update(@RequestBody Map<String, Object> param) {
		log.info("修改商品------start------{}", param);
		Map<String, Object> respMap = fightGroupGoodsInfoBiz.updateGoodsInfo(param);
		if (respMap.get("code").equals("00")) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("修改商品------end------{}", respMap);
		return respMap;
	}
}
