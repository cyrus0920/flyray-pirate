package com.github.wxiaoqi.security.biz.biz.restaurant;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.biz.entity.restaurant.RestaurantAppraisalDetailInfo;
import com.github.wxiaoqi.security.biz.entity.restaurant.RestaurantAppraisalInfo;
import com.github.wxiaoqi.security.biz.mapper.RestaurantAppraisalDetailInfoMapper;
import com.github.wxiaoqi.security.biz.mapper.RestaurantAppraisalInfoMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.util.Query;

/**
 * 菜品评价信息表
 *
 * @author he
 * @date 2018-06-29 10:31:27
 */
@Service
public class RestaurantAppraisalInfoBiz extends BaseBiz<RestaurantAppraisalInfoMapper,RestaurantAppraisalInfo> {
	
	private static final Logger logger = (Logger) LoggerFactory.getLogger(RestaurantAppraisalInfoBiz.class);
	
	@Autowired
	private RestaurantAppraisalInfoMapper restaurantAppraisalInfoMapper;
	@Autowired
	private RestaurantAppraisalDetailInfoMapper restaurantAppraisalDetailInfoMapper;
	
	/**
	 * 菜品评价信息查询
	 * @param request
	 * @return
	 */
	public Map<String, Object> queryAppraisalInfo(Map<String, Object> request){
		logger.info("菜品评价信息查询请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platFormId");
		String merId = (String) request.get("merId");
		String dishesId = (String) request.get("dishesId");
		RestaurantAppraisalInfo restaurantAppraisalInfo = new RestaurantAppraisalInfo();
		restaurantAppraisalInfo.setPlatformId(platFormId);
		restaurantAppraisalInfo.setMerId(merId);
		restaurantAppraisalInfo.setDishesId(Integer.valueOf(dishesId));
		RestaurantAppraisalInfo appraisalInfo = restaurantAppraisalInfoMapper.selectOne(restaurantAppraisalInfo);
		if(null != appraisalInfo){
			//查询评价明细列表
			RestaurantAppraisalDetailInfo restaurantAppraisalDetailInfo = new RestaurantAppraisalDetailInfo();
			restaurantAppraisalDetailInfo.setPlatformId(platFormId);
			restaurantAppraisalDetailInfo.setMerId(merId);
			restaurantAppraisalDetailInfo.setDishesId(appraisalInfo.getDishesId());
			List<RestaurantAppraisalDetailInfo> detailList = restaurantAppraisalDetailInfoMapper.queryDetailInfoList(restaurantAppraisalDetailInfo);
			response.put("detailList", detailList);
			response.put("appraisalInfo", appraisalInfo);
		}else{
			RestaurantAppraisalInfo newInfo = new RestaurantAppraisalInfo();
			newInfo.setPlatformId(platFormId);
			newInfo.setMerId(merId);
			newInfo.setDishesId(Integer.valueOf(dishesId));
			newInfo.setGeneralNum(0);
			newInfo.setNotSatisfiedNum(0);
			newInfo.setSatisfiedNum(0);
			newInfo.setTotalNum(0);
			newInfo.setVerySatisfiedNum(0);
			response.put("appraisalInfo", newInfo);
		}
		
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		logger.info("菜品评价信息查询响应。。。。。。{}", response);
		return response;
	}
	
	/**
	 * 查询评价信息列表
	 * @param param
	 * @return
	 */
	public Map<String, Object> queryRestaurantAppraisalInfoPage(Map<String, Object> param) {
		logger.info("查询评价信息列表。。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		try {
			Query query = new Query(param);
			Page<RestaurantAppraisalInfo> result = PageHelper.startPage(query.getPage(), query.getLimit());
			List<RestaurantAppraisalInfo> list = mapper.queryRestaurantAppraisalInfo(param);
			TableResultResponse<RestaurantAppraisalInfo> table = new TableResultResponse<>(result.getTotal(), list);
			respMap.put("code", "00");
			respMap.put("msg", "查询成功");
			respMap.put("body", table);
		} catch (Exception e) {
			respMap.put("code", "01");
			respMap.put("msg", "查询异常");
		}
		return respMap;
	}
}