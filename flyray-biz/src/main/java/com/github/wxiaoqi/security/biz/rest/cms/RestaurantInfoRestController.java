package com.github.wxiaoqi.security.biz.rest.cms;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.biz.biz.restaurant.RestaurantInfoBiz;
import com.github.wxiaoqi.security.biz.entity.restaurant.RestaurantInfo;
import com.github.wxiaoqi.security.common.rest.BaseController;

import lombok.extern.slf4j.Slf4j;

/**
 * 小程序点餐餐厅信息相关接口
 * @author he
 *
 */
@Slf4j
@Controller
@RequestMapping("restaurant/restaurant")
public class RestaurantInfoRestController extends BaseController<RestaurantInfoBiz, RestaurantInfo>{
	
	@Autowired
	private RestaurantInfoBiz restaurantInfoBiz;
	
	/**
	 * 查询点餐餐厅信息列表
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Map<String, Object> query(@RequestBody Map<String, Object> param) {
		log.info("查询点餐餐厅信息列表------start------{}", param);
		Map<String, Object> respMap = restaurantInfoBiz.queryRestaurantInfoPage(param);
		if (respMap.get("code").equals("00")) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("查询点餐餐厅信息列表-----end-------{}", respMap);
		return respMap;
	}
	
	/**
	 * 添加餐厅
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public Map<String, Object> add(@RequestBody Map<String, Object> param) {
		log.info("添加餐厅------start------{}", param);
		Map<String, Object> respMap = new HashMap<>();
		try {
			String platformId = (String) param.get("platformId");
			String name = (String) param.get("name");
			String logoImg = (String) param.get("logoImg");
			String area = (String) param.get("area");
			String latitude = (String) param.get("latitude");
			String longitude = (String) param.get("longitude");
			String conService = (String) param.get("conService");
			String perCapitaSpending = (String) param.get("perCapitaSpending");
			String bulletin = (String) param.get("bulletin");
			String conMobile = (String) param.get("conMobile");
			String openBtime = (String) param.get("openBtime");
			String openEtime = (String) param.get("openEtime");
			String introduce = (String) param.get("introduce");
			String qualification = (String) param.get("qualification");
			String isOrder = (String) param.get("isOrder");
			String isReservation = (String) param.get("isReservation");
			String isTakeaway = (String) param.get("isTakeaway");
			RestaurantInfo restaurantInfo = new RestaurantInfo();
			restaurantInfo.setPlatformId(platformId);
			restaurantInfo.setArea(area);
			restaurantInfo.setBulletin(bulletin);
			restaurantInfo.setConMobile(conMobile);
			restaurantInfo.setConService(conService);
			restaurantInfo.setIntroduce(introduce);
			restaurantInfo.setIsOrder(isOrder);
			restaurantInfo.setIsReservation(isReservation);
			restaurantInfo.setIsTakeaway(isTakeaway);
			restaurantInfo.setLatitude(latitude);
			restaurantInfo.setLogoImg(logoImg);
			restaurantInfo.setLongitude(longitude);
			restaurantInfo.setName(name);
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
			restaurantInfo.setOpenBtime(new Time(sdf.parse(openBtime).getTime()));
			restaurantInfo.setOpenEtime(new Time(sdf.parse(openEtime).getTime()));
			restaurantInfo.setPerCapitaSpending(perCapitaSpending);
			restaurantInfo.setQualification(qualification);
			restaurantInfoBiz.insert(restaurantInfo);
			respMap.put("code", "00");
			respMap.put("msg", "添加成功");
		} catch (Exception e) {
			respMap.put("code", "01");
			respMap.put("msg", "添加异常");
		}
		log.info("添加餐厅-----end-------------{}", respMap);
		return respMap;
	}
	
	/**
	 * 删除餐厅
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public Map<String, Object> delete(@RequestBody Map<String, Object> param) {
		log.info("删除餐厅------start------{}", param);
		Map<String, Object> respMap = restaurantInfoBiz.deleteRestaurantInfo(param);
		if (respMap.get("code").equals("00")) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("删除餐厅------end------{}", respMap);
		return respMap;
	}

	/**
	 * 修改餐厅
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public Map<String, Object> update(@RequestBody Map<String, Object> param) {
		log.info("修改餐厅------start------{}", param);
		Map<String, Object> respMap = restaurantInfoBiz.updateRestaurantInfo(param);
		if (respMap.get("code").equals("00")) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("修改餐厅------end------{}", respMap);
		return respMap;
	}

}
