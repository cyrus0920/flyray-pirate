package com.github.icloudpay.pay.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 退款流水表
 * 
 * @author hexufeng
 * @date 2018-06-05 14:17:16
 */
@Table(name = "refund_serial")
public class RefundSerial implements Serializable {
	private static final long serialVersionUID = 1L;

	//退款流水号
	@Id
	private String serialNo;

	//退款订单号
	@Column(name = "refund_order_no")
	private String refundOrderNo;

	//平台编号
	@Column(name = "platform_id")
	private String platformId;

	//商户编号
	@Column(name = "mer_id")
	private String merId;

	//支付渠道号
	@Column(name = "pay_channel_no")
	private String payChannelNo;

	//支付公司编号
	@Column(name = "pay_company_no")
	private String payCompanyNo;

	//退款金额
	@Column(name = "redund_amt")
	private BigDecimal redundAmt;

	//退款手续费
	@Column(name = "refund_fee")
	private BigDecimal refundFee;

	//退款状态（00退款成功 01退款失败 02退款处理中 03退款已发起）
	@Column(name = "refund_status")
	private String refundStatus;


	/**
	 * 设置：退款流水号
	 */
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}
	/**
	 * 获取：退款流水号
	 */
	public String getSerialNo() {
		return serialNo;
	}
	/**
	 * 设置：退款订单号
	 */
	public void setRefundOrderNo(String refundOrderNo) {
		this.refundOrderNo = refundOrderNo;
	}
	/**
	 * 获取：退款订单号
	 */
	public String getRefundOrderNo() {
		return refundOrderNo;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	public String getMerId() {
		return merId;
	}
	public void setMerId(String merId) {
		this.merId = merId;
	}
	/**
	 * 设置：支付渠道号
	 */
	public void setPayChannelNo(String payChannelNo) {
		this.payChannelNo = payChannelNo;
	}
	/**
	 * 获取：支付渠道号
	 */
	public String getPayChannelNo() {
		return payChannelNo;
	}
	/**
	 * 设置：支付公司编号
	 */
	public void setPayCompanyNo(String payCompanyNo) {
		this.payCompanyNo = payCompanyNo;
	}
	/**
	 * 获取：支付公司编号
	 */
	public String getPayCompanyNo() {
		return payCompanyNo;
	}
	/**
	 * 设置：退款金额
	 */
	public void setRedundAmt(BigDecimal redundAmt) {
		this.redundAmt = redundAmt;
	}
	/**
	 * 获取：退款金额
	 */
	public BigDecimal getRedundAmt() {
		return redundAmt;
	}
	/**
	 * 设置：退款手续费
	 */
	public void setRefundFee(BigDecimal refundFee) {
		this.refundFee = refundFee;
	}
	/**
	 * 获取：退款手续费
	 */
	public BigDecimal getRefundFee() {
		return refundFee;
	}
	/**
	 * 设置：退款状态（00退款成功 01退款失败 02退款处理中 03退款已发起）
	 */
	public void setRefundStatus(String refundStatus) {
		this.refundStatus = refundStatus;
	}
	/**
	 * 获取：退款状态（00退款成功 01退款失败 02退款处理中 03退款已发起）
	 */
	public String getRefundStatus() {
		return refundStatus;
	}
}
