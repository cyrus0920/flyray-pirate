package com.github.icloudpay.pay.core.config;

import java.util.ArrayList;
import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.github.wxiaoqi.security.auth.client.interceptor.ServiceAuthRestInterceptor;
import com.github.wxiaoqi.security.auth.client.interceptor.UserAuthRestInterceptor;
import com.github.wxiaoqi.security.auth.client.interceptor.ValidateParamInterceptor;
import com.github.wxiaoqi.security.common.handler.GlobalExceptionHandler;

/**
 *
 * @author ace
 * @date 2017/9/8
 */
@Configuration("payCoreWebConfig")
@Primary
public class WebConfiguration implements WebMvcConfigurer {
    @Bean
    GlobalExceptionHandler getGlobalExceptionHandler() {
        return new GlobalExceptionHandler();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
    	ArrayList<String> valPathPatterns = getExcludeValidatePathPatterns();
    	valPathPatterns.add("/admin/*/*");
        registry.addInterceptor(getServiceAuthRestInterceptor()).addPathPatterns(getIncludePathPatterns()).addPathPatterns("/api/user/validate");
        /*registry.addInterceptor(getValidateParamInterceptor()).addPathPatterns("/**").excludePathPatterns(valPathPatterns.toArray(new String[]{}));*/
        registry.addInterceptor(getUserAuthRestInterceptor()).
                addPathPatterns(getIncludePathPatterns());
    }

    @Bean
    ServiceAuthRestInterceptor getServiceAuthRestInterceptor() {
        return new ServiceAuthRestInterceptor();
    }

    @Bean
    UserAuthRestInterceptor getUserAuthRestInterceptor() {
        return new UserAuthRestInterceptor();
    }
    
    @Bean
    ValidateParamInterceptor getValidateParamInterceptor() {
        return new ValidateParamInterceptor();
    }
    /**
     * 需要用户和服务认证判断的路径
     * @return
     */
    private ArrayList<String> getIncludePathPatterns() {
        ArrayList<String> list = new ArrayList<>();
        String[] urls = {
                "/element/**",
                "/gateLog/**",
                "/group/**",
                "/groupType/**",
                "/menu/**",
                "/user/**",
                "/api/permissions",
                "/demo/**"
        };
        Collections.addAll(list, urls);
        return list;
    }
    
    private ArrayList<String> getExcludeValidatePathPatterns() {
        ArrayList<String> list = new ArrayList<>();
        String[] urls = {
                "/v2/api-docs",
                "/swagger-resources/**",
                "/cache/**",
                "/api/log/save",
                "/activity/*",
                "/coupon/*",
                "/incident/*",
                "/integral/*",
                "/common/*",
                "/marScene/*",
                "/job/*",
                "/scene/*",
                "/jobLog/*",
                "/customer/queryCustomerinfoByMobile"
        };
        Collections.addAll(list, urls);
        return list;
    }

}
