package com.github.icloudpay.pay.core.service.refund.wechat.domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "xml")
@XmlAccessorType(XmlAccessType.FIELD)
public class WechatRefundStatusResponse {
	@XmlElement(name = "return_code")
	private String returnCode;

	@XmlElement(name = "return_msg")
	private String returnMsg;

	@XmlElement(name = "result_code")
	private String resultCode;

	@XmlElement(name = "err_code")
	private String errCode;

	@XmlElement(name = "mch_id")
	private String mchId;

	@XmlElement(name = "device_info")
	private String deviceInfo;

	@XmlElement(name = "nonce_str")
	private String nonceStr;

	@XmlElement(name = "sign")
	private String sign;

	@XmlElement(name = "transaction_id")
	private String transactionId;

	@XmlElement(name = "out_trade_no")
	private String outTradeNo;

	@XmlElement(name = "total_fee")
	private String totalFee;

	@XmlElement(name = "refund_count")
	private String refundCount;

	@XmlElement(name = "out_refund_no_0")
	private String outRefundNo;

	@XmlElement(name = "refund_id_0")
	private String refundId;

	@XmlElement(name = "refund_fee_0")
	private String refundFee;

	@XmlElement(name = "refund_status_0")
	private String refundStatus;

	public String getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}

	public String getReturnMsg() {
		return returnMsg;
	}

	public void setReturnMsg(String returnMsg) {
		this.returnMsg = returnMsg;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getErrCode() {
		return errCode;
	}

	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}

	public String getMchId() {
		return mchId;
	}

	public void setMchId(String mchId) {
		this.mchId = mchId;
	}

	public String getDeviceInfo() {
		return deviceInfo;
	}

	public void setDeviceInfo(String deviceInfo) {
		this.deviceInfo = deviceInfo;
	}

	public String getNonceStr() {
		return nonceStr;
	}

	public void setNonceStr(String nonceStr) {
		this.nonceStr = nonceStr;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getOutTradeNo() {
		return outTradeNo;
	}

	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo = outTradeNo;
	}

	public String getTotalFee() {
		return totalFee;
	}

	public void setTotalFee(String totalFee) {
		this.totalFee = totalFee;
	}

	public String getRefundCount() {
		return refundCount;
	}

	public void setRefundCount(String refundCount) {
		this.refundCount = refundCount;
	}

	public String getOutRefundNo() {
		return outRefundNo;
	}

	public void setOutRefundNo(String outRefundNo) {
		this.outRefundNo = outRefundNo;
	}

	public String getRefundId() {
		return refundId;
	}

	public void setRefundId(String refundId) {
		this.refundId = refundId;
	}

	public String getRefundFee() {
		return refundFee;
	}

	public void setRefundFee(String refundFee) {
		this.refundFee = refundFee;
	}

	public String getRefundStatus() {
		return refundStatus;
	}

	public void setRefundStatus(String refundStatus) {
		this.refundStatus = refundStatus;
	}

	@Override
	public String toString() {
		return "WechatRefundStatusResponse [returnCode=" + returnCode + ", returnMsg=" + returnMsg + ", resultCode="
				+ resultCode + ", errCode=" + errCode + ", mchId=" + mchId + ", deviceInfo=" + deviceInfo
				+ ", nonceStr=" + nonceStr + ", sign=" + sign + ", transactionId=" + transactionId + ", outTradeNo="
				+ outTradeNo + ", totalFee=" + totalFee + ", refundCount=" + refundCount + ", outRefundNo="
				+ outRefundNo + ", refundId=" + refundId + ", refundFee=" + refundFee + ", refundStatus=" + refundStatus
				+ "]";
	}

}
