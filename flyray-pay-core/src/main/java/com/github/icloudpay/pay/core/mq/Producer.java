package com.github.icloudpay.pay.core.mq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 生产者
 * Created by zhongcy on 2017-02-22.
 */
@Component
public class Producer {
	
	private static final Logger logger = LoggerFactory.getLogger(Producer.class);


	@Autowired
	private AmqpTemplate rabbitTemplate;

	public void send() {
		String context = "hi, i am message all";
		System.out.println("Sender : " + context);
		this.rabbitTemplate.convertAndSend("topic.payment", context);
	}
}
