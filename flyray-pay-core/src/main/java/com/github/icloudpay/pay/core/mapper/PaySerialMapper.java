package com.github.icloudpay.pay.core.mapper;

import java.util.List;

import com.github.icloudpay.pay.core.entity.PaySerial;

import tk.mybatis.mapper.common.Mapper;

/**
 * 支付流水表
 * 
 * @author hexufeng
 * @date 2018-06-05 14:17:16
 */
@org.apache.ibatis.annotations.Mapper
public interface PaySerialMapper extends Mapper<PaySerial> {
	
	/**
	 * 查询处理中的支付流水数
	 * @param endDateTime
	 * @return
	 */
	public int getCountByDateTime(String endDateTime);
	
	/**
	 * 查询处理中的支付流水
	 * @param endDateTime
	 * @param page
	 * @param limit
	 * @return
	 */
	public List<PaySerial> queryPaySerialList(String endDateTime, int page, int limit);
	
}
