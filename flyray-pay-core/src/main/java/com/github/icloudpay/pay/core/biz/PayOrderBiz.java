package com.github.icloudpay.pay.core.biz;

import org.springframework.stereotype.Service;

import com.github.icloudpay.pay.core.entity.PayOrder;
import com.github.icloudpay.pay.core.mapper.PayOrderMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

/**
 * 支付订单表
 *
 * @author hexufeng
 * @email lfw6699@163.com
 * @date 2018-06-05 14:17:16
 */
@Service
public class PayOrderBiz extends BaseBiz<PayOrderMapper,PayOrder> {
}