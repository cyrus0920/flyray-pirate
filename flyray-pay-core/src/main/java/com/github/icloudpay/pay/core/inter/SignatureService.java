package com.github.icloudpay.pay.core.inter;

import java.util.Map;

/**
 * 签名服务
 * @author zhangjun
 *
 * 2015-7-29
 */
public interface SignatureService {
    /**
     * 签名
     * @param toBeSign
     * @param payChannelNo
     * @return
     */
    public String sign(String toBeSign , Map<String, Object> configMap);
    
    
    public boolean verify(String toBeSign, String sign, Map<String, Object> configMap);
    
}
