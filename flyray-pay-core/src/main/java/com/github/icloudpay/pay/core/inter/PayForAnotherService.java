package com.github.icloudpay.pay.core.inter;

import com.github.wxiaoqi.security.common.admin.pay.request.PayForAnotherRequest;
import com.github.wxiaoqi.security.common.admin.pay.response.PayForAnotherResponse;

/**
 * 向第三方发起代付
 * @author hexufeng
 *
 */
public interface PayForAnotherService {
	
	public PayForAnotherResponse toPayForAnother(PayForAnotherRequest request);

}
