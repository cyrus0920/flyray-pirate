package com.github.icloudpay.pay.core.biz;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.icloudpay.pay.core.entity.PayChannel;
import com.github.icloudpay.pay.core.mapper.PayChannelMapper;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.util.Query;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class PayChannelBiz extends BaseBiz<PayChannelMapper, PayChannel> {

	@Autowired
	private PayChannelMapper mapper;
	
	/**
	 * 支付通道
	 * @param map
	 * @return
	 */
	public Map<String, Object> queryPayChannel(Map<String, Object> param) {
		log.info("查询支付通道，请求参数.{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		try {
			Query query = new Query(param);
			Page<PayChannel> result = PageHelper.startPage(query.getPage(), query.getLimit());
			List<PayChannel> list = mapper.queryPayChannel(param);
			log.info("查询支付通道列表，响应参数.。。。{}"+list);
			TableResultResponse<PayChannel> table = new TableResultResponse<>(result.getTotal(), list);
			respMap.put("code", "00");
			respMap.put("msg", "请求成功");
			respMap.put("body", table);
		} catch (Exception e) {
			e.printStackTrace();
			respMap.put("code", "01");
			respMap.put("msg", "请求异常");
		}
		return respMap;
	}
	
	/**
	 * 详情
	 * @param param
	 * @return
	 */
	public Map<String, Object> payChannelInfo(Map<String, Object> param) {
		log.info("查询支付通道详情，请求参数.{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		try {
			PayChannel config = mapper.selectByPrimaryKey((Integer) param.get("id"));
			if (config != null) {
				respMap.put("info", config);
				respMap.put("code", "00");
				respMap.put("msg", "请求成功");
			}else {
				respMap.put("code", "01");
				respMap.put("msg", "通道不存在");
			}
		} catch (Exception e) {
			e.printStackTrace();
			respMap.put("code", "01");
			respMap.put("msg", "请求异常");
		}
		return respMap;
	}
	
	/**
	 * 添加
	 * @param param
	 * @return
	 */
	public Map<String, Object> addPayChannel(Map<String, Object> params) {
		log.info("添加支付通道。。。。。start。。。{}"+params);
		Map<String, Object> respMap = new HashMap<>();
		try {
			PayChannel config = new PayChannel();
			String payChannelNo = (String) params.get("payChannelNo");
			String payChannelName = (String) params.get("payChannelName");
			String payCompanyNo = (String) params.get("payCompanyNo");
			String trandeType = (String) params.get("trandeType");
			String payCompanyName = (String) params.get("payCompanyName");
			config.setPayChannelNo(payChannelNo);
			config.setPayCompanyNo(payCompanyNo);
			config.setTradeType(trandeType);
			config.setPayChannelName(payChannelName);
			config.setPayCompanyName(payCompanyName);
			config.setCreateTime(new Date());
			config.setUpdateTime(new Date());
			mapper.insert(config);
			respMap.put("code", "00");
			respMap.put("msg", "请求成功");
		} catch (Exception e) {
			e.printStackTrace();
			respMap.put("code", "01");
			respMap.put("msg", "请求异常");
		}
		log.info("添加支付通道。。。。。end。。。{}"+respMap);
		return respMap;
	}
	
	
	/**
	 * 更新
	 * @param params
	 * @return
	 */
	public Map<String, Object> updatePayChannel(Map<String, Object> params) {
		log.info("更新支付通道。。。。。start。。。{}"+params);
		Map<String, Object> respMap = new HashMap<>();
		try {
			String payChannelNo = (String) params.get("payChannelNo");
			String payChannelName = (String) params.get("payChannelName");
			String payCompanyNo = (String) params.get("payCompanyNo");
			String trandeType = (String) params.get("trandeType");
			String payCompanyName = (String) params.get("payCompanyName");
			Integer id = (Integer) params.get("id");
			PayChannel config = new PayChannel();
			config.setPayChannelNo(payChannelNo);
			config.setPayChannelName(payChannelName);
			config.setPayCompanyNo(payCompanyNo);
			config.setTradeType(trandeType);
			config.setPayCompanyName(payCompanyName);
			config.setId(id);
			config.setUpdateTime(new Date());
			mapper.updateByPrimaryKey(config);
			respMap.put("code", "00");
			respMap.put("msg", "请求成功");
		} catch (Exception e) {
			e.printStackTrace();
			respMap.put("code", "01");
			respMap.put("msg", "请求异常");
		}
		log.info("更新支付通道。。。。。end。。。{}"+respMap);
		return respMap;
	}
}
