package com.github.icloudpay.pay.core.biz;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.icloudpay.pay.core.entity.PayForAnotherOrder;
import com.github.icloudpay.pay.core.mapper.PayForAnotherOrderMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

/**
 * 代付
 * @author hexufeng
 * @email lfw6699@163.com
 * @date 2018-06-12 11:32:12
 */
@Service
public class PayForAnotherOrderBiz extends BaseBiz<PayForAnotherOrderMapper,PayForAnotherOrder> {
	
	@Autowired
	private PayForAnotherOrderMapper payForAnotherOrderMapper;
	
	/**
	 * 查询处理中的代付订单数
	 * @param endDateTime
	 * @return
	 */
	public int getCountByDateTime(String endDateTime){
		return payForAnotherOrderMapper.getCountByDateTime(endDateTime);
	}
	
	/**
	 * 查询处理中的代付订单
	 * @param endDateTime
	 * @param page
	 * @param limit
	 * @return
	 */
	public List<PayForAnotherOrder> queryPayForAnotherList(String endDateTime, int page, int limit){
		return payForAnotherOrderMapper.queryPayForAnotherList(endDateTime, page, limit);
	}
	
}