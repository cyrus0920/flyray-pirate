package com.github.icloudpay.pay.core.biz;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.github.icloudpay.pay.core.entity.PayChannelInterface;
import com.github.icloudpay.pay.core.mapper.PayChannelInterfaceMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class PayChannelInterfaceBiz extends BaseBiz<PayChannelInterfaceMapper, PayChannelInterface> {

	
	
	/**
	 * 查询列表
	 * @param map
	 * @return
	 */
	public Map<String, Object> queryList(Map<String, Object> map) {
		log.info("查询支付通道接口配置......start......."+map);
		Map<String, Object> respMap = new HashMap<>();
		try {
			List<PayChannelInterface> list = mapper.queryInterfaces(map);
			log.info("查询支付通道接口配置。。。。list...."+list);
			respMap.put("code", "00");
			respMap.put("msg", "请求成功");
			respMap.put("list", list);
		} catch (Exception e) {
			e.printStackTrace();
			respMap.put("code", "01");
			respMap.put("msg", "异常");
		}
		log.info("查询支付通道接口配置............end......."+respMap);
		return respMap;
	}
	
	/**
	 * 添加
	 * @param map
	 * @return
	 */
	public Map<String, Object> addInterface(Map<String, Object> map) {
		log.info("添加支付通道接口配置......start......."+map);
		Map<String, Object> respMap = new HashMap<>();
		try {
			String payChannelNo = (String) map.get("payChannelNo");
			String trandeType = (String) map.get("trandeType");
			String serviceName = (String) map.get("serviceName");
			PayChannelInterface interface1 = new PayChannelInterface();
			interface1.setPayChannelNo(payChannelNo);
			interface1.setTradeType(trandeType);
			interface1.setServiceName(serviceName);
			interface1.setCreateTime(new Date());
			interface1.setUpdateTime(new Date());
			mapper.insert(interface1);
			respMap.put("code", "00");
			respMap.put("msg", "请求成功");
		} catch (Exception e) {
			e.printStackTrace();
			respMap.put("code", "01");
			respMap.put("msg", "异常");
		}
		log.info("添加支付通道接口配置............end......."+respMap);
		return respMap;
	}
	
	/**
	 * 更新
	 * @param map
	 * @return
	 */
	public Map<String, Object> updateInterface(Map<String, Object> map) {
		log.info("更新支付通道接口配置......start......."+map);
		Map<String, Object> respMap = new HashMap<>();
		try {
			String payChannelNo = (String) map.get("payChannelNo");
			String trandeType = (String) map.get("trandeType");
			String serviceName = (String) map.get("serviceName");
			Integer id = (Integer) map.get("id"); 
			PayChannelInterface interface1 = mapper.selectByPrimaryKey(id);
			if (interface1 != null) {
				interface1.setPayChannelNo(payChannelNo);
				interface1.setTradeType(trandeType);
				interface1.setServiceName(serviceName);
				interface1.setUpdateTime(new Date());
				mapper.updateByPrimaryKey(interface1);
				respMap.put("code", "00");
				respMap.put("msg", "请求成功");
			}else {
				respMap.put("code", "01");
				respMap.put("msg", "记录不存在");
			}
		} catch (Exception e) {
			e.printStackTrace();
			respMap.put("code", "01");
			respMap.put("msg", "异常");
		}
		log.info("更新支付通道接口配置............end......."+respMap);
		return respMap;
	}
	
	
	/**
	 * 删除
	 * @param map
	 * @return
	 */
	public Map<String, Object> deleteInterface(Map<String, Object> map) {
		log.info("删除支付通道接口配置......start......."+map);
		Map<String, Object> respMap = new HashMap<>();
		try {
			String payChannelNo = (String) map.get("payChannelNo");
			Integer id = (Integer) map.get("id");
			PayChannelInterface interface1 = mapper.selectByPrimaryKey(id);
			if (interface1 != null) {
				mapper.delete(interface1);
				respMap.put("code", "00");
				respMap.put("msg", "请求成功");
			}else {
				respMap.put("code", "01");
				respMap.put("msg", "记录不存在");
			}
		} catch (Exception e) {
			e.printStackTrace();
			respMap.put("code", "01");
			respMap.put("msg", "异常");
		}
		log.info("删除支付通道接口配置............end......."+respMap);
		return respMap;
	}
}
