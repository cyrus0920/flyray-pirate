package com.github.icloudpay.pay.core.biz;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.icloudpay.pay.core.entity.PaySerial;
import com.github.icloudpay.pay.core.mapper.PaySerialMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

/**
 * 支付流水表
 *
 * @author hexufeng
 * @email lfw6699@163.com
 * @date 2018-06-05 14:17:16
 */
@Service
public class PaySerialBiz extends BaseBiz<PaySerialMapper,PaySerial> {
	
	@Autowired
	private PaySerialMapper paySerialMapper;
	
	/**
	 * 查询处理中的支付流水数
	 * @param endDateTime
	 * @return
	 */
	public int getCountByDateTime(String endDateTime){
		return paySerialMapper.getCountByDateTime(endDateTime);
	}
	
	/**
	 * 查询处理中的支付流水
	 * @param endDateTime
	 * @param page
	 * @param limit
	 * @return
	 */
	public List<PaySerial> queryPaySerialList(String endDateTime, int page, int limit){
		return paySerialMapper.queryPaySerialList(endDateTime, page, limit);
	}
	
}