package com.github.icloudpay.pay.core.mapper;

import java.util.List;
import java.util.Map;

import com.github.icloudpay.pay.core.entity.PayChannel;
import tk.mybatis.mapper.common.Mapper;

/**
 * 支付通道信息表
 * 
 * @author hexufeng
 * @date 2018-06-05 14:17:16
 */
@org.apache.ibatis.annotations.Mapper
public interface PayChannelMapper extends Mapper<PayChannel> {
	
	/**
	 * 
	 * @param map
	 * @return
	 */
	List<PayChannel> queryPayChannel(Map<String, Object> map);
}
