package com.github.icloudpay.pay.core.inter;

import com.github.wxiaoqi.security.common.admin.pay.request.RefundApplyRequest;
import com.github.wxiaoqi.security.common.admin.pay.request.RefundRequest;
import com.github.wxiaoqi.security.common.admin.pay.response.RefundApplyResponse;
import com.github.wxiaoqi.security.common.admin.pay.response.RefundResponse;

/** 
* @author: bolei
* @date：2017年2月23日 上午11:54:03 
* @description：退款逻辑接口
*/

public interface RefundHandlerService {
	
	/**
	 * 退款申请
	 * @param request
	 * @return
	 */
	public RefundApplyResponse refundApply(RefundApplyRequest request);

	/**
	 * 退款审核
	 * @param request
	 * @return
	 */
	public RefundResponse refund(RefundRequest request);
}
