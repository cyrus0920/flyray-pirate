package com.github.icloudpay.pay.core.service.pay.wechat.domain;

import javax.xml.bind.annotation.XmlRootElement;

/** 
* @author: bolei
* @date：2017年4月30日 下午1:54:48 
* @description：类说明 
*/
@XmlRootElement(name = "xml")
public class QueryOrderElement {


//		@XmlAttribute(name = "appid")
		private String appid;
		
//		@XmlAttribute(name = "mch_id")
		private String mch_id;
		
//		@XmlAttribute(name = "nonce_str")
		private String nonce_str;
			
//		@XmlAttribute(name = "out_trade_no")
		private String out_trade_no;
	
//		@XmlAttribute(name = "sign")
		private String sign;

		public String getAppid() {
			return appid;
		}

		public void setAppid(String appid) {
			this.appid = appid;
		}

		public String getMch_id() {
			return mch_id;
		}

		public void setMch_id(String mch_id) {
			this.mch_id = mch_id;
		}

		public String getNonce_str() {
			return nonce_str;
		}

		public void setNonce_str(String nonce_str) {
			this.nonce_str = nonce_str;
		}

		public String getOut_trade_no() {
			return out_trade_no;
		}

		public void setOut_trade_no(String out_trade_no) {
			this.out_trade_no = out_trade_no;
		}

		public String getSign() {
			return sign;
		}

		public void setSign(String sign) {
			this.sign = sign;
		}


}


