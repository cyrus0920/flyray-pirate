package com.github.icloudpay.pay.core.inter;

import com.github.wxiaoqi.security.common.admin.pay.request.PayOrderRequest;
import com.github.wxiaoqi.security.common.admin.pay.response.PayOrderResponse;

/** 
* @author: bolei
* @date：2017年4月30日 下午1:20:20 
* @description：支付订单处理,创建支付订单
*/

public interface PayOrderHandleService {

	/**
	 * 创建支付订单
	 */
	public PayOrderResponse createPayOrder(PayOrderRequest request);
}
