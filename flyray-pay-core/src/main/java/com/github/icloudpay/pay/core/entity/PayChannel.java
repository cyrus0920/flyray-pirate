package com.github.icloudpay.pay.core.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;


/**
 * 支付通道配置
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-08-06 13:58:35
 */
@Table(name = "pay_channel")
public class PayChannel implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //序号
    @Id
    private Integer id;
	
	    //支付公司编号
    @Column(name = "pay_company_no")
    private String payCompanyNo;
	
	    //支付公司名称
    @Column(name = "pay_company_name")
    private String payCompanyName;
	
	    //支付通道编号
    @Column(name = "pay_channel_no")
    private String payChannelNo;
	
	    //支付通道名称
    @Column(name = "pay_channel_name")
    private String payChannelName;
	
	    //交易类型（仅做展示）  00：支付  01：退款  02：代收  03：代付
    @Column(name = "trade_type")
    private String tradeType;
	
	    //创建时间
    @Column(name = "create_time")
    private Date createTime;
	
	    //更新时间
    @Column(name = "update_time")
    private Date updateTime;
	

	/**
	 * 设置：序号
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：序号
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：支付公司编号
	 */
	public void setPayCompanyNo(String payCompanyNo) {
		this.payCompanyNo = payCompanyNo;
	}
	/**
	 * 获取：支付公司编号
	 */
	public String getPayCompanyNo() {
		return payCompanyNo;
	}
	/**
	 * 设置：支付公司名称
	 */
	public void setPayCompanyName(String payCompanyName) {
		this.payCompanyName = payCompanyName;
	}
	/**
	 * 获取：支付公司名称
	 */
	public String getPayCompanyName() {
		return payCompanyName;
	}
	/**
	 * 设置：支付通道编号
	 */
	public void setPayChannelNo(String payChannelNo) {
		this.payChannelNo = payChannelNo;
	}
	/**
	 * 获取：支付通道编号
	 */
	public String getPayChannelNo() {
		return payChannelNo;
	}
	/**
	 * 设置：支付通道名称
	 */
	public void setPayChannelName(String payChannelName) {
		this.payChannelName = payChannelName;
	}
	/**
	 * 获取：支付通道名称
	 */
	public String getPayChannelName() {
		return payChannelName;
	}
	/**
	 * 设置：交易类型（仅做展示）  00：支付  01：退款  02：代收  03：代付
	 */
	public void setTradeType(String tradeType) {
		this.tradeType = tradeType;
	}
	/**
	 * 获取：交易类型（仅做展示）  00：支付  01：退款  02：代收  03：代付
	 */
	public String getTradeType() {
		return tradeType;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：更新时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	/**
	 * 获取：更新时间
	 */
	public Date getUpdateTime() {
		return updateTime;
	}
	@Override
	public String toString() {
		return "PayChannel [id=" + id + ", payCompanyNo=" + payCompanyNo + ", payCompanyName=" + payCompanyName
				+ ", payChannelNo=" + payChannelNo + ", payChannelName=" + payChannelName + ", tradeType=" + tradeType
				+ ", createTime=" + createTime + ", updateTime=" + updateTime + "]";
	}
}
