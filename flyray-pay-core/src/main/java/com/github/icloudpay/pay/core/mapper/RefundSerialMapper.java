package com.github.icloudpay.pay.core.mapper;

import java.util.List;

import com.github.icloudpay.pay.core.entity.RefundSerial;

import tk.mybatis.mapper.common.Mapper;

/**
 * 退款流水表
 * 
 * @author hexufeng
 * @date 2018-06-05 14:17:16
 */
@org.apache.ibatis.annotations.Mapper
public interface RefundSerialMapper extends Mapper<RefundSerial> {
	
	/**
	 * 查询处理中的退款流水数
	 * @param endDateTime
	 * @return
	 */
	public int getCountByDateTime(String endDateTime);
	
	/**
	 * 查询处理中的退款流水
	 * @param endDateTime
	 * @param page
	 * @param limit
	 * @return
	 */
	public List<RefundSerial> queryRefundSerialList(String endDateTime, int page, int limit);
	
}
