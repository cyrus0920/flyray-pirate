package com.github.icloudpay.pay.core.rest;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.icloudpay.pay.core.biz.PayChannelBiz;
import com.github.icloudpay.pay.core.entity.PayChannel;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("payChannels")
public class PayChannelController {

	
	@Autowired
	private PayChannelBiz biz;
	
	@ResponseBody
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Map<String, Object> queryPayChannel(@RequestBody Map<String, Object> param) {
		log.info("查询支付通道列表，请求参数。。。{}"+param);
		Map<String, Object> respMap = biz.queryPayChannel(param);
		if (respMap.get("code").equals("00")) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("查询支付通道列表，响应参数。。。{}"+respMap);
		return respMap;
	}
	
	
	/**
	 * 详情
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/info", method = RequestMethod.POST)
	public Map<String, Object> queryPayChannelConfigInfo(@RequestBody Map<String, Object> params) {
		log.info("查询支付通道，请求参数.。。。{}"+params);
		Map<String, Object> respMap = biz.payChannelInfo(params);
		if (respMap.get("code").equals("00")) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("查询支付通道，响应参数.。。。{}"+respMap);
		return respMap;
	}
	
	/**
	 * 删除
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public Map<String, Object> deletePayChannel(@RequestBody Map<String, Object> params) {
		log.info("删除支付通道，请求参数。。。{}"+params);
		Map<String, Object> respMap = new HashMap<>();
		try {
			PayChannel config = new PayChannel();
			String payChannelNo = (String) params.get("payChannelNo");
			config.setPayChannelNo(payChannelNo);
			biz.delete(config);
			respMap.put("code", "00");
			respMap.put("msg", "请求成功");
			respMap.put("success", true);
		} catch (Exception e) {
			respMap.put("code", "01");
			respMap.put("msg", "异常");
		}
		return respMap;
	}
	
	
	/**
	 * 添加
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public Map<String, Object> addPayChannel(@RequestBody Map<String, Object> params) {
		log.info("添加支付通道，请求参数。。。{}"+params);
		Map<String, Object> respMap = biz.addPayChannel(params);
		if (respMap.get("code").equals("00")) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("添加支付通道，响应参数.。。。{}"+respMap);
		return respMap;
	}
	
	/**
	 * 更新
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public Map<String, Object> updatePayChannelConfig(@RequestBody Map<String, Object> params) {
		log.info("更新支付通道，请求参数。。。{}"+params);
		Map<String, Object> respMap = biz.updatePayChannel(params);
		if (respMap.get("code").equals("00")) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("更新支付通道，响应参数.。。。{}"+respMap);
		return respMap;
	}
	

}
