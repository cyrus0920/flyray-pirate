package com.github.icloudpay.pay.core.biz;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.icloudpay.pay.core.entity.RefundSerial;
import com.github.icloudpay.pay.core.mapper.RefundSerialMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

/**
 * 退款流水表
 *
 * @author hexufeng
 * @email lfw6699@163.com
 * @date 2018-06-05 14:17:16
 */
@Service
public class RefundSerialBiz extends BaseBiz<RefundSerialMapper,RefundSerial> {
	
	@Autowired
	private RefundSerialMapper refundSerialMapper;
	
	/**
	 * 查询处理中的退款流水数
	 * @param endDateTime
	 * @return
	 */
	public int getCountByDateTime(String endDateTime){
		return refundSerialMapper.getCountByDateTime(endDateTime);
	}
	
	/**
	 * 查询处理中的退款流水
	 * @param endDateTime
	 * @param page
	 * @param limit
	 * @return
	 */
	public List<RefundSerial> queryRefundSerialList(String endDateTime, int page, int limit){
		return refundSerialMapper.queryRefundSerialList(endDateTime, page, limit);
	}
}