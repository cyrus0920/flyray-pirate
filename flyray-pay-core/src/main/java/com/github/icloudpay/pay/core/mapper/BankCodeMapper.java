package com.github.icloudpay.pay.core.mapper;

import java.util.List;

import com.github.wxiaoqi.security.common.admin.pay.response.BankCode;

import tk.mybatis.mapper.common.Mapper;

/**
 * 银行编码
 * @author hexufeng
 * @email lfw6699@163.com
 * @date 2018-06-12 11:32:12
 */
@org.apache.ibatis.annotations.Mapper
public interface BankCodeMapper extends Mapper<BankCode> {
	
	List<BankCode> queryBankCode();
	
}
