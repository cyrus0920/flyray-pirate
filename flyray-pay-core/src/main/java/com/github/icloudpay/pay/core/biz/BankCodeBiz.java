package com.github.icloudpay.pay.core.biz;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.icloudpay.pay.core.mapper.BankCodeMapper;
import com.github.wxiaoqi.security.common.admin.pay.response.BankCode;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

/**
 * 银行编码
 * @author hexufeng
 * @email lfw6699@163.com
 * @date 2018-06-12 11:32:12
 */
@Service
public class BankCodeBiz extends BaseBiz<BankCodeMapper,BankCode> {
	
	@Autowired
	private BankCodeMapper bankCodeMapper;
	
	public List<BankCode> queryBankCode(){
		List<BankCode> queryBankCode = bankCodeMapper.queryBankCode();
		return queryBankCode;
	}
	
}