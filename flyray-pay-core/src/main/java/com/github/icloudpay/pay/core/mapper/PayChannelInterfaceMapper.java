package com.github.icloudpay.pay.core.mapper;

import java.util.List;
import java.util.Map;

import com.github.icloudpay.pay.core.entity.PayChannelInterface;
import tk.mybatis.mapper.common.Mapper;

/**
 * 支付通道接口关系表
 * 
 * @author hexufeng
 * @date 2018-06-05 14:17:16
 */
public interface PayChannelInterfaceMapper extends Mapper<PayChannelInterface> {
	
	public List<PayChannelInterface> queryInterfaces(Map<String, Object> map);
}
