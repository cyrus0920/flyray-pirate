package com.github.icloudpay.pay.core.inter;

import com.github.wxiaoqi.security.common.admin.pay.request.PayForAnotherApplyRequest;
import com.github.wxiaoqi.security.common.admin.pay.request.PayForAnotherRequest;
import com.github.wxiaoqi.security.common.admin.pay.response.PayForAnotherApplyResponse;
import com.github.wxiaoqi.security.common.admin.pay.response.PayForAnotherResponse;

/**
 * 代付逻辑处理
 * @author hexufeng
 *
 */
public interface PayForAnotherHandlerService {
	
	/**
	 * 代付申请
	 * @param request
	 * @return
	 */
	public PayForAnotherApplyResponse payForAnotherApply(PayForAnotherApplyRequest request);
	
	/**
	 * 发起代付
	 * @param request
	 * @return
	 */
	public PayForAnotherResponse payForAnother(PayForAnotherRequest request);

}
