package com.github.icloudpay.pay.core.inter;

import com.github.wxiaoqi.security.common.admin.pay.request.RefundRequest;
import com.github.wxiaoqi.security.common.admin.pay.response.RefundResponse;

/**
 * 退款服务接口
 * 
 * @author zhangjun
 * 
 *         2015-7-9
 */
public abstract class RefundService {

    public RefundResponse refund(RefundRequest refundRequest){
    	 return null;
    }

    /**
     * @Title: refund
     * @Description: 退款无密接口
     * @param RefundRequest
     * @return RefundResponse 2015-7-20 下午2:34:47
     * @author wang.hai
     * @throws
     */
    public abstract RefundResponse refundNoPwd(RefundRequest refundNoPwdRequest);
}
