package com.github.icloudpay.pay.core;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.github.icloudpay.pay.core.mq.Producer;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TopicTest {

	@Autowired
	private Producer sender;

	@Test
	public void topic() throws Exception {
		sender.send();
	}

}