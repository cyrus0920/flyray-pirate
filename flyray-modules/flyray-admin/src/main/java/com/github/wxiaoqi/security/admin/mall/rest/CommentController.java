package com.github.wxiaoqi.security.admin.mall.rest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.wxiaoqi.security.admin.mall.domain.LitemallComment;
import com.github.wxiaoqi.security.admin.mall.feign.CommentFeign;

@RestController
@RequestMapping("comment")
public class CommentController {
	
    private final Log logger = LogFactory.getLog(CommentController.class);
    
    @Autowired
    private CommentFeign commentFeign;

    @GetMapping("/list")
    public Object list(String userId, String valueId,
                       @RequestParam(value = "page", defaultValue = "1") Integer page,
                       @RequestParam(value = "limit", defaultValue = "10") Integer limit,
                       String sort, String order){
        return commentFeign.list(userId, valueId, page, limit, sort, order);
    }

    @PostMapping("/create")
    public Object create(@RequestBody LitemallComment comment){
        return commentFeign.create(comment);
    }

    @GetMapping("/read")
    public Object read(Integer id){
        return commentFeign.read(id);
    }

    @PostMapping("/update")
    public Object update(@RequestBody LitemallComment comment){
        return commentFeign.update(comment);
    }

    @PostMapping("/delete")
    public Object delete(@RequestBody LitemallComment comment){
        return commentFeign.delete(comment);
    }

}
