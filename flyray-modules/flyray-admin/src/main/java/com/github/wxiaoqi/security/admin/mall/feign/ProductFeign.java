package com.github.wxiaoqi.security.admin.mall.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.github.wxiaoqi.security.admin.mall.domain.LitemallProduct;

@FeignClient(value = "mall-admin-api", configuration = {})
public interface ProductFeign {

	@RequestMapping(value = "/admin/product/list")
	public Object list(@RequestParam(value = "goodsId") Integer goodsId, @RequestParam(value = "page", defaultValue = "1") Integer page, @RequestParam(value = "limit", defaultValue = "10") Integer limit, @RequestParam(value = "sort") String sort, @RequestParam(value = "order") String order);

	/**
	 * @param adminId
	 * @param litemallProduct
	 * @return
	 */
	@RequestMapping(value = "/admin/product/create", method = RequestMethod.POST)
	public Object create(LitemallProduct litemallProduct);

	@RequestMapping(value = "/admin/product/read")
	public Object read(Integer id);

	@RequestMapping(value = "/admin/product/update", method = RequestMethod.POST)
	public Object update(LitemallProduct product);

	@RequestMapping(value = "/admin/product/delete", method = RequestMethod.POST)
	public Object delete(LitemallProduct product);

}
