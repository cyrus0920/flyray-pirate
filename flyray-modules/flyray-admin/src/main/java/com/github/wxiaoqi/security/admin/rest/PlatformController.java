package com.github.wxiaoqi.security.admin.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.github.wxiaoqi.security.admin.biz.PlatformBiz;
import com.github.wxiaoqi.security.admin.entity.Dept;
import com.github.wxiaoqi.security.admin.entity.Platform;
import com.github.wxiaoqi.security.admin.rpc.service.DeptService;
import com.github.wxiaoqi.security.admin.rpc.service.PlatformService;
import com.github.wxiaoqi.security.auth.common.util.SnowFlake;
import com.github.wxiaoqi.security.common.rest.BaseController;

/**
 *　@author bolei
 *　@date Apr 8, 2018
　*　@description 
**/

@RestController
@RequestMapping("platform")
public class PlatformController extends BaseController<PlatformBiz, Platform> {
	
	@Autowired
    private PlatformService platformService;
	@Autowired
    private DeptService deptService;
	
	/**
	 * 新增一个平台记录的同时初始化该平台的顶层组织结构
	 * @param entity
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> addPlatform(@RequestBody Platform entity) throws Exception {
		long platformId = SnowFlake.getId();
		entity.setPlatformId(platformId);
		platformService.addPlatform(entity);
		Dept dept = new Dept();
		dept.setName(entity.getPlatformName());
		dept.setParentId(-1);
		dept.setPlatformId(platformId);
		deptService.addDept(dept);
        return ResponseEntity.ok(entity);
    }
	
	/**
	 * 
	 */
	@RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<?> updatePlatform(@RequestBody Platform entity) throws Exception {
		platformService.updatePlatform(entity);
		Dept dept = new Dept();
		dept.setName(entity.getPlatformName());
		dept.setPlatformId(entity.getPlatformId());
		deptService.updateByPlatformId(dept);
        return ResponseEntity.ok(entity);
    }
}
