package com.github.wxiaoqi.security.admin.mall.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.github.wxiaoqi.security.admin.mall.domain.LitemallFootprint;

@FeignClient(value = "mall-admin-api", configuration = {})
public interface FootprintFeign {
	
	@RequestMapping(value = "/admin/footprint/list")
    public Object list(@RequestParam(value = "userId") String userId, @RequestParam(value = "goodsId") String goodsId, @RequestParam(value = "page", defaultValue = "1") Integer page, @RequestParam(value = "limit", defaultValue = "10") Integer limit, @RequestParam(value = "sort") String sort, @RequestParam(value = "order") String order);

	@RequestMapping(value = "/admin/footprint/create")
    public Object create(LitemallFootprint footprint);

	@RequestMapping(value = "/admin/footprint/read")
    public Object read(Integer id);

	@RequestMapping(value = "/admin/footprint/update")
    public Object update(LitemallFootprint footprint);

	@RequestMapping(value = "/admin/footprint/delete")
    public Object delete(LitemallFootprint footprint);

}
