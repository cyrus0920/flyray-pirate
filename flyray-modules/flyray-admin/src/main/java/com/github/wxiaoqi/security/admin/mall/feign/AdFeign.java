package com.github.wxiaoqi.security.admin.mall.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.github.wxiaoqi.security.admin.mall.domain.LitemallAd;

@FeignClient(value = "mall-admin-api", configuration = {})
public interface AdFeign {
	
	@RequestMapping(value = "/admin/ad/list")
	public Object list(@RequestParam(value = "name") String name,@RequestParam(value = "content") String content, @RequestParam(value = "page", defaultValue = "1") Integer page, @RequestParam(value = "limit", defaultValue = "10") Integer limit,@RequestParam(value = "sort") String sort,@RequestParam(value = "order") String order);

	@RequestMapping(value = "/admin/ad/create")
	public Object create(LitemallAd ad);
	
	@RequestMapping(value = "/admin/ad/read")
	public Object read(Integer id);
	
	@RequestMapping(value = "/admin/ad/update")
	public Object update(LitemallAd ad);
	
	@RequestMapping(value = "/admin/ad/delete")
	public Object delete(LitemallAd ad);

}
