package com.github.wxiaoqi.security.admin.mall.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.github.wxiaoqi.security.admin.mall.annotation.LoginAdmin;
import com.github.wxiaoqi.security.admin.mall.domain.LitemallCategory;

@FeignClient(value = "mall-admin-api", configuration = {})
public interface CategoryFeign {

	@RequestMapping(value = "/admin/category/list")
	public Object list(@RequestParam(value = "id") String id, @RequestParam(value = "name") String name, @RequestParam(value = "page", defaultValue = "1") Integer page, @RequestParam(value = "limit", defaultValue = "10") Integer limit, @RequestParam(value = "sort") String sort, @RequestParam(value = "order") String order);

	@RequestMapping(value = "/admin/category/create", method = RequestMethod.POST)
	public Object create(LitemallCategory category);

	@RequestMapping(value = "/admin/category/read")
	public Object read(Integer id);

	@RequestMapping(value = "/admin/category/update", method = RequestMethod.POST)
	public Object update(LitemallCategory category);

	@RequestMapping(value = "/admin/category/delete", method = RequestMethod.POST)
	public Object delete(LitemallCategory category);

	@RequestMapping(value = "/admin/category/l1")
	public Object catL1(@LoginAdmin Integer adminId);

}
