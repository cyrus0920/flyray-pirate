package com.github.wxiaoqi.security.admin.mall.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.github.wxiaoqi.security.admin.mall.domain.LitemallBrand;

@FeignClient(value = "mall-admin-api", configuration = {})
public interface BrandFeign {

	@RequestMapping(value = "/admin/brand/list")
	public Object list(@RequestParam(value = "id") String id, @RequestParam(value = "name") String name, @RequestParam(value = "page", defaultValue = "1") Integer page, @RequestParam(value = "limit", defaultValue = "10") Integer limit,@RequestParam(value = "sort") String sort,@RequestParam(value = "order") String order);

	@RequestMapping(value = "/admin/brand/create", method = RequestMethod.POST)
	public Object create(@RequestBody LitemallBrand brand);

	@RequestMapping(value = "/admin/brand/read")
	public Object read(Integer id);

	@RequestMapping(value = "/admin/brand/update", method = RequestMethod.POST)
	public Object update(LitemallBrand brand);

	@RequestMapping(value = "/admin/brand/delete", method = RequestMethod.POST)
	public Object delete(@RequestBody LitemallBrand brand);

}
