package com.github.wxiaoqi.security.admin.rpc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.admin.biz.UserBiz;
import com.github.wxiaoqi.security.admin.entity.User;

/** 
* @author: bolei
* @date：2018年4月17日 上午9:51:11 
* @description：类说明
*/

@Service
public class UserService {

	@Autowired
    private UserBiz userBiz;
	
	/**
	 * 新增用户 
	 * @param entity
	 */
	public int addUser(User entity) {
		return userBiz.insertSelective(entity);
    }
}
