package com.github.wxiaoqi.security.admin.mall.rest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.wxiaoqi.security.admin.mall.domain.LitemallTopic;
import com.github.wxiaoqi.security.admin.mall.feign.TopicFeign;

@RestController
@RequestMapping("topic")
public class TopicController {
	
    private final Log logger = LogFactory.getLog(TopicController.class);
    
    @Autowired
    private TopicFeign topicFeign;

    @GetMapping("/list")
    public Object list(String title, String subtitle,
                       @RequestParam(value = "page", defaultValue = "1") Integer page,
                       @RequestParam(value = "limit", defaultValue = "10") Integer limit,
                       String sort, String order){
        return topicFeign.list(title, subtitle, page, limit, sort, order);
    }

    @PostMapping("/create")
    public Object create( @RequestBody LitemallTopic topic){
        return topicFeign.create(topic);
    }

    @GetMapping("/read")
    public Object read( Integer id){
        return topicFeign.read(id);
    }

    @PostMapping("/update")
    public Object update( @RequestBody LitemallTopic topic){
        return topicFeign.update(topic);
    }

    @PostMapping("/delete")
    public Object delete( @RequestBody LitemallTopic topic){
        return topicFeign.delete(topic);
    }

}
