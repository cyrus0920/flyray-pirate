package com.github.wxiaoqi.security.admin.mall.rest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.wxiaoqi.security.admin.mall.domain.LitemallAdmin;
import com.github.wxiaoqi.security.admin.mall.feign.AdminFeign;

@RestController
@RequestMapping("admin")
public class AdminController {
	
	private final Log logger = LogFactory.getLog(AuthController.class);
	
	@Autowired
	private AdminFeign adminFeign;

    @GetMapping("/info")
    public Object info(String token){
        return adminFeign.info(token);
    }

    @GetMapping("/list")
    public Object list(String username,
                       @RequestParam(value = "page", defaultValue = "1") Integer page,
                       @RequestParam(value = "limit", defaultValue = "10") Integer limit,
                       String sort, String order){
        return adminFeign.list(username, page, limit, sort, order);
    }

    @PostMapping("/create")
    public Object create(@RequestBody LitemallAdmin admin){
        return adminFeign.create(admin);
    }

    @GetMapping("/read")
    public Object read(Integer id){
        return adminFeign.read(id);
    }

    @PostMapping("/update")
    public Object update(@RequestBody LitemallAdmin admin){
        return adminFeign.update(admin);
    }

    @PostMapping("/delete")
    public Object delete(@RequestBody LitemallAdmin admin){
        return adminFeign.delete(admin);
    }
}
