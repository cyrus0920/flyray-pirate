package com.github.wxiaoqi.security.admin.mall.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "mall-admin-api", configuration = {})
public interface RegionFeign {
	
	@RequestMapping(value = "/admin/region/clist")
	public Object clist(Integer id);

	@RequestMapping(value = "/admin/region/list")
    public Object list(@RequestParam(value = "name") String name, @RequestParam(value = "code") Integer code, @RequestParam(value = "page", defaultValue = "1") Integer page, @RequestParam(value = "limit", defaultValue = "10") Integer limit, @RequestParam(value = "sort") String sort, @RequestParam(value = "order") String order);

}
