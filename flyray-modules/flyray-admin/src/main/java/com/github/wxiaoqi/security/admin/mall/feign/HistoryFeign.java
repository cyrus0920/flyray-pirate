package com.github.wxiaoqi.security.admin.mall.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.github.wxiaoqi.security.admin.mall.domain.LitemallSearchHistory;

@FeignClient(value = "mall-admin-api", configuration = {})
public interface HistoryFeign {

	@RequestMapping(value = "/admin/history/list")
	public Object list(@RequestParam(value = "userId") String userId, @RequestParam(value = "keyword") String keyword, @RequestParam(value = "page", defaultValue = "1") Integer page, @RequestParam(value = "limit", defaultValue = "10") Integer limit, @RequestParam(value = "sort") String sort, @RequestParam(value = "order") String order);

	@RequestMapping(value = "/admin/history/create")
	public Object create(LitemallSearchHistory history);

	@RequestMapping(value = "/admin/history/read")
	public Object read(Integer id);

	@RequestMapping(value = "/admin/history/update")
	public Object update(LitemallSearchHistory history);

	@RequestMapping(value = "/admin/history/delete")
	public Object delete(LitemallSearchHistory history);

}
