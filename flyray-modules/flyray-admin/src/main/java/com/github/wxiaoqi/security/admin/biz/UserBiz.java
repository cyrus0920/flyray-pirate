package com.github.wxiaoqi.security.admin.biz;

import com.ace.cache.annotation.Cache;
import com.ace.cache.annotation.CacheClear;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.admin.entity.Role;
import com.github.wxiaoqi.security.admin.entity.User;
import com.github.wxiaoqi.security.admin.entity.UserRole;
import com.github.wxiaoqi.security.admin.mapper.MenuMapper;
import com.github.wxiaoqi.security.admin.mapper.UserMapper;
import com.github.wxiaoqi.security.auth.client.jwt.UserAuthUtil;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.constant.UserConstant;
import com.github.wxiaoqi.security.common.crm.request.QueryPersonalBaseListRequest;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.util.EntityUtils;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * ${DESCRIPTION}
 *
 * @author wanghaobin
 * @create 2017-06-08 16:23
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class UserBiz extends BaseBiz<UserMapper,User> {

    @Autowired
    private MenuMapper menuMapper;
    @Autowired
    private UserAuthUtil userAuthUtil;
    @Override
    public int insertSelective(User entity) {
        String password = new BCryptPasswordEncoder(UserConstant.PW_ENCORDER_SALT).encode(entity.getPassword());
        entity.setPassword(password);
        return super.insertSelective(entity);
    }
    public Map<String, Object> add(Map<String, Object> param) throws Exception {
    	String crtUser = (String) param.get("crtUser");
    	String crtName = (String) param.get("crtName");
    	User entity = EntityUtils.map2Bean(param, User.class);
        String password = new BCryptPasswordEncoder(UserConstant.PW_ENCORDER_SALT).encode(entity.getPassword());
        entity.setPassword(password);
        entity.setCrtTime(new Date());
        entity.setUpdTime(new Date());
        entity.setCrtName(crtName);
        entity.setCrtUser(crtUser);
        entity.setUpdName(crtName);
        entity.setUpdUser(crtUser);
        mapper.addUser(entity);
        Map<String, Object> result = new HashMap<String, Object>();
    	result.put("code", ResponseCode.OK.getCode());
    	result.put("msg", ResponseCode.OK.getMessage());
    	result.put("success", true);
    	result.put("userId", entity.getUserId());
        return result;
    }
    
    @Override
    @CacheClear(pre="user{1.username}")
    public void updateSelectiveById(User entity) {
        super.updateSelectiveById(entity);
    }

    /**
     * 根据用户名获取用户信息
     * @param username
     * @return
     */
    public User getUserByUsername(String username){
        User user = new User();
        user.setUsername(username);
        return mapper.selectOne(user);
    }

	public int addUser(User entity) {
		return mapper.addUser(entity);
	}
	public TableResultResponse<User> pageList(@RequestBody QueryPersonalBaseListRequest bean){
		Example example = new Example(User.class);
		Criteria criteria = example.createCriteria();
		if(!"".equals(bean.getPlatformId()) && null != bean.getPlatformId()){
			//是平台管理员
			criteria.andEqualTo("platformId", bean.getPlatformId());
		}else {
			//系统管理员
		}
		if(!"".equals(bean.getName()) && null != bean.getName()){
			criteria.andLike("name", "%" + bean.getName() + "%");
		}
		
		Page<User> result = PageHelper.startPage(bean.getPage(), bean.getLimit());
		List<User> list = mapper.selectByExample(example);
		return new TableResultResponse<User>(result.getTotal(), list);
	}
}
