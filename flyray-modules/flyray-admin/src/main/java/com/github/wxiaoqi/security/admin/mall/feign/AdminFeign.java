package com.github.wxiaoqi.security.admin.mall.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.github.wxiaoqi.security.admin.mall.domain.LitemallAdmin;

@FeignClient(value = "mall-admin-api", configuration = {})
public interface AdminFeign {

	@RequestMapping(value = "/admin/admin/info")
	public Object info(String token);

	@RequestMapping(value = "/admin/admin/list")
	public Object list(@RequestParam(value = "username") String username, @RequestParam(value = "page", defaultValue = "1") Integer page, @RequestParam(value = "limit", defaultValue = "10") Integer limit, @RequestParam(value = "sort") String sort, @RequestParam(value = "order") String order);

	@RequestMapping(value = "/admin/admin/create")
	public Object create(LitemallAdmin admin);

	@RequestMapping(value = "/admin/admin/read")
	public Object read(Integer id);

	@RequestMapping(value = "/admin/admin/update")
	public Object update(LitemallAdmin admin);

	@RequestMapping(value = "/admin/admin/delete")
	public Object delete(LitemallAdmin admin);
}
