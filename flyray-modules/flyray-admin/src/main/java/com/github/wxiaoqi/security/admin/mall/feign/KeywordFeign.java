package com.github.wxiaoqi.security.admin.mall.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.github.wxiaoqi.security.admin.mall.domain.LitemallKeyword;

@FeignClient(value = "mall-admin-api", configuration = {})
public interface KeywordFeign {

	@RequestMapping(value = "/admin/keyword/list")
	public Object list(@RequestParam(value = "keyword") String keyword, @RequestParam(value = "url") String url, @RequestParam(value = "page", defaultValue = "1") Integer page, @RequestParam(value = "limit", defaultValue = "10") Integer limit, @RequestParam(value = "sort") String sort, @RequestParam(value = "order") String order);

	@RequestMapping(value = "/admin/keyword/create", method = RequestMethod.POST)
	public Object create(LitemallKeyword keywords);

	@RequestMapping(value = "/admin/keyword/read")
	public Object read(Integer id);

	@RequestMapping(value = "/admin/keyword/update", method = RequestMethod.POST)
	public Object update(LitemallKeyword keywords);

	@RequestMapping(value = "/admin/keyword/delete", method = RequestMethod.POST)
	public Object delete(LitemallKeyword brand);

}
