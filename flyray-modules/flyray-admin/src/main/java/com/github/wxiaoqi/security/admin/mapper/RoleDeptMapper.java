package com.github.wxiaoqi.security.admin.mapper;

import com.github.wxiaoqi.security.admin.entity.RoleDept;
import tk.mybatis.mapper.common.Mapper;

public interface RoleDeptMapper extends Mapper<RoleDept> {
}