package com.github.wxiaoqi.security.admin.mall.rest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.wxiaoqi.security.admin.mall.domain.LitemallGoods;
import com.github.wxiaoqi.security.admin.mall.feign.GoodsFeign;

@RestController
@RequestMapping("goods")
public class GoodsController {
	
	@Autowired
	private GoodsFeign goodsFeign;
	
    private final Log logger = LogFactory.getLog(GoodsController.class);

    @GetMapping("/list")
    public Object list(String goodsSn, String name,
                       @RequestParam(value = "page", defaultValue = "1") Integer page,
                       @RequestParam(value = "limit", defaultValue = "10") Integer limit,
                       String sort, String order){
        return goodsFeign.list(goodsSn, name, page, limit, sort, order);
    }

    @PostMapping("/create")
    public Object create(@RequestBody LitemallGoods goods){
        return goodsFeign.create(goods);
    }

    @GetMapping("/read")
    public Object read(Integer id){
        return goodsFeign.read(id);
    }

    @PostMapping("/update")
    public Object update(@RequestBody LitemallGoods goods){
        return goodsFeign.update(goods);
    }

    @PostMapping("/delete")
    public Object delete(@RequestBody LitemallGoods goods){
        return goodsFeign.delete(goods);
    }

}
