package com.github.wxiaoqi.security.admin.mall.rest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.wxiaoqi.security.admin.mall.domain.LitemallIssue;
import com.github.wxiaoqi.security.admin.mall.feign.IssueFeign;

@RestController
@RequestMapping("issue")
public class IssueController {
	
    private final Log logger = LogFactory.getLog(IssueController.class);
    
    @Autowired
    private IssueFeign issueFeign;

    @GetMapping("/list")
    public Object list(String question,
                       @RequestParam(value = "page", defaultValue = "1") Integer page,
                       @RequestParam(value = "limit", defaultValue = "10") Integer limit,
                       String sort, String order){
        return issueFeign.list(question, page, limit, sort, order);
    }

    @PostMapping("/create")
    public Object create( @RequestBody LitemallIssue brand){
        return issueFeign.create(brand);
    }

    @GetMapping("/read")
    public Object read( Integer id){
        return issueFeign.read(id);
    }

    @PostMapping("/update")
    public Object update( @RequestBody LitemallIssue brand){
        return issueFeign.update(brand);
    }

    @PostMapping("/delete")
    public Object delete( @RequestBody LitemallIssue brand){
        return issueFeign.delete(brand);
    }

}
