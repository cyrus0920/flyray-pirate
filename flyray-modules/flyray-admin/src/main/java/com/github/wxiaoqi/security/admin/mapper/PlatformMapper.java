package com.github.wxiaoqi.security.admin.mapper;

import com.github.wxiaoqi.security.admin.entity.Platform;
import tk.mybatis.mapper.common.Mapper;

public interface PlatformMapper extends Mapper<Platform> {
}