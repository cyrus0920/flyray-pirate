package com.github.wxiaoqi.security.admin.mall.rest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.wxiaoqi.security.admin.mall.feign.RegionFeign;

@RestController
@RequestMapping("region")
public class RegionController {
	
    private final Log logger = LogFactory.getLog(RegionController.class);
    
    @Autowired
    private RegionFeign regionFeign;

    @GetMapping("/clist")
    public Object clist(Integer id) {
        return regionFeign.clist(id);
    }

    @GetMapping("/list")
    public Object list(String name, Integer code,
                       @RequestParam(value = "page", defaultValue = "1") Integer page,
                       @RequestParam(value = "limit", defaultValue = "10") Integer limit,
                       String sort, String order){
        return regionFeign.list(name, code, page, limit, sort, order);
    }
}
