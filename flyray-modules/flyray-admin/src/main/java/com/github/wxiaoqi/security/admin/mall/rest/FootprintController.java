package com.github.wxiaoqi.security.admin.mall.rest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.wxiaoqi.security.admin.mall.domain.LitemallFootprint;
import com.github.wxiaoqi.security.admin.mall.feign.FootprintFeign;

@RestController
@RequestMapping("footprint")
public class FootprintController {
	
    private final Log logger = LogFactory.getLog(FootprintController.class);
    
    @Autowired
    private FootprintFeign footprintFeign;

    @GetMapping("/list")
    public Object list(String userId, String goodsId,
                       @RequestParam(value = "page", defaultValue = "1") Integer page,
                       @RequestParam(value = "limit", defaultValue = "10") Integer limit,
                       String sort, String order){
        return footprintFeign.list(userId, goodsId, page, limit, sort, order);
    }

    @PostMapping("/create")
    public Object create( @RequestBody LitemallFootprint footprint){
        return footprintFeign.create(footprint);
    }

    @GetMapping("/read")
    public Object read( Integer id){
        return footprintFeign.read(id);
    }

    @PostMapping("/update")
    public Object update( @RequestBody LitemallFootprint footprint){
        return footprintFeign.update(footprint);
    }

    @PostMapping("/delete")
    public Object delete( @RequestBody LitemallFootprint footprint){
        return footprintFeign.delete(footprint);
    }

}
