package com.github.wxiaoqi.security.admin.rpc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.admin.biz.UserRoleBiz;
import com.github.wxiaoqi.security.admin.entity.UserRole;

/** 
* @author: bolei
* @date：2018年4月17日 上午9:55:51 
* @description：类说明
*/

@Service
public class UserRoleService {
	
	@Autowired
	private UserRoleBiz userRoleBiz;

	public void addUserRole(UserRole userRole) {
		userRoleBiz.insertSelective(userRole);
	}

}
