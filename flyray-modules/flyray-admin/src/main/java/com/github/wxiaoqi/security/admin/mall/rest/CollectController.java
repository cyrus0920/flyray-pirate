package com.github.wxiaoqi.security.admin.mall.rest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.wxiaoqi.security.admin.mall.domain.LitemallCollect;
import com.github.wxiaoqi.security.admin.mall.feign.CollectFeign;

@RestController
@RequestMapping("collect")
public class CollectController {
	
    private final Log logger = LogFactory.getLog(CollectController.class);
    
    @Autowired
    private CollectFeign collectFeign;

    @GetMapping("/list")
    public Object list(String userId, String valueId,
                       @RequestParam(value = "page", defaultValue = "1") Integer page,
                       @RequestParam(value = "limit", defaultValue = "10") Integer limit,
                       String sort, String order){
        return collectFeign.list(userId, valueId, page, limit, sort, order);
    }

    @PostMapping("/create")
    public Object create(@RequestBody LitemallCollect collect){
        return collectFeign.create(collect);
    }

    @GetMapping("/read")
    public Object read(Integer id){
        return collectFeign.read(id);
    }

    @PostMapping("/update")
    public Object update(@RequestBody LitemallCollect collect){
        return collectFeign.update(collect);
    }

    @PostMapping("/delete")
    public Object delete(@RequestBody LitemallCollect collect){
        return collectFeign.delete(collect);
    }

}
